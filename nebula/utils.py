import functools
import logging
import subprocess
import traceback
from threading import Thread, Barrier
from typing import Tuple, Collection, Callable


def get_commit_message() -> Tuple[str, str]:
    """

    :return: A tuple (commit number, commit message
    """
    try:
        res = subprocess.run(["git", "log", "-1", "--oneline"], capture_output=True, text=True).stdout

        # Being sure of only keeping the first line
        res = res.split("\n")[0]

        commit_nb = res.split(" ")[0]
        message = " ".join(res.split(" ")[1:])
        return commit_nb, message
    except:
        return "unknown", "no commit info"

def threaded_function_on_collection(function:Callable, collection:Collection, *args) :
    """Runs a threaded process on a function taking as a first parameter an element of a collection.  Will end when all threads will be done.

    If a thread fails or crashes, its exception is logged as an error and BrokenBarrierError is raised."""
    if len(collection) == 0 :
        return

    barrier = Barrier(len(collection) + 1)

    def f(e) :
        try :
            function(e, *args)

        except Exception as err :
            traceback.print_exc()
            logging.error(err)
            barrier.abort()

        else:
            barrier.wait()

    ths = [Thread(target=functools.partial(f, e)) for e in collection]
    for t in ths: t.start();
    barrier.wait()

def async_threaded_function_on_collection(function:Callable, collection:Collection, *args) :

    def f(e) :
        function(e, *args)

    ths = [Thread(target=functools.partial(f, e)) for e in collection]
    for t in ths: t.start();