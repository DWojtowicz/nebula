CREATE OR REPLACE LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS object_size;
CREATE OR REPLACE FUNCTION object_size(object_name varchar)
    RETURNS varchar
AS
$object_size$
DECLARE
    explain_line varchar DEFAULT '';
    --curs_explain CURSOR FOR ;
BEGIN
    IF object_name IN (SELECT table_name FROM information_schema.tables WHERE table_type = 'BASE TABLE') THEN
        RETURN '' || pg_relation_size(object_name);
    ELSEIF object_name IN
           (SELECT table_name FROM information_schema.tables WHERE table_type IN ('VIEW', 'FOREIGN')) THEN
        EXECUTE FORMAT('EXPLAIN SELECT * FROM ' || object_name || ';') INTO explain_line;
        RETURN explain_line;
    ELSE
        RAISE EXCEPTION 'Object does not exist in the schema.';
    END IF;
END;
$object_size$
    LANGUAGE PLPGSQL;
