import json
import logging
import os


def process_object_size(res):
    logging.info(f"Processing object size {res}")
    try:
        return int(res)
    except ValueError:
        if "Object does not exist in the schema" in res:
            raise ValueError
        else:
            # Nested Loop  (cost=100.00..134626439.35 rows=10760150700 width=29)
            width = int(res.split("width=")[1].split(")")[0])
            rows = int(res.split("rows=")[1].split(" width=")[0])
            return width * rows


def relation_size(connection, rel):
    """Computes the size of a relation or a relation list in GB"""
    cursor = connection.cursor()

    # Query computing input relations size
    size = 0
    if type(rel) not in (list, tuple, set, frozenset):
        rel = [rel]

    for t in rel:
        cursor.execute(f"SELECT object_size('{t}');")
        size += process_object_size(cursor.fetchone()[0])

    cursor.close()
    return float(size) / (1024 ** 3)

def estimation_info(q, connection, cost_factor):
    """Returns execution plan info for a given query. Cost factor is the number to use when dividing the cost top get the execution time"""
    # Asking for an execution plan
    cursor = connection.cursor()
    cursor.execute(f"EXPLAIN (FORMAT JSON) {q}")
    plan_lines = cursor.fetchall()
    cursor.close()
    # Parsing the plan

    # Returns a river-ready data point
    return parse_plan(plan_lines, cost_factor=cost_factor)


def parse_plan(plan, details=False, cost_factor=255):
    acc = []  # Accumulator to remember the details. Will be returned if details is set to True

    def parse_plan_rec(plan):

        # Default stuffn also leaf data
        res = {
            "width": plan["Plan Width"],
            "rows": plan["Plan Rows"],
            "output_size": plan["Plan Width"] * plan["Plan Rows"] / (1024 ** 3),  # In GB
            "cost": plan["Total Cost"],
            "est_time": plan["Total Cost"] / cost_factor,
            "depth": 1,
            "joins": 0,
            "filters": 0,
            "conditions": 0
        }
        try:
            res["actual_rows"] = plan["Actual Rows"]
            res["actual_time"] = plan["Actual Total Time"]
        except KeyError:
            pass

        # Non-default stuff
        if "Plans" in plan.keys():
            preds = [parse_plan_rec(e) for e in plan["Plans"]]

            # Counting joins
            if len(preds) > 1:
                res["joins"] = 1 + max(e["joins"] for e in preds)
            else:
                res["joins"] = max(e["joins"] for e in preds)

            res["cost"] += sum(e["cost"] for e in preds)
            res["depth"] += max(e["depth"] for e in preds)
            res["conditions"] = sum(e["conditions"] for e in preds)
            res["filters"] = sum(e["filters"] for e in preds)

            # If the query was actually executed
            try:
                res["actual_time"] += sum(e["actual_time"] for e in preds)
            except KeyError:
                pass

        # Other cases
        if any("Cond" in k for k in plan.keys()):
            res["conditions"] += 1

        if "Filter" in plan.keys():
            res["filters"] += 1

        acc.append(res)
        return res

    res = parse_plan_rec(plan[0][0][0]["Plan"])

    if details:
        return acc
    else:
        return res

def __element_creation(query, query_name, provider, type: str):
    # Generating instructions
    query_bak = query
    query = f"DROP {type} IF EXISTS {query_name} ; CREATE {type} {query_name} AS {query}".strip()

    # Ensuring proper formatting
    query = query.replace("\\n", "  ").replace("\n", "  ")

    if query[-1] != ";":
        query = f"{query} ;"

    query = f"{query}COMMIT;"

    # Creating the object
    res = provider.admin(query)
    return res


def view_creation(query, query_name, provider):
    print("=== VIEW CREATION")
    print(query)
    print(query_name)
    res = __element_creation(query, query_name, provider, "VIEW")
    est = provider.view_estimation(query_name, query)
    print("-------")
    print(res)
    print(est)
    print("========")
    return res


def table_creation(query, query_name, provider):
    res = __element_creation(query, query_name, provider, "TABLE")
    provider.estimate(query_name, query)
    return res


def foreign_table_creation(local_server, source_server, relations, estimate=False, query_name=None):
    print("===== Foreign table creation")
    print(local_server)
    print(source_server)
    print(relations)
    print(estimate)
    print(query_name)
    print("----")
    view_data = dict()
    if estimate:
        for r in relations:
            est = source_server.estimate(f"SELECT * FROM {r};", )
            view_data[r] = est.estimated_output_size

    query = f"{'; '.join(f'DROP FOREIGN TABLE IF EXISTS {r}' for r in relations)}"
    query = f"{query}; IMPORT FOREIGN SCHEMA public LIMIT TO ({', '.join(r for r in relations)}) "
    query = f"{query} FROM SERVER {source_server.name} INTO public;"

    res = local_server.admin(query, str(view_data))

    print("-----")
    print(res)
    print("================")
    return res


def clean_view(node, G):
    """Deletes a view from the database"""
    provider = node[0]

    return json.loads(
        provider.admin(
            f"DROP VIEW IF EXISTS {G[node]['qname']} CASCADE;"
        )
    )


def object_size_function():
    """Returns the SQL code from `object_size_function.sql` as a string"""
    file_path = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "object_size_function.sql"
    )
    with open(file_path, 'r') as f:
        return "".join(f.readlines())


class AdminError(Exception):
    def __init__(self, message):
        super().__init__(message)

class DuplicateObjectError(Exception):
    """Deprecated since issue triggering this one was fixed"""
    def __init__(self, message=""):
        super().__init__(message)