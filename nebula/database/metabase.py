"""Contains the ORM utilities to manipulate the metabase"""
import json
import logging
import os
from json import JSONDecodeError
from threading import Lock
from typing import Tuple

import requests
import sys
import time
from psycopg.errors import WrongObjectType
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Boolean, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker

# Getting db file and cleaning it up
from nebula.database.utils import AdminError, DuplicateObjectError
from nebula.engine.quotation import Quotation

dbname = str(sys.argv[0]).split("/")[-1].split(".py")[0]
try:
    if "conf_" in str(sys.argv[1]):
        dbname += str(sys.argv[1]).split("conf_")[-1].split(".json")[0]
except IndexError:
    pass  # Case when the main script is not a provider

try:
    os.remove(f"{dbname}.db")
except FileNotFoundError :
    pass

db_lock = Lock()

# Establishment of the communication betwenn the DB and the script

engine = create_engine(f"sqlite:///{dbname}.db?check_same_thread=False")  # , echo=True)
Base = declarative_base()

class Provider(Base) :
    """A cloud provider"""
    __tablename__ = "providers"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    url = Column(String)
    port = Column(Integer)
    dbname = Column(String)
    export = Column(Float)
    query = Column(Float)
    storage = Column(Float)

    def __new_entity(self, name, rel_type="VIEW"):
        db_lock.acquire()
        try:
            new_schema = self.schema()
            attrs = [Attribute(name=a["name"], data_type=a["data_type"], primary_key=a["primary_key"]) for a in
                     new_schema[name]["attributes"]]
        except KeyError:
            # The new schema is not yet up to date. Retrying...
            logging.info(f"{time.asctime()} Relation {name} not yet in {self.name}... retrying")
            time.sleep(2)
            new_schema = self.schema()
            attrs = [Attribute(name=a["name"], data_type=a["data_type"], primary_key=a["primary_key"]) for a in
                     new_schema[name]["attributes"]]

        rel = Relation(name=name, rel_type=rel_type)
        rel.attributes = attrs
        self.relations.append(rel)
        logging.info(f"In provider {name},entity {name} has been added to the metabase")
        db_lock.release()

    def __drop_entity(self, name):
        #        self.relations = filter(lambda x: x.name != name, self.relations)
        global db_lock
        db_lock.acquire()
        to_delete = None
        for i in range(len(self.relations)):
            if self.relations[i].name == name:
                to_delete = self.relations[i]

        if to_delete is not None:
            self.relations.remove(to_delete)
        Session.commit()
        logging.debug(f"In provider {self.name},entity {name} has been deleted from the metabase")
        db_lock.release()

    def __update_schema(self, query):
        global db_lock
        query = query.casefold().replace(" or replace", "")
        name = None
        rel_type = None

        # Determining wether a new entity is added or not
        if "create" in query and ("table" in query or "view" in query):
            # Extract entity name
            name = query.split("create")[1].split(" as ")[0].strip().split(" ")[1].strip()
            if "view" in query:
                rel_type = "VIEW"
            else:
                rel_type = "TABLE"
        elif "import foreign schema" in query:
            name = query.split("limit to (")[1].strip().split(")")[0].strip()
            rel_type = "FOREIGN TABLE"
        elif "drop" in query:
            name = query.split("drop")[1].split(";")[0].strip().split(" ")[-1].strip()
            creation = False

        # It there's nothing to do, we exit the method
        if name is None:
            return

        if rel_type is not None:
            self.__new_entity(name, rel_type)
        else:
            self.__drop_entity(name)

    def admin(self, query, views_info=None, no_update=False, debug=False, asynchronous=False):

        data = {"query": query}
        if views_info is not None:
            data["views_info"] = views_info

        if debug:
            print("== SENDING MESSAGE TO ", f"http://{self.url}:{self.port}/admin")
            print(data)
        """
        if asynchronous :
            grequests.post(
                f"http://{self.url}:{self.port}/admin",
                data=data
            )
            return
        """
        message =requests.post(
            f"http://{self.url}:{self.port}/admin",
            data=data
        )

        message = json.loads(message.content)
        if message['status_code'] == 500:
            logging.error(f"Provider {self.name} failed to execute admin query {query}")
            logging.error(message["details"])
            if "uplicate" in message["details"] :
                raise DuplicateObjectError()
            elif "WrongObjectType" in message["details"] :
                raise WrongObjectType()
            else :
                raise AdminError(f"Admin failure on {self.name} with code {message['status_code']}.")

        # Updating providers schema when a new table or view is added
        if not no_update:
            self.__update_schema(query)

        return message

    def schema(self):
        response = requests.get(f"http://{self.url}:{self.port}/schema")
        return json.loads(response.content)

    def estimate(self, query, query_name="") -> Quotation:
        logging.info(f"Estimating on {self.name} query {query_name} : {query}")
        message = requests.post(
            f"http://{self.url}:{self.port}/estimate",
            data={"query": query, "query_name": query_name}
        )
        try :
            message = json.loads(message.content)
        except JSONDecodeError as e :
            raise AdminError(message.content)
        if "status_code" in message and message["status_code"] != 200:
            logging.error(query)
            raise AdminError(message)
        logging.info(f"Estimation of {self.name} query {query_name} : {message}")
        return Quotation(query, self, message, query_name=query_name)

    def execute(self, query, query_name, substitute_name="", mode="storage", clean=False):
        """
        Params
        ------
        query : str
            SQL code for the query

        mode : str (optionnal, defaults to 'storage')
            Either 'storage' or 'export'

        query_name : str (Optionnal)
            Name of the table where results will be stored

        substitute_name : str (Optionnal)
            Query name used for estimates

        clean : bool (Optionnal)
            Deletes everything involving the query starting by "msq_"

        """
        data = {
            "query": query,
            "query_name": query_name,
            "substitute_name": substitute_name,
            "mode": mode
        }

        message = requests.post(f"http://{self.url}:{self.port}/execute", data=data)
        try:
            message = json.loads(message.content)
        except JSONDecodeError :
            logging.error("")
        if "status_code" in message and message["status_code"] == 500:
            logging.error(f"ERREUR sur {self.name} ===========================")
            logging.error(query)
            logging.error(message)
            logging.error("FIN ERREUR =======================")

        if clean:
            tables = [
                t.strip()
                for t in query.split("FROM")[1] \
                    .split("WHERE")[0] \
                    .split(",")
                if t.strip().startswith("msq_") or t.strip().startswith("MSQ_")
            ]
            for t in tables:
                self.admin(f"DROP VIEW IF EXISTS {t};")
                self.admin(f"DROP TABLE IF EXISTS {t};")
                self.admin(f"DROP FOREIGN TABLE IF EXISTS {t};")
        return message

    def view_estimation(self, view_name, query):
        logging.info(f"VIEW ESTIMATION\n{view_name}\n{query}")
        message = requests.post(
            f"http://{self.url}:{self.port}/view_estimation",
            data={"query": query, "view_name": view_name}
        )
        message = json.loads(message.content)
        return message

    def object_size(self, object_name: str) -> float:
        """Wrapper for provider's API's object_size"""

        message = requests.post(
            f"http://{self.url}:{self.port}/object_size",
            data={"object_name": object_name}
        )

        message = json.loads(message.content)
        return message["size"]

    def dump_table(self, table_name:str):
        """Saves a table into a file"""
        message = requests.post(
            f"http://{self.url}:{self.port}/dump_table",
            data={"table_name": table_name}
        )
        try :
            message = json.loads(message.content)
        except JSONDecodeError as e :
            raise AdminError(message.content)
        if "status_code" in message and message["status_code"] != 200:
            logging.error(f"Dump table {table_name} failed on {self.name}")
            raise AdminError(message)

    def delete_table_file(self, table_name:str):
        """Orders a table file to be deleted"""
        """
        grequests.post(
            f"http://{self.url}:{self.port}/delete_table_file",
            data={"table_name": table_name}
        )
        """
        message = requests.post(
            f"http://{self.url}:{self.port}/delete_table_file",
            data={"table_name": table_name}
        )
        try:
            message = json.loads(message.content)
        except JSONDecodeError as e:
            raise AdminError(message.content)
        if "status_code" in message and message["status_code"] != 200:
            logging.error(f"Delete file table {table_name} failed on {self.name}")
            raise AdminError(message)

    def load_table(self, table_name:str, origin_provider):
        """Loads a table table_name from origin_provider (a Provider)"""
        message = requests.post(
            f"http://{self.url}:{self.port}/load_table",
            data={
                "table_name": table_name,
                "origin_provider_name": origin_provider.name,
                "origin_provider_url": origin_provider.url,
                "origin_provider_port": origin_provider.port
            }
        )
        try :
            message = json.loads(message.content)
        except JSONDecodeError as e :
            raise AdminError(message.content)
        if "status_code" in message and message["status_code"] != 200:
            logging.error(f"Load table {table_name} from {origin_provider.name} failed on {self.name}")
            raise AdminError(message)

    @property
    def prices(self):
        try :
            return self.__prices_dict
        except AttributeError :
            response = requests.get(f"http://{self.url}:{self.port}/pricing")
            self.__prices_dict = json.loads(response.content)
            return self.__prices_dict

    @property
    def export_cost(self):
        return self.prices['export']

    @property
    def query_cost(self):
        return self.prices['query']

    @property
    def storage_cost(self):
        return self.prices['storage']

    def __repr__(self):
        return f"Provider {self.id} - {self.name}"

    def __getitem__(self, item):
        """Legacy purposes"""
        if item == 0 :
            return self

        raise TypeError("'Provider' object is not subscriptable")

class Relation(Base) :
    """A relation from a provider's DB"""
    __tablename__ = "relations"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    provider_id = Column(Integer, ForeignKey("providers.id"))
    provider = relationship("Provider", back_populates="relations")
    rel_type = Column(String)

    alias = None

    def __repr__(self):
        return f"<Relation {self.rel_type} id:'{self.id}', name:'{self.name}', provider_id:'{self.provider_id}'>"

    def __str__(self):
        return repr(self)

    def full_name(self):
        return f"{self.provider.name}.{self.name}"

    @property
    def primary_key(self) -> Tuple:
        return tuple(a for a in self.attributes if a.primary_key)

    @property
    def primary_key_str(self) -> str:
        return f"({', '.join(a.name for a in self.primary_key)})"


# Adds an attribute listing the provider's relations
Provider.relations = relationship("Relation", order_by=Relation.id, back_populates="provider", lazy='subquery')


class Attribute(Base):
    """The attribute of a relation"""
    __tablename__ = "attributes"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    data_type = Column(String)
    primary_key = Column(Boolean, default=False)
    relation_id = Column(Integer, ForeignKey("relations.id"))
    relation = relationship("Relation", back_populates="attributes")

    def __repr__(self):
        return f"Attribute {self.id} - {self.data_type} {self.name} from {self.relation.name}"

    def full_name(self):
        return f"{self.relation.full_name()}.{self.name}"


# Adds an attribute listing the relation's attributes
Relation.attributes = relationship("Attribute", order_by=Attribute.id, back_populates="relation", lazy='subquery')

# Final creation of the DB scheme
Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine, expire_on_commit=False)
Session = Session()
