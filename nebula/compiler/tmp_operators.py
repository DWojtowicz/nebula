class TmpProjection() :
    def __init__(self, predicate:str, attribute_names:list):
        self.predicate = predicate
        self.attribute_names = attribute_names

class TmpProjectionStar() :
    def __init__(self, predicate):
        self.predicate = predicate


class TmpAttribute() :
    def __init__(self, name, alias=None):
        self.name = name
        self.alias = alias

    def __str__(self):
        if self.alias :
            return f"{self.alias}.{self.name}"
        else :
            return self.name

    def __repr__(self):
        return "ATTR " + str(self)


class TmpRelation() :
    def __init__(self, name, alias=None):
        self.name = name
        self.alias = alias

    def __str__(self):
        if self.alias :
            return self.alias
        else :
            return self.name

    def __repr__(self):
        if self.alias :
            return f"REL {self.alias}.{self.name}"
        else :
            return f"REL {self.name}"
