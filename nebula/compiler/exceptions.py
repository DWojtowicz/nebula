class SQLSyntaxError(Exception):
    def __init__(self, message):
        super(SQLSyntaxError, self).__init__(message)
