from typing import List, Tuple

from sly import Parser
from sly.lex import LexError

from nebula.compiler.exceptions import SQLSyntaxError
from nebula.compiler.lexer import SqlLexer
from nebula.compiler.tmp_operators import TmpAttribute, TmpProjectionStar, TmpRelation
from nebula.database.metabase import Relation, Provider, Attribute, Session
from nebula.engine.query.operators import Ensembly, ReadTable


class SqlParser(Parser):
    """Parser for a SQL Data Interrogation Languge grammar, returning an execution plan.
    Implementation follows the tutorial at https://sly.readthedocs.io/en/latest/sly.html#writing-a-parser


    Attributes
    ----------

    alias_mapping : Dict[str, str]
        Maps table alias to real table names
    read_tables : List[ReadTable]
        List of relations used in the query
    projection : List
        The projection from the SELECT clause
    selections : List
        The selections from the WHERE clause
    session : Session
        SqlAlchemy's session, used to interact with the database containing the metabase
    """
    debugfile = "parser.out"

    tokens = SqlLexer.tokens

    precedence = (
        ("nonassoc", EQ, GE, GT, LE, LT, NE_A, NE_B),
        ("left", PLUS, SUBSTRACT, AND, BWISE_OR, BWISE_XOR, EXCEPT, MINUS, UNION),
        ("left", STAR, DIVIDE, OR, BWISE_AND, INTERSECT),
        ("left", MODULO, DOT),
        # The case for unary operators
        ("right", USUBSTRACT)
    )

    def __init__(self):

        self.alias_mapping = dict()
        self.read_tables = list()
        self.clauses = list()
        self.projection_predicates = list()
        self.session = Session

        self.subqueries = dict()

        self.lexer = SqlLexer()

    #
    # DEVELOPER'S NOTES
    #
    # The main issue here is the SELECT part of the query is scanned before the FROM part, implying pre-processing
    # the SELECT by fetching all its clauses and the attributes they involve. Existence of attributes and final setup of
    # the projection is performed after extracting the tables from the FROM.
    # Hence, the case of the WHERE is easier, because errors can be detected at parsing time.
    #
    # Grammar rules :
    # For each nonterminal (the functions below), the rules shall be written from the broadest to the more specific.
    #


    ####################################################################################################################
    #
    # General query structure
    #
    ####################################################################################################################

    @_("query INTERSECT query",
       "query EXCEPT query",
       "query MINUS query",
       "query UNION query"
       )
    def meta_query(self, p):
        """Case for queries containing ensemble operators"""
        return Ensembly(p[0], p[2] , p[1])

    @_("query SEMICOL")
    def meta_query(self, p):
        return p.query

    @_("select_statements from_statements optional_statements",
       "select_statements from_statements")
    def query(self, p):
        """Temporary fallback"""

        # Gets all ReadTable operators
        # Fetches projection form the execution plan
        return p


    @_("where_statements")
    def optional_statements(self, p):
        return p

    @_("where_statements group_by_statements")
    def optional_statements(self, p):
        return p

    @_("group_by_statements")
    def optional_statements(self, p):
        return p

    @_("group_by_statements having_statements")
    def optional_statements(self, p):
        return p

    @_("where_statements group_by_statements having_statements")
    def optional_statements(self, p):
        return p

    ####################################################################################################################
    #
    # SELECT clause
    #
    ####################################################################################################################

    # select_statements ################################################################################################
    @_("SELECT STAR")
    def select_statements(self, p):
        return [TmpProjectionStar("*")]

    @_("SELECT projection_clauses")
    def select_statements(self, p):
        """AST is scanned, looking for attributes
        :returns A list of temporary projection
        """
        clauses = self.clauses
        self.clauses = list()
        return clauses


    # projection_clauses ###############################################################################################

    @_("projection_clause",
       "projection_clause AS alias")
    def projection_clauses(self, p):
        self.clauses.append(p[0])

    @_("projection_clause COMMA projection_clauses")
    def projection_clauses(self, p):
        self.clauses.append(p.projection_clause)


    # projection_clause ################################################################################################

    @_("SUBSTRACT projection_clause %prec USUBSTRACT")
    def projection_clause(self, p):
        return self.handle_negation(p.projection_clause, " -")

    @_("value")
    def projection_clause(self, p):
        return p.value

    def handle_negation(self, proj_clause, operator):
        """Manages application of a negation using a - or a NOT"""
        if type(proj_clause) is list :
            attrs, rpr = proj_clause
            return [attrs, f"{operator}{rpr}"]

        elif type(proj_clause) is TmpAttribute :
            return [[proj_clause], f"{operator}{str(proj_clause)}"]

        else :
            return f"{operator}{proj_clause}"

    def handle_projection_clause(self, proj_clause):
        """Returns a list of attributes and a string according to the function parameters format"""
        attributes = list()
        if type(proj_clause) is TmpAttribute :
            attributes.append(proj_clause)
            representation = str(proj_clause)
        elif type(proj_clause) is list :
            attributes.extend(proj_clause[0])
            representation = proj_clause[1]
        else :
            representation = proj_clause

        return attributes, representation

    def handle_twoside_projection_clause(self, left, sym, right):
        out_attr = list()
        att_1, rpr_1 = self.handle_projection_clause(left)
        att_2, rpr_2 = self.handle_projection_clause(right)

        out_attr.extend(att_1)
        out_attr.extend(att_2)

        rpr = f"{rpr_1} {sym} {rpr_2}"

        if len(out_attr) > 0 :
            return [out_attr, rpr]
        else:
            return rpr

    @_("projection_clause DIVIDE projection_clause",
       "projection_clause MODULO projection_clause",
       "projection_clause PLUS projection_clause",
       "projection_clause STAR projection_clause",
       "projection_clause SUBSTRACT projection_clause")
    def projection_clause(self, p):
        return self.handle_twoside_projection_clause(p[0], p[1], p[2])

    @_("'(' func_params ')'")
    def projection_clause(self, p):
        return p.func_params

    # functions ########################################################################################################

    @_("func_name '(' func_params ')'")
    def function(self, p):
        if type(p.func_params) is list:
            return [p.func_params[0], f"{p.func_name}({p.func_params[1]})"]
        elif type(p.func_params) is TmpAttribute:
            return [[p.func_params], f"{p.func_name}({p.func_params})"]
        else:
            return f"{p.func_name}({p.func_params})"


    @_("func_name '(' STAR ')'")
    def function(self, p):
        return f"{p.func_name}(*)"

    # func_params ######################################################################################################

    @_("projection_clause")
    def func_params(self, p):
        return p.projection_clause

    @_("projection_clause COMMA func_params")
    def func_params(self, p):
        return self.handle_twoside_projection_clause(p.projection_clause, ",", p.func_params)


    # names and constants ##############################################################################################

    @_("attribute",
       "constant",
       "function")
    def value(self, p):
        return p[0]

    @_("NAME")
    def func_name(self, p) -> str:
        return p.NAME

    @_("DECIMAL")
    def constant(self, p) -> str:
        return str(p.DECIMAL)

    @_("STRING")
    def constant(self, p) -> str:
        return p.STRING

    @_("NULL")
    def constant(self, p) -> str:
        return "NULL"

    @_("table_name DOT attribute_name")
    def attribute(self, p) -> TmpAttribute:
        return TmpAttribute(p.attribute_name, p.table_name)

    @_("attribute_name")
    def attribute(self, p) -> TmpAttribute:
        return TmpAttribute(p.attribute_name)

    @_("NAME")
    def attribute_name(self, p) -> str:
        return p.NAME

    ####################################################################################################################
    #
    # FROM clause
    #
    ####################################################################################################################

    @_("FROM from_statement")
    def from_statements(self, p):
        read_tables = self.read_tables
        self.read_tables = list()
        return read_tables


    # from_statement ###################################################################################################

    @_("table_declaration")
    def from_statement(self, p):
        """Creates the list of ReadTable operators from the query
        :returns List[ReadTable]
        """
        self.read_tables.append(p.table_declaration)


    @_("table_declaration COMMA from_statement",
       "table_declaration NATURAL_JOIN from_statement")
    def from_statement(self, p):
        """Expands the list of tables used in the query, created by the function above.
        """
        self.read_tables.append(p.table_declaration)


    # table_declaration ################################################################################################
    @_("table_name alias",
       "table_name AS alias")
    def table_declaration(self, p) -> ReadTable:
        """
        :raises: NameError
        """
        if p.alias in self.alias_mapping:
            raise NameError(f"Table alias {p.alias} already defined.")  # TODO Create a specific error type

        elif p.table_name in self.alias_mapping:
            raise NameError(f"Table {p.table_name} already defined without alias.")

        else:
            self.alias_mapping[p.alias] = p.table_name

        # The relation is fetched into the database
        return TmpRelation(p.table_name, p.alias)

    @_("table_name")
    def table_declaration(self, p) -> ReadTable:
        """
        table_name : str
        :raises: NameError
        """
        if p.table_name in self.alias_mapping:
            print(self.alias_mapping)
            raise NameError(f"Table {p.table_name} already defined.")

        elif any(self.alias_mapping[k] == p.table_name for k in self.alias_mapping):
            raise NameError(f"Table {p.table_name} already aliased.")

        else:
            self.alias_mapping[p.table_name] = p.table_name

        return TmpRelation(p.table_name)


    # names and constants ##############################################################################################

    @_("NAME")
    def alias(self, p) -> str:
        return p.NAME

    @_("NAME")
    def table_name(self, p) -> str:
        return p.NAME

    ####################################################################################################################
    #
    # WHERE clause
    #
    # A WHERE clause is basically a SELECT clause where commas are replaced by boolean operators (AND, OR, bitwise AND,
    # OR and XOR). Parenthesed clauses is the main difference.
    #
    # Here are examples showing how clauses are identified :
    #   p1 AND p2 OR p1 AND p3
    #      -> clauseA AND clauseB OR clauseC AND clauseD
    #      -> clauseX OR clause Y
    #      => [clauseZ]
    #
    #    p1 AND p2
    #      -> clause1 AND clause2
    #      => [clause1, clause2]
    #
    #    (p1 OR p2) AND p3
    #      -> (clause1 OR clause2) AND clause3
    #      -> clauseA AND clauseB
    #      -> [clauseA, clauseB]
    #
    #
    # In order to avoid code repetition, function handling is the same as SELECT statement.
    #
    ####################################################################################################################

    @_("WHERE boolean_evaluations")
    def where_statements(self, p):
        return p

    # boolean_evaluations ##############################################################################################

    @_("boolean_evaluation")
    def boolean_evaluations(self, p):
        return [p.boolean_evaluation]

    @_("boolean_evaluation AND boolean_evaluations")
    def boolean_evaluations(self, p):
        p.boolean_evaluations.append(p.boolean_evaluation)
        return p.boolean_evaluations

#        if type(p.boolean_evaluations) is list :
#            if all(type(e) is TmpAttribute for e in p.boolean_evaluations[0]) and type(p.boolean_evaluations[1]) is str:
#                return [p.boolean_evaluation, p.boolean_evaluations]
#            else :
#                p.boolean_evaluations.append(p.boolean_evaluation)
#                return p.boolean_evaluations
#        else :
#            return [p.boolean_evaluation, p.boolean_evaluations]


    # boolean_evaluation ###############################################################################################

    @_("projection_clause",
       "NOT projection_clause")
    def boolean_evaluation(self, p):
        # Attributes, constants or functions might be boolean
        attr, rpr = self.handle_projection_clause(p.projection_clause)
        if p[0] == "NOT":
            return self.handle_negation(p.projection_clause, " NOT ")
        else:
            return p.projection_clause

    @_("value")
    def values_list(self, p):
        return p[0]

    @_("value COMMA values_list")
    def values_list(self, p):
        return f"{p[0]} , {p[-1]}"

    @_("projection_clause IN ( values_list )",
       "projection_clause NOT IN ( values_list )")
    def boolean_evaluation(self, p):
        # Attributes, constants or functions might be boolean
        if len(p) == 5:
            return self.handle_twoside_projection_clause(p[0], " IN ", f"( {p[-2]} )")
        elif len(p) == 6:
            return self.handle_twoside_projection_clause(p[0], " NOT IN ", f"( {p[-2]} )")
        else:
            raise SQLSyntaxError(p)

    @_("projection_clause EQ projection_clause",
       "projection_clause NE_A projection_clause",
       "projection_clause NE_B projection_clause",
       "projection_clause GE projection_clause",
       "projection_clause GT projection_clause",
       "projection_clause LE projection_clause",
       "projection_clause LT projection_clause",
       "projection_clause LIKE STRING",
       "projection_clause NOT LIKE STRING",
       "projection_clause IS NOT NULL",
       "projection_clause IS NULL"
       )
    def boolean_evaluation(self, p):
        # Attributes, constants or functions might be boolean
        if len(p) == 3:
            return self.handle_twoside_projection_clause(p[0], p[1], p[2])
        else:
            return self.handle_twoside_projection_clause(p[0], f"{p[1]} {p[2]}", p[3])

    @_("projection_clause BETWEEN projection_clause AND projection_clause")
    def boolean_evaluation(self, p):
        tmp = self.handle_twoside_projection_clause(p[0], p[1], p[2])
        tmp = self.handle_twoside_projection_clause(tmp, p[3], p[4])
        return tmp

    @_("boolean_evaluation OR boolean_evaluation")
    def boolean_evaluation(self, p):
        """OR operator """
        return self.handle_twoside_projection_clause(p[0], f" {p[1]} ", p[2])

    def handle_boolean_evaluation(self, bool_eval:List) -> Tuple[List, str] :
        attr = list()
        rpr = ""

        for e in bool_eval :
            if type(e[0]) is str :
                rpr = f"{e[0]} AND {rpr}"
            elif type(e[0]) is TmpAttribute :
                attr.append(e[0])
                rpr = f"{str(e[0])} AND {rpr}"
            else :
                attr.extend(e[0])
                rpr = f"{e[1]} AND {rpr}"

        # Cleaning useless trailing AND
        rpr = rpr.strip()
        if rpr.endswith(" AND") :
            rpr = rpr[:-4]

        return attr, rpr

    @_("( boolean_evaluations )",
       "NOT ( boolean_evaluations )")
    def boolean_evaluation(self, p):
        attr, rpr = self.handle_boolean_evaluation(p.boolean_evaluations)
        rpr = f"{'NOT' if p[0] == 'NOT' else ''}({rpr})"
        return [attr, rpr]



    ####################################################################################################################
    #
    # GROUP BY clause
    #
    ####################################################################################################################

    @_("GROUP_BY group_by_clauses")
    def group_by_statements(self, p):
        p.group_by_clauses.reverse() # Keeps the order of the clause
        return p

    @_("attribute COMMA group_by_clauses")
    def group_by_clauses(self, p):
        p.group_by_clauses.append(p.attribute)
        return p.group_by_clauses

    @_("attribute")
    def group_by_clauses(self, p):
        return [p.attribute]


    ####################################################################################################################
    #
    # HAVING clause
    #
    ####################################################################################################################

    @_("HAVING boolean_evaluations")
    def having_statements(self, p):
        return p


    ####################################################################################################################
    #
    # Misc class functions
    #
    ####################################################################################################################

    def parse(self, query: str) -> tuple:
        """May rise LexError"""
        try:
            tks = self.lexer.tokenize(query.strip())
            tree = super().parse(tks)
        except LexError as e:
            raise SQLSyntaxError(e)
        return tree, self.alias_mapping

    def error(self, token):
        '''
        Default error handling function.  This may be subclassed.
        '''
        if token:
            lineno = getattr(token, 'lineno', 0)
            if lineno:
                raise SQLSyntaxError(f'sly: Syntax error at line {lineno}, token={token.type}\n')
            else:
                raise SQLSyntaxError(f'sly: Syntax error, token={token.type}')
        else:
            raise SQLSyntaxError('sly: Parse error in input. EOF\n')



if __name__ == '__main__':

    p1 = Provider(name="P1")
    p1.relations = [
        Relation(name="Final"),
        Relation(name="Worser"),
        Relation(name="cast_info")
    ]

    p1.relations[0].attributes = [
        Attribute(name="test", data_type="int")
    ]

    p1.relations[1].attributes = [
        Attribute(name="x", data_type="int"),
        Attribute(name="y1", data_type="int"),
        Attribute(name="y2", data_type="int")
    ]

    p1.relations[2].attributes = [
        Attribute(name="borz", data_type="int"),
        Attribute(name="y", data_type="int"),
        Attribute(name="x", data_type="int"),
        Attribute(name="a", data_type="int")
    ]

    p2 = Provider(name="P2")
    p2.relations = [
        Relation(name="Bazor")
    ]
    p2.relations[0].attributes = [
        Attribute(name="a", data_type="int"),
        Attribute(name="x", data_type="int"),
    ]

    session = Session
    session.add(p1)
    session.add(p2)
    session.commit()
    # session.close()
    parser = SqlParser()

    query = "SELECT borz FROM Worser w1, Worser w, Final AS f,\ncast_info, Bazor WHERE a in ('a', 'b');"  # WHERE borz > 10 AND w.x > f.test OR f(10, a) LIKE 'TEST%%' AND a AND w.x IN ('a', 'b', 42) GROUP BY w.y1, w.x HAVING AVG(w.x) > 5; "

    tree, alias_mapping = parser.parse(query)

    print(tree)