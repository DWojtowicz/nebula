from sly import Lexer

class SqlLexer(Lexer) :
    """Generates a list of tokens from a query.

    Written by following the tutorial at https://sly.readthedocs.io/en/latest/sly.html#writing-a-lexer

    Attributes :
        tokens  The set of tokens
        ignore  The characters to be considered as skipable
    """
    tokens = {NAME, STRING,
              AS, EXCEPT, FROM, GROUP_BY, HAVING, INTERSECT, IS, MINUS, NATURAL_JOIN, NULL, SELECT, UNION, WHERE,
              BWISE_AND, BWISE_NOT, BWISE_OR, BWISE_XOR, DIVIDE, SUBSTRACT, MODULO, PLUS, STAR,
              EQ, NE_A, NE_B, GE, GT, LE, LT,
              ALL, AND, ANY, BETWEEN, EXISTS, IN, LIKE, NOT, OR, SOME,
              COMMA, DOT, SEMICOL,
              DECIMAL}


    ignore = " \t\n\r"
    ignore_comment = r"(--.*)|(\/\*((?!\*\/)(.|\n|\r))*\*\/)" # See https://regexr.com/541s5

    literals = {"(", ")"}

    #
    # Case-insensitive keywords
    # TODO find a way to generate the following functions from a list of strings
    #

    @_(r"(a|A)(l|L)(l|L)")
    def ALL(self, t) :
        t.value = "ALL"
        return t

    @_(r"AND")
    def AND(self, t):
        t.value = "AND"
        return t

    @_(r"(a|A)(s|S)")
    def AS(self, t):
        t.value = "AS"
        return t

    @_(r"(b|B)(e|E)(t|T)(w|W)(e|E)(e|E)(n|N)")
    def BETWEEN(self, t) :
        t.value = "BETWEEN"
        return t

    @_(r"(e|E)(x|X)(c|C)(e|E)(p|P)(t|T)")
    def EXCEPT(self, t):
        t.value = "EXCEPT"
        return t


    @_(r"(e|E)(x|X)(i|I)(s|S)(t|T)(s|S)")
    def EXISTS(self, t):
        t.value = "EXISTS"
        return t

    @_(r"FROM")
    def FROM(self, t):
        t.value = "FROM"
        return t

    @_(r"(g|G)(r|R)(o|O)(u|U)(p|P)\s(b|B)(y|Y)")
    def GROUP_BY(self, t):
        t.value = "GROUP BY"
        return t

    @_(r"(h|H)(a|A)(v|V)(i|I)(n|N)(g|G)")
    def HAVING(self, t):
        t.value = "HAVING"
        return t

    @_(r"IN")
    def IN(self, t):
        t.value = "IN"
        return t

    @_(r"(i|I)(n|N)(t|T)(e|E)(r|R)(s|S)(e|E)(c|C)(t|T)")
    def INTERSECT(self, t):
        t.value = "INTERSECT"
        return t

    @_(r"IS")
    def IS(self, t):
        t.value = "IS"
        return t

    @_(r"(l|L)(i|I)(k|K)(e|E)")
    def LIKE(self, t):
        t.value = "LIKE"
        return t

    @_(r"(m|M)(i|I)(n|N)(u|U)(s|S)")
    def MINUS(self, t):
        t.value = "MINUS"
        return t

    @_(r"(n|N)(a|A)(t|T)(u|U)(r|R)(a|A)(l|L)\s(j|J)(o|O)(i|I)(n|N)")
    def NATURAL_JOIN(self, t):
        t.value = "NATURAL JOIN"
        return t

    @_(r"NOT")
    def NOT(self, t):
        t.value = "NOT"
        return t

    @_(r"NULL")
    def NULL(self, t):
        t.value = "NULL"
        return t

    @_(r"(o|O)(n|N)")
    def ON(self, t):
        t.value = "ON"
        return t

    @_(r"OR")
    def OR(self, t):
        t.value = "OR"
        return t

    @_(r"(s|S)(e|E)(l|L)(e|E)(c|C)(t|T)")
    def SELECT(self, t):
        t.value = "SELECT"
        return t

    @_(r"(s|S)(o|O)(m|M)(e|E)")
    def SOME(self, t):
        t.value = "SOME"
        return t

    @_(r"(u|U)(n|N)(i|I)(o|O)(n|N)")
    def UNION(self, t):
        t.value = "UNION"
        return t

    @_(r"(w|W)(h|H)(e|E)(r|R)(e|E)")
    def WHERE(self, t):
        t.value = "WHERE"
        return t

    #
    # Simple tokens
    #

    STRING = r"\'([^\']*(\'\')?)*\'"  # See https://regexr.com/53l4k

    # Tokens for identifiers and keywords
    NAME = r"[a-zA-Z_][a-zA-Z0-9_]*"

    BWISE_AND = "&"
    BWISE_NOT = "~"
    BWISE_OR = "\|"
    BWISE_XOR = "\^"

    DIVIDE = r"/"
    SUBSTRACT = r"-"
    MODULO = r"%"
    PLUS = r"\+"
    STAR = r"\*"

    EQ = r"="
    NE_A = r"<>"
    NE_B = r"!="
    GE = r">="
    GT = r">"
    LE = r"<="
    LT = r"<"

    COMMA = r","
    DOT = r"\."
    SEMICOL = r";"

    def __init__(self):
        self.nesting_level = 0  # Counts the depth of parenthesis-enclosed expressions.
        self.line_count = 1 # Counts the number of plan_lines. Useful to print error messages.

    # Complex tokens
    @_(r"\d+[\.\d+]?")
    def DECIMAL(self, t):
        """Return the decimal number of the token, whether it is an integer or a float"""
        try :
            if t.value.startswith('0x'):
                t.value = int(t.value[2:], 16)
            else:
                t.value = int(t.value)
        except ValueError :
            t.value = float(t.value)

        return t

    # Literals
    @_(r"\(")
    def lpar(self, t):
        t.type = "("  # Set token type to the expected literal
        self.nesting_level += 1
        return t

    @_(r"\)")
    def rpar(self, t):
        t.type = ")"  # Set token type to the expected literal
        self.nesting_level -= 1
        return t

"""
_KEYWORDS = ["WHERE", "IS NOT NULL"]

for k in _KEYWORDS :
    @_(r"".join([f"({c.lower()}|{c.upper()})" if c != " " else "\s+" for c in k]))
    def func(cls, t) :
        t.type = k.replace(" ", "_")
        return t

    setattr(SqlLexer, k.replace(" ", "_"), classmethod(func))
"""

if __name__ == '__main__':
    query = "sElEcT x, (y+z), 1.3 \nFROM a, b WHERE /* Why ABBA? */ z = 'ABBA''s dance' AND x > 10 AND (y < 10 OR y > 12) AND y IS NOT NULL GROUP BY x, y, z HAVING AVG(z) > 10; -- a useless comment"
    lexer = SqlLexer()
    for t in lexer.tokenize(query) :
        print(t)