# Simulated provider API



## Configuration file

A provider can be configured using a JSON file (see example below). The following keys must be defined :

- `PROV_NAME` (str): A display name for the provider (It can be whatever you want, just don't have two providers with
  the same name)
- `FLASK_PORT` (inr): The port used by the flask app. GUI & API will be accessible at `localhost:$FLASK_PORT`.
- `DB_NAME` (str): The DB name you want to use that is available on your PostgreSQL instance.
- `DB_PORT` (int): The port PostgreSQL is listening to. Default is `5432`, it is the `port` key in
  your `postgresql.conf` file.
- `DB_HOST` (str): URL where your PostgreSQL instance is available. When running locally, tt is `localhost`. When
  running remotely, well, you should know your server's URL.
- `DB_USER` (str): PostgreSQL username. Must have admin rights. For experimental purposes, I just use the `postgres`
  user.
- `DB_PASSWD` (str): Aforementioned username's password. For experimental purposes, I use the default password.
- `HOST_URL` (str): Present provider's URL.
- `OPENED_FILE_PATH` (str): Directory containing file server's data (defaults to `/root/nebula/nebula/provider/static`).
- `POSTGRESQL_PATH` (str): Driectory containing postgres' data (defaults to `/var/lib/postgresql`).
- `TIMEOUT` (int): A timeout for queries execution in ms.
- `COST_FACTOR` (float) : Value used to compute execution time from DBMS cost. Default is 255.0.
- `PRICES` (dict): A dictionary containing the prices the provider applies :
  - `storage` (float): Storage fees (in €/GB, no time dimension, see explanation in the paper)
  - `query` (float): Querying fees (in €/GB, depends on the total query's input size)
  - `export` (float): Export fees (in €/GB)

Here is an example, for a provider named "Prov1" available at port `9991` hosting a DB named `local_1` available
at `localhost:5432` through a user `postgres` whose password is `postgres`. The provider bills 3.5 € per GB of data you
want to store, 1.75 € per GB of input data for querying and 8.5 € per GB for exporting data.

```json
{
  "PROV_NAME": "Prov1",
  "FLASK_PORT": 9991,
  "DB_NAME": "local_1",
  "DB_PORT": 5432,
  "DB_HOST": "localhost",
  "DB_USER": "postgres",
  "DB_PASSWD": "postgres",
  "HOST_URL": "localhost",
  "OPENED_FILE_PATH": "/root/nebula/nebula/provider/static",
  "POSTGRESQL_PATH": "/var/lib/postgresql",
  "TIMEOUT": 60000,
  "COST_FACTOR": 255.0,
  "PRICES": {
    "storage": 3.5,
    "query": 1.75,
    "export": 8.5
  }
}
```

_Note: File [`nebula/provider/conf_template.json`](nebula/provider/conf_template.json) contains values I use to deploy a
provider on the [Grid'5000](https://www.grid5000.fr/) computing platform._

## Running a provider

In the `nebula` directory you cloned :

```shell
bash provider.sh conf.json
```

Following the previous example, you should now be able to access the GUI of that provider in your browser
at [`http://localhost:9991/`](http://localhost:9991/).

## API documentation

### `GET` > `/`

Returns the homepage

### `GET` > `/schema`

Returns the relational schema in a JSON file structured as a dictionary, whose keys are relations' name and whose values
are dictionaries listing their attributes.
<details>
The latter store the following information:

- `type` (str): The relations' value of `table_type` in
  PostgreSQL's [information schema (`tables`)](https://www.postgresql.org/docs/current/infoschema-tables.html).
- `attributes`: A list of dictionaries, one item per attribute in the relation, structured as follows:
  - `name` (str): The attribute's name.
  - `data_type` (str): The type of the attribute from `data_type` in
    PostgreSQL's [information schema (`columns`)](https://www.postgresql.org/docs/current/infoschema-columns.html).

Here is an example of the schema returned:

```json
{
  "relation_1": {
    "type": "TABLE",
    "attributes": [
      {
        "name": "attr_1",
        "data_type": "integer"
      }
    ]
  }
}
```

</details>

### `GET` > `/pricing`

Returns the prices as a dict in the same format as the `PRICES` entry in the configuration file.

### `POST` > `/estimate`

### `POST` > `/view_estimation`

### `POST` > `/execute`

### `POST` > `/admin`

### `POST` > `/dump_table`

Dumps a table into a file accessible via HTTP.

Arguments :
- `table_name` (str): The table name to store in a file `table_name.dump`.

The file will be stored into `/var/www/http` and will therefore be accessible at  
`http://DB_HOST/table_name.dump`.

### `POST` > `/delete_table_file`

Deletes a previously dumped table.

Arguments :
- `table_name` (str)

### `POST` > `/load_table`

Loads a table dumped by another provider. This is a workaround to the slow `CREATE TABLE ... AS SELECT *` feasible with 
foreign tables embedded into PostgreSQL.

Arguments :
- `table_name` (str):
- `origin_provider_url` (str): URL of the origin provider.
- `origin_provider_name`(str): name of the origin provider.
- `origin_provider_port`(str): port of the origin provider.


### `POST` > `/get_dump_file`

Gets the file for a given table_name. 

Arguments :
- `table_name` (str):