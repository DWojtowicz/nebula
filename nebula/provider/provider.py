"""Nebula simuated provider Flask app

This app emulates a cloud provider that would allow query estimations and
billed query execution.
"""
import argparse
import datetime
import json
import logging
import os

import sys

from nebula.utils import get_commit_message

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)-5s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

import flask
import psycopg
import psycopg.errors as bderr
import requests
from flask import jsonify, render_template, request, send_from_directory

from nebula.compiler.exceptions import SQLSyntaxError
from nebula.compiler.parser import SqlParser
from nebula.database.utils import estimation_info, relation_size, object_size_function, parse_plan

# Arugument parsing
parser = argparse.ArgumentParser(description="Provider interface for Nebula prototype.")
parser.add_argument("--conf", default="./conf_template.json", help="Path to JSON conf file")
args = parser.parse_args()

# Load configuration
with open(args.conf) as f:
    CONF = dict(json.load(f))

OPENED_FILE_PATH = CONF["OPENED_FILE_PATH"]
POSTGRESQL_PATH = CONF["POSTGRESQL_PATH"]
PROV_NAME = CONF["PROV_NAME"]
FLASK_PORT = CONF["FLASK_PORT"]
DB_NAME = CONF["DB_NAME"]
DB_PORT = CONF["DB_PORT"]
DB_HOST = CONF["DB_HOST"]
DB_USER = CONF["DB_USER"]
DB_PASSWD = CONF["DB_PASSWD"]
HOST_URL = CONF["HOST_URL"]
FAIRES = CONF["PRICES"]
TIMEOUT = CONF["TIMEOUT"]
COST_FACTOR = CONF["COST_FACTOR"]


# Setup DB connection
def make_connection():
    """Timeout in ms"""
    global DB_NAME
    global DB_HOST
    global DB_PORT
    global DB_USER
    global DB_PASSWD
    global TIMEOUT
    conn = psycopg.connect(dbname=DB_NAME, user=DB_USER, password=DB_PASSWD, port=int(DB_PORT), host=DB_HOST,
                           options=f'-c statement_timeout={TIMEOUT}')  # In ms
    logging.info(f"Made connection for {DB_NAME} as {DB_USER} on {DB_HOST}:{DB_PORT}")
    return conn


CONNECTION = make_connection()

# Adding necessary `object_size` function in the DBMS
object_function_sql = object_size_function()
cur = CONNECTION.cursor()
cur.execute(object_function_sql)
cur.close()

#
# Faires declaration
#

# Queries history
QUERY_HIST = dict()
QUERY_NUMBER = 0

# Views size
VIEWS_SIZE = dict()

# LOAD USEFUL QUERY
with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), "get_schema.sql"), 'r') as f:
    SCHEMA_QUERY = "".join(f.readlines())

#
# App declaration
#

app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route('/', methods=['GET'])
def home():
    global PROV_NAME, DB_NAME, FAIRES

    commit_nb, commit_msg = get_commit_message()
    files = {
        f.split(".")[0] : f"http://{HOST_URL}/{f}" for f in os.listdir(OPENED_FILE_PATH) if f.endswith(".dump")
    }

    return render_template(
        "welcome.html",
        DB_NAME=DB_NAME,
        PROV_NAME=PROV_NAME,
        FAIRES=FAIRES,
        COMMIT_NB=commit_nb,
        COMMIT_MSG=commit_msg,
        FILES=files
    )

def format_table_type(table_type):
    if "FOREIGN" in table_type:
        return "FOREIGN TABLE"
    elif "BASE" in table_type:
        return "TABLE"
    else:  # It's a VIEW here
        return table_type

def __extract_schema():
    global SCHEMA_QUERY
    schema = dict()
    conn = make_connection()
    cursor = conn.cursor()
    logging.info("Ready to extract schema")

    cursor.execute(SCHEMA_QUERY)

    for e in cursor.fetchall():
        attr = {
            "name": e[1],
            "data_type": e[3],
            "primary_key": e[4]
        }
        try:
            schema[e[0]]["attributes"].append(attr)
        except KeyError:
            schema[e[0]] = {
                "attributes": [attr],
                "type": format_table_type(e[2])
            }

    cursor.close()
    conn.close()

    return schema


@app.route("/schema", methods=["GET"])
def extract_schema():
    """
    Returns the relational schema as a JSON file. Keys are the relations and the lists contains the attributes.
    """
    return jsonify(__extract_schema())


@app.route("/pricing", methods=["GET"])
def pricing_policy():
    return jsonify(FAIRES)


@app.route("/estimate", methods=["POST"])
def estimate():
    """
    query: str
        SQL command

    quer_name: str
        Alias for results

    cost_minus: str
        PostgreSQL costs to substract
    :return:
    """
    global FAIRES
    global QUERY_HIST
    global VIEWS_SIZE
    global COST_FACTOR
    CONNECTION = make_connection()

    query = request.form["query"]
    try:
        query_name = request.form["query_name"]
        if query_name == "":
            query_name = query
    except:
        query_name = query

    try:
        cost_minus = request.form["cost_minus"]
    except KeyError:
        cost_minus = 0

    parser = SqlParser()
    _, query_tables = parser.parse(query)  # Returns 400 on parsing error

    # Extracting input tables
    query_tables = [f"{query_tables[k]}" for k in query_tables]

    # Checking if query tables exists. Returns 400 if not.
    diff = set(query_tables) \
           - set(json.loads(extract_schema().data).keys())

    if diff:
        raise SQLSyntaxError(f"Table{'' if len(diff) == 1 else 's'} {', '.join(diff)} unknown.")

    # Extracting input views size
    size = sum(VIEWS_SIZE[k] for k in query_tables if k in VIEWS_SIZE.keys())

    # Computing input size and response time estimation
    size += relation_size(CONNECTION, query_tables)
    river_X = estimation_info(query, CONNECTION, COST_FACTOR)
    logging.info(f"Estimation: {river_X}")
    logging.info(f"Cost minus: {cost_minus}")
    river_X["cost"] = river_X["cost"] - cost_minus
    rows = river_X["rows"]
    width = river_X["width"]
    output_size = river_X["output_size"]

    quotation = {
        "query_name": query_name,
        "query": {
            "price": size * FAIRES["query"],
            "input_size": size,
            "width": width,
            "est_output_size": output_size,
            "est_time": river_X["cost"],
            "est_card": rows,
            "storage_cost": output_size * FAIRES["storage"],
            "export_cost": output_size * FAIRES["export"],
            "sql": query,
            "pg_cost": river_X["cost"],
            "pg_total_cost": river_X["cost"] + cost_minus,
            "pg_plan_depth": river_X["depth"],
            "pg_join_count": river_X["joins"]
        }
    }
    QUERY_HIST[query_name.casefold().strip()] = quotation
    QUERY_HIST[f"{query_name}_tmp".casefold().strip()] = quotation
    QUERY_HIST[query.casefold().strip()] = quotation

    logging.info(f"QUERY {query_name} estimated.")

    CONNECTION.close()
    return jsonify(quotation)


def prepare_query(q: str) -> str:
    q = q.casefold()
    q_1, q_2 = q.split("from")
    q_1 = f"{q_1} FROM "

    try:
        q_2, q_3 = q_2.split("where")
        q_3 = f" WHERE {q_3}"
    except ValueError:
        try:
            q_2, q_3 = q_2.split("group by")
            q_3 = f" GROUP BY {q_3}"
        except ValueError:
            q_3 = ";"

    rels = [f"public.{e.strip()}" for e in q_2.split(',')]

    return f"{q_1} {', '.join(rels)} {q_3}"


@app.route("/execute", methods=["POST"])
def execute():
    """
    Params
    ------
    query : str (Optionnal if query_name already used for estimation)
        SQL code for the query

    pricing : str
        Faires

    mode : str
        Either 'storage' or 'export'

    query_name : str (Optionnal)
        Name of the table where results will be stored

    substitute_name : str (Optionnal)
        Query name used for estimates

    """
    """Runs the query and computes its costs"""
    global QUERY_NUMBER
    global FAIRES
    global QUERY_HIST
    global COST_FACTOR

    # Defining this query's name
    query_name = (QUERY_NUMBER := QUERY_NUMBER + 1)
    query_name = f"res_query_{query_name}"


    # Getting querying price
    query = request.form["query"]
    mode = request.form["mode"]
    if request.form["query_name"] != "":
        query_name = request.form["query_name"]

    # Alias for tmp tables at execution
    substitute_name = query_name
    if request.form["substitute_name"] != "":
        substitute_name = request.form["substitute_name"]

    logging.info(f"Received query for execution: {query}")

    # Retrieving quotation for this query
    try:
        base_price = QUERY_HIST[substitute_name.casefold().strip()]
    except KeyError:
        try:
            base_price = QUERY_HIST[query_name.casefold().strip()]
        except KeyError:
            try:
                base_price = QUERY_HIST[query.casefold().strip()]
            except KeyError:
                raise NoQuotationError(substitute_name)

    # Preparing the query
    query = prepare_query(query)
    logging.info(f"Query reformatted: {query}")
    CONNECTION = make_connection()
    river_point = estimation_info(query, CONNECTION, COST_FACTOR)
    CONNECTION.close()
    logging.info(f"Built river datapoint {river_point}")

    # Query execution

    CONNECTION = make_connection()
    cursor = CONNECTION.cursor()
    q = f"DROP TABLE IF EXISTS {query_name};"
    cursor.execute(q)
    cursor.close()


    if mode == "storage":
        q = f"CREATE UNLOGGED TABLE {query_name} WITH (autovacuum_enabled=false) AS {query}"
    elif mode == "export" :
        q = query
    else :
        raise SQLSyntaxError(f"Mode '{mode}' unknown. Please choose between 'export' and 'storage'.")

    # Getting plan data
    start = datetime.datetime.now()

    try:
        logging.info(f"Executing {q}")
        cursor = CONNECTION.cursor()
        cursor.execute(f"EXPLAIN (ANALYZE, FORMAT JSON) {q}")
        plan_lines = cursor.fetchall()
    except psycopg.errors.SyntaxError as e:
        raise SQLSyntaxError(str(e))
    except psycopg.errors.UndefinedColumn as e:
        raise SQLSyntaxError(str(e))
    else :
        ex_time = datetime.datetime.now() - start


        if mode == "export":
            data = cursor.fetchall()
            data = [[e for e in row] for row in data]
            result_size = sys.getsizeof(data) * 1e-8
            data = "\n".join(["\t".join([str(e) for e in row]) for row in data])
        else:
            result_size = relation_size(CONNECTION, [f"{query_name}"])
    finally:
        cursor.close()

    CONNECTION.commit()
    CONNECTION.close()

    # Parsing the plan
    res = parse_plan(plan_lines, details=True)

    base_price["name"] = query_name
    base_price["query"]["time"] = res[-1]["actual_time"]
    base_price["query"]["old_output_size"] = res[-1]["rows"]
    base_price["query"]["output_size"] = res[-1]["output_size"]
    base_price["query"]["real_card"] = res[-1]["actual_rows"]
    base_price["query"]["export_cost"] = result_size * FAIRES["export"]
    base_price["execution_plan_stats"] = res

    # Price computation
    base_price[mode] = {
        "price": result_size * FAIRES["storage"],
        "size": result_size
    }
    base_price["total_price"] = base_price["query"]["price"] + base_price[mode]["price"]
    if mode == "export":
        base_price["data"] = data

    return jsonify(base_price)

@app.route("/view_estimation", methods=["POST"])
def view_estimation() :
    """Return the estimated size for the content of a view"""
    global VIEWS_SIZE
    global FLASK_PORT

    view_name = request.form["view_name"]
    try:
        query = request.form["query"]
    except KeyError:
        query = f"SELECT * FROM {view_name} ;"

    message = requests.post(
        f"http://localhost:{str(FLASK_PORT)}/estimate",
        data={"query": query}
    )
    message = json.loads(message.content)
    logging.info(f"Received estimation for view {view_name} : {message}")
    est = message["query"]["est_output_size"]
    VIEWS_SIZE[view_name] = est
    return jsonify({view_name: est})

@app.route("/admin", methods=["POST"])
def admin():
    global VIEWS_SIZE

    query = request.form["query"]

    # Case when data about views size should be passed
    try :
        views_info = request.form["views_info"]
        views_info = eval(views_info)

        for k in views_info :
            VIEWS_SIZE[k] = views_info[k]
    except KeyError :
        pass

    CONNECTION = make_connection()
    cursor = CONNECTION.cursor()
    try:
        cursor.execute(query)
        logging.info(f"Admin query {query} returned {cursor.statusmessage}")

        res = {
            "status_code": 201
        }
        cursor.close()
        CONNECTION.commit()
    except Exception as e:
        res = {
            "status_code": 500,
            "details": f"{type(e)} -- {str(e)} -- {query}"
        }
        logging.warning(f"Admin query {query} failed : {res}")
        cursor.close()

    finally:
        CONNECTION.close()
        return jsonify(res)


@app.route("/object_size", methods=["POST"])
def object_size():
    """Returns the size of an object in the DB."""
    object_name = request.form["object_name"]

    connection = make_connection()
    size = relation_size(connection, object_name)
    connection.close()

    res = {
        "object_name": object_name,
        "size": size
    }
    return jsonify(res)

@app.route("/get_dump_file", methods=["POST"])
def get_dump_file():
    table_name = request.form["table_name"]
    return send_from_directory("static", f"{table_name}.dump")

@app.route("/dump_table", methods=["POST"])
def dump_table():
    table_name = request.form["table_name"]
    logging.info(f"Asked to dump {table_name}")

    if f"{table_name}.dump" in os.listdir(OPENED_FILE_PATH):
        os.remove(f"{OPENED_FILE_PATH}/{table_name}.dump")
        logging.info(f"Cleared the way for {table_name}")

    logging.info(f"Dumping {table_name}")
    """
    connection = make_connection()
    cur = connection.cursor()
    cur.execute(f"COPY {table_name} TO '{POSTGRESQL_PATH}/{table_name}.dump' WITH (FORMAT 'binary');")
    cur.close()
    os.system(f"mv {POSTGRESQL_PATH}/{table_name}.dump {OPENED_FILE_PATH}/{table_name}.dump")
    connection.close()
    """
    os.system(f"pg_dump imdbload -U postgres -w -F c -Z 9 -t {table_name} -f {POSTGRESQL_PATH}/{table_name}.dump")
    os.system(f"mv {POSTGRESQL_PATH}/{table_name}.dump {OPENED_FILE_PATH}/{table_name}.dump")
    logging.info(f"Dumping done for {table_name}")

    return jsonify({
        "filename": "{table_name}.dump"
    })


@app.route("/delete_table_file", methods=["POST"])
def delete_table_file():
    table_name = request.form["table_name"]
    os.remove(f"{OPENED_FILE_PATH}/{table_name}.dump")
    logging.info(f"Deleted file {OPENED_FILE_PATH}/{table_name}.dump")

    return jsonify({
        "info": "OK, I'm doing this!"
    })


@app.route("/load_table", methods=["POST"])
def load_table():
    table_name = request.form["table_name"]
    origin_provider_name = request.form["origin_provider_name"]
    origin_provider_url = request.form["origin_provider_url"]
    origin_provider_port = request.form["origin_provider_port"]

    # Downloading the file
    logging.info(f"Downloading table {table_name} from {origin_provider_name} (http://{origin_provider_url}:{origin_provider_port}/get_dump_file).")

    response = requests.post(
        f"http://{origin_provider_url}:{origin_provider_port}/get_dump_file",
        data={"table_name": table_name}
    )
    with open(f"{POSTGRESQL_PATH}/{table_name}.dump", "wb") as f:
        f.write(response.content)

    os.system(f"chown postgres {POSTGRESQL_PATH}/{table_name}.dump")

    # Loading up into the DB
    # https://www.postgresql.org/docs/current/sql-copy.html

    logging.info(f"Restoring {table_name}")
    os.system(f"pg_restore {POSTGRESQL_PATH}/{table_name}.dump -d imdbload -U postgres -w -v -j 16 -t {table_name} || true")
    """
    connection = make_connection()
    cur = connection.cursor()

    # Creating the schema
    cur.execute(
        f"IMPORT FOREIGN SCHEMA public LIMIT TO ({table_name}) FROM SERVER {origin_provider_name} INTO public;"
    )
    logging.info("Imported foreign table")
    cur.execute(f"ALTER FOREIGN TABLE {table_name} RENAME TO {table_name}_tmp;")
    logging.info("Renamed foreign table")
    cur.execute(f"CREATE TABLE {table_name} (LIKE {table_name}_tmp INCLUDING all);")
    logging.info("Created table schema")

    # Loading the data
    cur.execute(f"COPY {table_name} FROM '{POSTGRESQL_PATH}/{table_name}.dump' WITH (FORMAT 'binary');")
    logging.info("Loaded table content")

    cur.execute(f"DROP FOREIGN TABLE {table_name}_tmp;")
    logging.info("Dropped foreign table")
    cur.close()
    connection.commit()
    connection.close()
    """

    # Checking if the table is properly loaded
    connection = make_connection()
    cur = connection.cursor()

    # Creating the schema
    try :
        cur.execute(f"EXPLAIN SELECT * FROM {table_name};")

        res = {
            "info": f"Success!"
        }
        logging.info(f"Loading successful for {table_name}")

    except Exception as e:
        res = {
            "status_code": 500,
            "details": f"{type(e)} -- {str(e)}"
        }
        logging.error(f"Loading table {table_name} failed : {type(e)} -- {str(e)}")

    finally :
        cur.close()
        connection.commit()
        connection.close()

    # Cleaning when all is done
    logging.info(f"Cleaning after loading {table_name}")
    os.remove(f"{POSTGRESQL_PATH}/{table_name}.dump")

    return jsonify(res)


##########################################################################
@app.errorhandler(SQLSyntaxError)
def handle_invalid_usage(error):
    return {
        "status_code": 500,
        "details": str(error)
    }


@app.errorhandler(bderr.UndefinedColumn)
@app.errorhandler(psycopg.errors.AmbiguousColumn)
def handle_undefined_column(error):
    return {
        "status_code": 500,
        "details": str(error)
    }


@app.errorhandler(bderr.SyntaxError)
def handle_undefined_column(error):
    return {
        "status_code": 500,
        "details": str(error)
    }


class NoQuotationError(Exception):
    def __init__(self, qname):
        super().__init__(
            f"No quotation for this query have been generated. Please estimate it before executing it.\nQuery: {qname}")


@app.errorhandler(NoQuotationError)
def handle_no_estimation(error):
    return {
        "status_code": 412,
        "details": str(error)
    }

@app.errorhandler(FileNotFoundError)
@app.errorhandler(FileNotFoundError)
def handle_file_not_found(error):
    return {
        "status_code": 413,
        "details": str(error)
    }

def clean():
    """Deletes all temporary tables."""
    global CONNECTION
    global QUERY_NUMBER

    cursor = CONNECTION.cursor()

    cursor.execute(
        "SELECT DISTINCT table_name, table_type "
        "FROM information_schema.tables "
        "WHERE table_name LIKE 'msq_%' "
        "   OR table_name LIKE 'res_%' "
        "   OR table_name LIKE 'benchmark_%';"
    )
    for t in cursor.fetchall() :
        type = t[1]
        type = type.replace("BASE", "")
        if type == "FOREIGN" :
            type = f"{type} TABLE"

        cursor.execute(f"DROP {type} IF EXISTS {t[0]} CASCADE;")

    cursor.close()
    CONNECTION.commit()

#
# Run app
#
if __name__ == '__main__':
    clean()
    # from gevent import monkey # https://github.com/miguelgrinberg/Flask-SocketIO/issues/65#issuecomment-60795011
    # monkey.patch_all(select=False)
    app.run(port=FLASK_PORT, use_reloader=False, host='0.0.0.0')
    clean()  # Deletes all temporary tables created during the session
    CONNECTION.close()
