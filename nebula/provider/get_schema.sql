SELECT table_name, column_name, table_type, col_type, COALESCE(constraint_type = 'PRIMARY KEY', false) AS is_pk
FROM (
         SELECT isc.table_name  AS table_name,
                isc.column_name AS column_name,
                table_type,
                isc.data_type || CASE
                                     WHEN isc.character_maximum_length IS NOT NULL
                                         THEN '(' || isc.character_maximum_length || ')'
                                     ELSE ''
                    END AS col_type
         FROM information_schema.columns isc,
              information_schema.tables ist
         WHERE isc.table_name = ist.table_name
           AND isc.table_catalog = ist.table_catalog
           AND isc.table_schema = ist.table_schema
           AND ist.table_schema = 'public'
           AND isc.table_schema = 'public'
     ) attr_data
         LEFT OUTER JOIN (
    SELECT column_name, constraint_type
    FROM information_schema.key_column_usage iskcu,
         information_schema.table_constraints istc
    WHERE iskcu.constraint_name = istc.constraint_name
      AND iskcu.table_name = istc.table_name
      AND iskcu.table_schema = 'public'
      AND istc.table_schema = 'public'
      AND istc.constraint_type = 'PRIMARY KEY'
) primary_keys USING (column_name);