// Execution when DOM is loaded
$(document).ready(function() {

    // prepare Options Object
    var slaOptions = {
        target:     '#results',
        url:        "estimate",
        dataType:   'json',
        success:    function(responseText, statusText, xhr) {
            // Puts data from forms into an iframe
            $("#results h2 span").html( "<code>estimate</sla>" );
            var prettyJson = JSON.stringify(responseText, null, 4);
            prettyJson = prettyJson.replace(/\n/g, "<br />").replace(/\t/g, "&nbsp;&nbsp;&nbsp;&nbsp;");
            $("#results p").html(prettyJson);
        }
    };
    // pass options to ajaxForm
    $('#estimate').ajaxForm(slaOptions);

    var executeOptions = {
        target:     '#results',
        url:        "execute",
        dataType:   'json',
        success:    function(responseText, statusText, xhr) {
            // Puts data from forms into an iframe
            $("#results h2 span").html( "<code>execute</sla>" );
            var prettyJson = JSON.stringify(responseText, null, 4);
            prettyJson = prettyJson.replace(/\n/g, "<br />").replace(/\t/g, "&nbsp;&nbsp;&nbsp;&nbsp;");
            $("#results p").html(prettyJson);
        }
    };
    // pass options to ajaxForm
    $('#execute').ajaxForm(executeOptions);

});