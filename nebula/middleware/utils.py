import json
import logging
from concurrent.futures import ThreadPoolExecutor
from typing import Dict, Collection, Callable

import requests
import time

from nebula.database.metabase import Relation, Attribute, Provider
from nebula.database.utils import object_size_function


def wait_providers(providers) -> bool:
    pr = None
    try:
        for p in providers:
            pr = p
            requests.get(f"http://{providers[p]['url']}:{providers[p]['provider_port']}/")
            logging.info(f"Provider at {providers[p]['url']} is up.")
        return True
    except requests.exceptions.ConnectionError:
        logging.warning(f"Providers not ready yet ({pr})... Retrying !")
        time.sleep(10)

        return False


def __load_one_schema(providers, p, session):
    try:
        schema = requests.get(
            f"http://{providers[p]['url']}:{providers[p]['provider_port']}/schema")
        schema = json.loads(schema.content)
        logging.info(f"Schema fetched from {p}")

        faires = requests.get(
            f"http://{providers[p]['url']}:{providers[p]['provider_port']}/pricing")
        faires = json.loads(faires.content)
        logging.info(f"Prices fetched from {p}")

        rels = list()
        for r in schema:
            relation = Relation(name=r, rel_type=schema[r]["type"])
            relation.attributes = [
                Attribute(
                    name=a["name"],
                    data_type=a["data_type"],
                    primary_key=a["primary_key"]
                ) for a in schema[r]["attributes"]]
            rels.append(relation)

        prov = Provider(
            name=p,
            url=providers[p]['url'],
            port=providers[p]['provider_port'],
            dbname=providers[p]['dbname'],
            export=faires["export"],
            query=faires["query"],
            storage=faires["storage"]
        )
        prov.relations = rels
        session.add(prov)
        session.commit()

        providers[p]["status"] = True
    except requests.exceptions.ConnectionError as e:
        # When the provider is not connected
        logging.error(f"Provider {p} not ready - {e}")
        raise e


def load_schema(providers, session):
    """Loads the schema of all the providers into the DB"""

    # This task is mostly bound to IO, so it's safe to create so much threads
    with ThreadPoolExecutor(len(providers)) as executor:
        for p in providers:
            future = executor.submit(__load_one_schema, providers, p, session)
            err = future.exception()
            if err:
                raise err

    return providers


def __one_provider_interlink(providers, p, object_function_sql):
    logging.info(f"Configuring provider {p}")

    # Create function object_size
    message = requests.post(
        f"http://{providers[p]['url']}:{providers[p]['provider_port']}/admin",
        data={"query": object_function_sql}
    )
    message = json.loads(message.content)
    if message["status_code"] != 201:
        logging.error(
            f"Could not create function object_size on {p} : {message['details']}")
        exit()

    # Create extension postgres_fdw
    message = requests.post(
        f"http://{providers[p]['url']}:{providers[p]['provider_port']}/admin",
        data={
            "query": "create extension if not exists postgres_fdw;"})
    message = json.loads(message.content)
    if message["status_code"] != 201:
        logging.error(f"Can't create extension on {p} : {message['details']}")
        exit()

    # Create servers
    for p2 in providers:
        # Skipping __providers not ready
        if providers[p]["status"] and p != p2:

            # Foreign data wrapper
            message = requests.post(
                f"http://{providers[p]['url']}:{providers[p]['provider_port']}/admin",
                data={
                    "query": f"create server if not exists {p2} " +
                             "foreign data wrapper postgres_fdw " +
                             f"options (" +
                                f"host '{providers[p2]['url']}', port '{providers[p2]['db_port']}', dbname '{providers[p2]['dbname']}', " +
                                 "fetch_size '10000', batch_size '10000'" +
                             ");"
                }
            )
            message = json.loads(message.content)
            if message["status_code"] != 201:
                logging.error(
                    f"On {p}, unable to create FDW for {p2} : {message['details']}")
                exit()

            # User mapping
            message = requests.post(
                f"http://{providers[p]['url']}:{providers[p]['provider_port']}/admin",
                data={
                    "query": f"create user mapping if not exists for postgres server {p2} options (user 'postgres', password 'postgres');"}
            )
            message = json.loads(message.content)
            if message["status_code"] != 201:
                logging.error(
                    f"On {p}, unable to create mapping for {p2} : {message['details']}")
                exit()

    logging.info(f"{p} successfully configured.")


def providers_interlink(providers):
    object_function_sql = object_size_function()

    # This task is mostly bound to IO, so it's safe to create so much threads
    #with ThreadPoolExecutor(len(providers)) as executor:
    #    for p in providers:
    #        future = executor.submit(__one_provider_interlink, providers, p, object_function_sql)
    #        err = future.exception()
    #        if err:
    #            raise err
    for p in providers :
        __one_provider_interlink(providers, p, object_function_sql)


class UnknownCostModel(Exception):
    def __init__(self, cost_model):
        super(UnknownCostModel, self).__init__(
            f"Cost model `{cost_model}` unknown."
        )


class UnknownExplorationStrategy(Exception):
    def __init__(self, cost_model):
        super(UnknownExplorationStrategy, self).__init__(
            f"Exploration strategy `{cost_model}` unknown."
        )


def check_param(form: Dict, field: str, default: str, values: Collection[str], exception: Callable) -> str:
    """Parameter checking for optional values."""
    try:
        val = form[field]
    except KeyError:
        val = default
    else:
        if val not in values:
            raise exception(val)
    return val
