"""Nebula middleware Flask app"""
import csv
import datetime
import logging
import os.path
import pickle
from argparse import ArgumentParser
from threading import BrokenBarrierError
from typing import Callable

import flask
import networkx as nx
import requests
import werkzeug
from flask import jsonify, render_template, request, send_from_directory
from werkzeug.utils import secure_filename

from nebula.compiler.parser import SqlParser
from nebula.cost_models.comparison_cost_models import LiteratureCostModel, StackedCostModel, \
    PostprocessedStackedCostModel
from nebula.cost_models.cost_model import NebulaCostModel
from nebula.database.metabase import Provider, Session, dbname
from nebula.database.utils import AdminError
from nebula.engine.exhaustive.dynamic_execution import DynamicEngine, ReasoningError
from nebula.engine.exhaustive.static_execution import StaticEngine
from nebula.engine.query.query_builder import QueryBuilder, UnknownRelationError
from nebula.engine.quotation import Quotation
from nebula.engine.randomised.static_execution import StaticRandomisedExecution
from nebula.engine.utils import to_plantuml

from nebula.middleware.utils import load_schema, providers_interlink, wait_providers, UnknownCostModel, \
    UnknownExplorationStrategy, check_param
from nebula.engine.query.query import Dict, disable_memory_control

from nebula.utils import get_commit_message

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(levelname)-5s %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)

# Starting the app

if __name__ == "__main__":
    app = flask.Flask(__name__)
    app.config["DEBUG"] = True
    app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:///{dbname}?check_same_thread=False"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["ALLOWED_EXTENSIONS"] = {'pickle'}

# Connect to Database and create database session

# app.config['SQLALCHEMY_DATABASE_URI'] = f"sqlite:///{__name__}_schema.db"


parser = ArgumentParser()
parser.add_argument(
    "--g5k_prov_list",
    required=False,
    default="http://public.grenoble.grid5000.fr/~dwojtowicz/machines",
    help="Provider list on Grid'5000.")
parser.add_argument(
    "--prov_list",
    required=False,
    default="",
    help="A list of providers")
parser.add_argument("--bypass_config", action="store_true",
                    help="Bypasses the provider configuration, assuming it was done at least once.")
parser.add_argument("--let_errors", action="store_true",
                    help="Throws raw Python exceptions to the users. Use for devlopment and debug purpose only.")
parser.add_argument("--disable_memory_control", action="store_true",
                    help="Disables the memory control mechanism that avoids OOM to unleash its wrath.")
args = parser.parse_args()

if args.disable_memory_control:
    disable_memory_control()

if args.prov_list:
    logging.info("Loading providers from file")
    with open(args.prov_list, mode='r') as file:

        # reading the CSV file
        csvFile = csv.reader(file, delimiter="\t")
        __providers = {
            line[0]: {
                "url": line[1],
                "provider_port": line[2],
                "dbname": line[3],
                "db_port": line[4]
            }
            for line in csvFile
        }

else:
    logging.info("Loading providers from g5k")
    if args.g5k_prov_list.startswith("http") :
        machines = requests.get(args.g5k_prov_list).text
        __providers = {
            e.split("-")[0]: {"url": e, "provider_port": 4242, "dbname": "imdbload", "db_port": 5432}
            for e in set(machines.strip().split("\n"))
        }
    else :
        with open(args.g5k_prov_list, 'r') as f :
            machines = {l.strip().replace("\n", "").replace("\r", "") for l in f.readlines()}
        __providers = {
            line.split("-")[0]: {"url": line, "provider_port": 4242, "dbname": "imdbload", "db_port": 5432}
            for line in machines
        }

logging.info(f"Providers: {__providers}")

session = Session

# Waiting for providers to be available
while not wait_providers(__providers): pass;

# Loading provider's schema
__providers = load_schema(__providers, session)

PROVIDERS = session.query(Provider).all()
# Database scheme loaded

logging.info(f"Providers conf: {__providers}")

# Setting up links between providers
if not args.bypass_config :
    providers_interlink(__providers)
else:
    logging.info("Bypassing providers' configuration")

# Query history
HISTORY = dict()

sess = Session
one_hot_size = 0

# Cost model
COST_MODELS = {
    "vanilla": NebulaCostModel(__providers.keys()),
    "literature": LiteratureCostModel(__providers.keys()),
    "stacked": StackedCostModel(__providers.keys()),
    "stacked_postprocessed": PostprocessedStackedCostModel(__providers.keys())
}

#
# App declaration
#

app = flask.Flask(__name__)
app.config["DEBUG"] = True

logging.info(f"Ready to run app.")


@app.route('/', methods=['GET'])
def home():
    """Returns the home page"""
    global PROVIDERS
    commit_nb, commit_msg = get_commit_message()
    return render_template("welcome.html", PROVIDERS=PROVIDERS, COMMIT_NB=commit_nb, COMMIT_MSG=commit_msg)


def __qname_from_request(request):
    """Gets a query name from a request.

    When the `query_name` field is missing, the query becomes
    its own name.
    """
    try:
        query_name = request.form["query_name"]
    except werkzeug.exceptions.BadRequestKeyError:
        query_name = ""

    if query_name.strip() == "":
        query_name = request.form["query"]

    return query_name


def response_time(G, n):
    """Computes the response time of a Quotation"""
    paths = list()
    for p in G.nodes:
        if len(list(G.predecessors(p))) == 0:
            paths.append(
                nx.shortest_path_length(
                    G,
                    p,
                    n,
                    weight="total_time") +
                G.nodes[n]["tender"].estimated_execution_time)
    return max(paths)


@app.route("/estimate", methods=['POST'])
def estimate():
    """Computes a Quotation for a given query

    Parameters
    ----------
        query : str
            SQL code for the query
        query_name : str
            Alias for the results. Must be compliant with SQL table names spec.
        cost_model : str (Default : "stacked")
            The cost model to use.
            Can be "vanilla" or "stacked".

        exploration_strategy : str (Default : "exhaustive")
            The exploration strategy to use.
            Can be "exhaustive" or "randomised"

        nb_iterations : int (Default: 1)
            Number of hypergraph expansions tolerated when finding a local minimum

        max_size_hypergraph : int (Default: 100)
            Max number of neighbours considered

        max_depth : int (Default: 16)
            Max number of neighbours layers from the initial plan

        no_history : bool (Default: False)
            Keeps or not the quotations history

    """
    global HISTORY
    global COST_MODELS

    query = request.form["query"]
    query_sql = query

    cost_model = check_param(
        request.form, "cost_model",
        "stacked", ("stacked", "stacked_postprocessed", "literature", "vanilla"),
        UnknownCostModel
    )
    exploration_strategy = check_param(request.form, "exploration_strategy",
                                       "exhaustive", ("exhaustive", "randomised"),
                                       UnknownExplorationStrategy
                                       )

    try :
        nb_iterations = int(request.form["nb_iterations"])
    except :
        nb_iterations = 1

    try :
        max_size_hypergraph = int(request.form["max_size_hypergraph"])
    except :
        max_size_hypergraph = 100

    try :
        max_depth = int(request.form["max_depth"])
    except :
        max_depth = 16

    try :
        # WTF! `bool("False")` returns `True`! ... JS, is that you??
        if request.form["no_history"] in ("on", "True") :
            no_history = True
        elif  request.form["no_history"] in ("off", "False") :
            no_history = False
        else:
            logging.warning(f"Wrong input for no_history: {request.form['no_history']}")
            raise Exception()
    except :
        no_history = False

    try :
        # WTF! `bool("False")` returns `True`! ... JS, is that you??
        if request.form["disable_cost_model"] in ("on", "True") :
            disable_cost_model = True
        elif  request.form["disable_cost_model"] in ("off", "False") :
            disable_cost_model = False
        else:
            logging.warning(f"Wrong input for disable_cost_model: {request.form['disable_cost_model']}")
            raise Exception()
    except :
        disable_cost_model = False

    # When the query name is not properly defined, the query is its own name
    query_name = __qname_from_request(request)

    # Log
    logging.info(f"Estimation demand for {query_name} -- {query}")
    logging.info(f"Parameters for {query_name}: cost_model ({cost_model}) exploration_strategy ({exploration_strategy}) nb_iterations ({nb_iterations}） max_size_hypergraph ({max_size_hypergraph}) max_depth ({max_depth}) no_history ({no_history})")

    start = datetime.datetime.now()

    # Parsing the query
    parser = SqlParser()
    tree, alias_mapping = parser.parse(query)

    # Building the query's execution graph
    logging.info(f"Tree for {query_name} : {tree}")

    bldr = QueryBuilder(tree, alias_mapping, query_name, COST_MODELS[cost_model], exploration_strategy,
                        nb_iterations, max_size_hypergraph, max_depth, disable_cost_model)
    query = bldr.build_query(__schema())
    graph = query.graph()

    # Computing the Quotation
    quotations: Dict[tuple | Callable, Quotation] = query.quotation()
    ex_time = datetime.datetime.now() - start
    clauses = query.get_clauses()

    # Quotation correction
    if not disable_cost_model :
        for q in quotations.values():
            q.total_cost, q.estimated_execution_time = COST_MODELS[cost_model].postprocess_query(
                q.to_dict())

    # quotations[1].response_time = response_time(graph, quotations[0])
    output = {
        "qname": query_name,
        "sla_generation_time": ex_time.total_seconds() * 1000,
        "clauses": str(clauses),
        "quotations": [{
            "monetary_cost": quotation.total_cost,
            "response_time": quotation.estimated_execution_time,
            "plan": str(k),
            "misc": quotation.to_dict()
        } for k, quotation in quotations.items()]
    }

    for k in quotations.keys():
        try :
            quotations[k].graph = graph.copy()
        except AttributeError :
            # exploration_strategy is randomised here
            quotations[k].graph = graph

    if not no_history :
        logging.info("Back up quotations in history")
        # Backup
        HISTORY[query_name] = dict()
        HISTORY[query_name]["tender"] = {str(k): v for k, v in quotations.items()}
        # Saving node key
        for k in quotations.keys():
            HISTORY[query_name]["tender"][str(k)].plan_node = k

        HISTORY[query_name]["graph"] = graph
        HISTORY[query_name]["query"] = query
        HISTORY[query_name]["sql"] = query_sql
        HISTORY[query_name]["cost_model"] = cost_model
        HISTORY[query_name]["exploration_strategy"] = exploration_strategy

    # Cleaning views in the provider
    query.clean_db()

    logging.info(f"Estimation done for {query_name} -- {output}")
    return jsonify(output)


def __schema() :
    session = Session
    output = dict()

    # Iterating over all providers schemas
    for p in session.query(Provider).all():
        schema = dict()
        for r in p.relations:
            schema[r.name] = [(a.name, a.data_type, a.primary_key) for a in r.attributes]

        output[p.name] = schema

    return output


@app.route("/schema", methods=['GET'])
def schema():
    """Returns the relational schema of each provider"""
    return jsonify(__schema())


@app.route("/cost_model_history", methods=['GET'])
def cost_model_history():
    """Returns the relational schema of each provider"""
    v_cm = COST_MODELS["vanilla"]
    l_cm = COST_MODELS["literature"]
    s_cm = COST_MODELS["stacked"]
    sp_cm = COST_MODELS["stacked_postprocessed"]
    data = {
        "literature": {
            "card": l_cm.card_history,
            "perf": l_cm.rt_history
        },
        "stacked": {
            k: {
                "card": v["cardinality"].history,
                "perf": v["response_time"].history
            } for k, v in s_cm.sub_query_models.items()
        },
        "stacked_postprocessed": {
            k: {
                "card": v["cardinality"].history,
                "perf": v["response_time"].history
            } for k, v in sp_cm.sub_query_models.items()
        }
    }
    data["stacked_postprocessed"]["postprocess"] = sp_cm.postprocessor.history
    data["stacked_postprocessed"]["network"] = {str(k): v for k, v in sp_cm.net_history.items()}
    data["vanilla"] = {
        "postprocess": v_cm.postprocessor.history
    }
    data["literature"]["network"] = {str(k): v for k, v in l_cm.net_history.items()}
    return jsonify(data)

@app.route("/execute", methods=["POST"])
def execute():
    """Executes a multi-cloud query

    Fields requested are the following :
    query : str
        A query to be executed or its name

    method : str in ('static', 'dynamic', 'static_randomised) (Default : 'static')
        The execution engine

    plan : str (Optional)
        The execution plan to follow for the static method.

    query_name : str (Optionnal)
        The name of the query to be executed

    clean : boolean (Optionnal), default = False
        Deletes the query results at the end.
    """
    global HISTORY

    # Get data from the request
    query_name = request.form["query_name"]
    method = request.form["method"]
    quotation_name = request.form["quotation_name"]

    try :
        # WTF! `bool("False")` returns `True`! ... JS, is that you??
        if request.form["disable_cost_model"] in ("on", "True") :
            disable_cost_model = True
        elif  request.form["disable_cost_model"] in ("off", "False") :
            disable_cost_model = False
        else:
            logging.warning(f"Wrong input for disable_cost_model: {request.form['disable_cost_model']}")
            raise Exception()
    except :
        disable_cost_model = False

    # Removing copied double quotes out of convenience when using the GUI
    if quotation_name[0] == quotation_name[-1] == '"' :
        quotation_name = quotation_name[1:-1]

    clean = True

    logging.info(f"Asked to execute {query_name} with method {method} with quotation {quotation_name}")


    # Verify if a Quotation have been generated
    try:
        quotations = HISTORY[query_name]["tender"]
    except KeyError:
        logging.error(f"KeyError {query_name}; Unknown query, History: {HISTORY}")
        raise NoQuotationError(query_name)

    try:
        quotation = quotations[quotation_name]
    except KeyError:
        logging.error(f"KeyError {quotation_name}; History: {HISTORY}")
        raise NoQuotationError(quotation_name)

    cost_model = COST_MODELS[HISTORY[query_name]["cost_model"]]

    if method == "static_randomised" :
        quotation : Quotation
        eng = StaticRandomisedExecution(quotation, cost_model)
        eng.cost_model.disable_cost_model = disable_cost_model

    elif method == "static":
        eng = StaticEngine(
            cost_model,
            query_name,
            HISTORY[query_name],
            quotation_name
        )
    elif method == "dynamic":
        eng = DynamicEngine(
            cost_model,
            query_name,
            HISTORY[query_name],
            quotation_name
        )

    else:
        raise UnknownModelError(method)

    logging.info(f"Execution engine for {query_name}: {str(type(eng))}")

    bill = eng.execute()
    # Finishing the bill
    bill.estimated_execution_time = quotation.estimated_execution_time

    # Training the corrector - Tuple price, response time

    # Deleting quotation first, then the query itself if all quotations have been used
    HISTORY[query_name]["tender"].pop(quotation_name)
    if len(HISTORY[query_name]["tender"]) <= 0:
        HISTORY.pop(query_name)

    if clean and "randomised" not in method:
        eng.plan[0].admin(f"DROP TABLE IF EXISTS {query_name};", asynchronous=True)

    res = {
        "status_code": 201,
        "details": "Execution OK",
        "qname": query_name,
        "bill": bill.to_dict(),
        "last_node": str(eng.plan),
        "last_provider_name": eng.plan[0].name,
        "last_provider_url": eng.plan[0].url
    }
    return jsonify(res)


@app.route("/backup_cost_model", methods=["GET"])
def backup_cost_model():
    ts = datetime.datetime.now().strftime("%s")
    path = os.path.join(app.static_folder, f"{ts}.pickle")
    with open(path, "wb") as f:
        pickle.dump(COST_MODELS, f)
    return send_from_directory("static", f"{ts}.pickle")


@app.route("/load_cost_model", methods=["POST"])
def load_cost_model():
    global COST_MODELS
    # check if the post request has the file part
    if 'file' not in request.files:
        raise FileNotFoundError("There is no file in this request!")

    file = request.files['file']
    # If the user does not select a file, the browser submits an
    # empty file without a filename.
    if file.filename == '':
        raise FileNotFoundError("This file must have a name...")

    if file.filename.endswith("pickle"):
        filename = secure_filename(file.filename)
        uploaded_file_name = os.path.join(app.static_folder, "uploads", filename)
        file.save(uploaded_file_name)

        with open(uploaded_file_name, "rb") as f:
            COST_MODELS = pickle.load(f)

        return "Success!"


@app.route("/static/<path:path>")
def static_dir(path):
    return send_from_directory("static", path)


class UnknownModelError(Exception):
    def __init__(self, model):
        super().__init__(
            f"Model {model} unknown. Please choose between 'static' and 'dynamic'.")


class CompilationError(Exception):
    def __init__(self, error):
        super().__init__(f"Compilation error in query. Message: {str(error)}")


class NoQuotationError(Exception):
    def __init__(self, qname):
        super().__init__(f"Quotation for query {qname} not yet generated.")


if not args.let_errors:
    @app.errorhandler(UnknownModelError)
    @app.errorhandler(UnknownRelationError)
    @app.errorhandler(CompilationError)
    def handle_invalid_usage(error):
        return {
            "status_code": 400,
            "details": f"{type(error)} - {str(error)}"
        }


    @app.errorhandler(NoQuotationError)
    def handle_no_estimation(error):
        return {
            "status_code": 412,
            "details": str(error)
        }


    @app.errorhandler(UnknownCostModel)
    @app.errorhandler(UnknownExplorationStrategy)
    def handle_no_estimation(error):
        return {
            "status_code": 413,
            "details": str(error)
        }


    @app.errorhandler(OSError)
    @app.errorhandler(MemoryError)
    @app.errorhandler(TypeError)
    @app.errorhandler(AttributeError)
    @app.errorhandler(RuntimeError)
    @app.errorhandler(BrokenBarrierError)
    @app.errorhandler(ReasoningError)
    @app.errorhandler(AdminError)
    def handle_os_error(error):
        return {
            "status_code": 442,
            "details": str(error)
        }


    @app.errorhandler(FileNotFoundError)
    def handle_file_error(error):
        return {
            "status_code": 409,
            "details": str(error)
        }


if __name__ == '__main__':
    app.run(port=4242, debug=True, use_reloader=False, host='0.0.0.0')
    session.close()
