# Nebula middleware API


## Configuration file

## Running the middleware

In the `nebula` directory you cloned :

```shell
bash nebula.sh prov_list.tsv
```

## API documentation

### `GET` > `/`

Returns the homepage

### `GET` > `/schema`

### `POST` > `/estimate`

### `POST` > `/execute`

### `GET` > `/cost_model_history`


