import copy
from typing import Set

from nebula.database.metabase import Provider


def to_plantuml(G, title="") -> str:
    """Debugging purposes"""
    puml = "@startuml\n" \
           "hide class circle\n" \
           "hide class methods\n"

    puml += f"title {title}\n"

    # Getting query's declaration
    for sq in G.nodes:
        try:
            puml += f"class {sq.name} <<{sq.provider.name}>>" + "{\n"
        except AttributeError:
            puml += f"class {sq.name}" + "{\n"
        puml += "\n".join(p.name for p in sq.providers)
        puml += "\n..\n"
        puml += sq.sql
        puml += "\n..\n"
        puml += "\n".join(r.name for r in sq.relations)
        puml += "\n}\n"

    for e in G.edges:
        puml += f"{e[1].name} <-- {e[0].name}\n"

    puml += "\n@enduml"
    return puml


class Bill:
	"""Metrics collector for the queries

	Attributes
	----------
	query_execution_cost: float
		Sum of the monetary cost for querying for all subqueries
	query_total_execution_time: int
		Sum of the response time of all queries
	query_storage_cost: float
		Overall storage cost for all subqueries
	query_transfer_cost: float
		Sum of all transfer costs of the query
	query_total_transfer_time: int
		Sum of all the time spent in data transfers
	response_time: int
		Time spent between the moment the query was received and its execution is over
	total_cost: float
		Sum of all costs of the query
	transferred_data: int
		Sum of all data transfers
	# one_hot: list[int]
	#	One-hot representation of the query
	"""

	def __init__(self, providers:Set[Provider] = set()):
		self.providers = providers
		self.query_execution_cost = 0.0  # Total cost of the query
		self.query_total_execution_time = 0  # Total time spent in executing subqueries

		self.query_storage_cost = 0.0  # Money spent on storage

		self.query_transfer_cost = 0.0  # Money spent on exporting data
		self.query_total_transfer_time = 0  # All time spent in tranfers

		self.total_cost = 0.0  # Sum of all costs
		self.transferred_data = 0  # Amount of transfered data

		self.input_size = 0  # Sum of the multi-cloud query's input relations' size
		self.estimated_execution_time = 0.0

		self.response_time = None  # Response time of the system

		# self.one_hot = None

	def __add__(self, other):
		"""
		response_time addition is undefined, no addition for this field is performed.
		"""
		assert type(other) is Bill
		self.query_execution_cost += other.query_execution_cost
		self.query_total_execution_time += other.query_total_execution_time
		self.query_storage_cost += other.query_storage_cost
		self.query_transfer_cost += other.query_transfer_cost
		self.query_total_transfer_time += other.query_total_transfer_time
		self.total_cost += other.total_cost
		self.transferred_data += other.transferred_data
		self.input_size += other.input_size
		self.providers |= other.providers
		# if self.one_hot is None :
		#	self.one_hot = other.one_hot
		#elif other.one_hot is not None:
		#	self.one_hot = {
		#		k: max(v, other.one_hot[k]) for k, v in self.one_hot.items()
		#	}

		return self

	def __iadd__(self, other):
		assert type(other) is Bill
		return copy.copy(self) + other

	def __str__(self):
		return str(self.to_dict())

	def to_dict(self):
		return {
			"query_execution_cost": self.query_execution_cost,
			"query_total_execution_time": self.query_total_execution_time,
			"query_storage_cost": self.query_storage_cost,
			"query_transfer_cost": self.query_transfer_cost,
			"query_total_transfer_time": self.query_total_transfer_time,
			"response_time": self.response_time,
			"total_cost": self.total_cost,
			"transferred_data": self.transferred_data,
			"input_size": self.input_size,
			"estimated_execution_time": self.estimated_execution_time,
			"nb_providers": len(self.providers)#,
			# "one_hot": self.one_hot
		}
