import copy
import logging
import random
import time

from threading import Barrier, BrokenBarrierError
from typing import Set, Dict, Iterator, Tuple, Callable, Collection, List

import networkx as nx

from nebula.database.metabase import Provider, Relation, Attribute
from nebula.database.utils import DuplicateObjectError
from nebula.engine.query.operators import Clause, Selection, AliasedAttribute
from nebula.engine.query.query import Query
from nebula.engine.quotation import Quotation
from nebula.engine.utils import to_plantuml
from nebula.utils import threaded_function_on_collection

SUBQUERY_NUMBER = 0

class OperationImpossibleSubQuery(Exception):
    def __init__(self, ):
        super(OperationImpossibleSubQuery, self).__init__("Operation impossible between those subqueries.")

class PlanInvalidError(Exception):
    def __init__(self, ):
        super(PlanInvalidError, self).__init__("This plan shall not exist.")

class SubQuery():
    def __init__(self, selections: Set[Clause] = set(), projection: Set = set(), execution_plan=None, multi_cloud_query_name=""):
        super().__init__()
        global SUBQUERY_NUMBER
        self.subquery_number = f"{(SUBQUERY_NUMBER := SUBQUERY_NUMBER + 1)}_{int(time.time())}"
        self.projection = projection
        self.selections = selections
        self.execution_plan = execution_plan
        self.multi_cloud_query_name = multi_cloud_query_name
        self.scans: Set[str] = set()
        for s in self.selections:
            s.subquery = self

        self.provider = None
        self.quotation = None
        self.costed = False
        self.executed = False

    @property
    def attributes(self) -> Set[Attribute]:
        return {a.attribute for s in self.selections for a in s.attributes}

    @property
    def needed_attributes(self) -> Set[Attribute]:
        return self.attributes | {a.attribute if type(a) is AliasedAttribute else a for a in self.projection}

    #@property
    #def relations(self) -> Set[Relation]:
    #    return {a.attribute.relation for s in self.selections for a in s.attributes}

    @property
    def relations(self) -> Set[Relation]:
        """includes ReadTables"""
        return {a.attribute.relation for s in self.selections for a in s.attributes} \
               | {a.attribute.relation if type(a) is AliasedAttribute else a.relation for s in self.scans if
                  type(s) is Relation for a in s.attributes} \
               | {a.attribute.relation if type(a) is AliasedAttribute else a.relation \
                  for a in self.projection if issubclass(type(a), Attribute)}

    @property
    def providers(self) -> Set[Provider]:
        return {a.attribute.relation.provider for s in self.selections for a in s.attributes} \
            | {r.provider for r in self.relations}

    def cost(self):
        """Computes the cost for this query"""
        if self.costed :
            return
        self.quotation = self.provider.estimate(
            self.sql, self.name
        )
        self.costed = True

    def execute(self):
        """TODO : Implement this better"""
        self.bill = self.provider.execute(self.sql, self.name)
        self.executed = True

    @property
    def sql(self):
        sql = "SELECT " + ' ,\n\t '.join(a.name for a in self.projection) + "\n"
        sql += f"FROM " + ' ,\n\t '.join(s.name if type(s) is Relation else s for s in self.scans) + "\n"
        if len(self.selections) > 0:
            sql += f"WHERE " + ' AND \n\t'.join(s.predicate for s in self.selections)

        sql += " ;"
        return sql

    @property
    def name(self) -> str:
        return f"msq_{id(self):x}__{self.multi_cloud_query_name}__{self.subquery_number}"

    def __hash__(self):
        return hash(self.name)  # hash(f"{''.join(str(s) for s in self.selections)} - {self.provider}")

    def __add__(self, other):
        if self.provider != self.provider:
            raise OperationImpossibleSubQuery()

        res = SubQuery(selections=self.selections | other.selections, execution_plan=self.execution_plan, multi_cloud_query_name=self.multi_cloud_query_name)
        res.provider = self.provider

        return res

    def __copy__(self):
        res = SubQuery(self.selections.copy(), self.projection.copy(), execution_plan=self.execution_plan, multi_cloud_query_name=self.multi_cloud_query_name)
        res.scans = self.scans.copy()
        for s in res.selections:
            s.subquery = res

        res.provider = self.provider
        return res

    def __getitem__(self, item):
        """Implemented for compatibility purposes with exhaustive strategy"""
        if item == 0 : # A node in the exhaustive graph was identified by a tuple, itm 0 being the host provider
            return self.provider
        elif item == "tender" :
            return self.quotation
        elif item == "output_size" :
            return self.quotation.estimated_output_size

        raise TypeError(f"{type(self)} does not have '{item}' as a dict element")

    def copy(self):
        return self.__copy__()

###############################################################################

# Functions used within Execution plan's constructor

###############################################################################

def _compute_projections(ep, root: SubQuery) -> None:
    """Applies projection to the sub-queries"""

    def rec(n: SubQuery, proj):
        """
        :proj: the set of attributes needed by the subsequent sub-queries
        :returns: the actual projection
        """
        # Computing projection needed for me
        preds = list(ep.plan.predecessors(n))
        logging.info(f"Rec into {n.name}, {len(preds)}")

        all_attrs = {a for r in n.relations for a in r.attributes}
        if len(preds) == 0:
            n.projection = proj & all_attrs
            return n.projection

        else:
            available_attributes = set()
            next_proj = proj | n.attributes
            for p in preds:
                available_attributes |= rec(p, next_proj)

            n.projection = available_attributes & proj
            n.projection |= all_attrs & proj
            return n.projection

    rec(root, {a.attribute for c in ep.query.projection.clauses for a in c.attributes})


def _compute_scans(ep, root: SubQuery) -> None:
    """Adds scans to the sub-queries"""

    def rec(n: SubQuery):
        preds = list(ep.plan.predecessors(n))

        if len(preds) == 0:
            n.scans = n.relations
            return n.relations
        else:
            n.scans = {p.name for p in preds}

            previous_rels = set()
            for p in preds:
                previous_rels |= rec(p)

            n.scans |= n.relations - previous_rels

            return previous_rels | n.relations


    rec(root)

def _add_idempotent(ep) :
    for n in ep.plan :
        n:SubQuery
        if len(list(ep.plan.predecessors(n))) :
            n.selections |= {
                Clause({AliasedAttribute(k, r) for k in r.primary_key},
                       f"{r.primary_key_str} = {r.primary_key_str}",
                       idempotent=True) for r in n.relations
            }

def _remove_idempotent(ep, root):
    for n in ep.plan:
        n.selections = {c for c in n.selections if not c.idempotent}

class ExecutionPlan():

    def __init__(self, tree: nx.DiGraph, query, by=["Default"]):
        self.plan = tree
        self.query = query
        self.by = by
        self.root: SubQuery = [n for n in self.plan if len(list(self.plan.successors(n))) == 0][0]
        _add_idempotent(self)
        _compute_projections(self, self.root)
        _compute_scans(self, self.root)
        _remove_idempotent(self, self.root)
        self.__response_time = None
        self.__monetary_cost = None
        self.__input_size = 0.0
        self.__costed = False
        self.__signature = None
        self.consider_neighbours = True # Set to false after its neighbours are generated during the exploration

        self.__quotation = None

        for q in self.plan :
            q.plan = self

    @property
    def monetary_cost(self) -> float:
        return self.__monetary_cost

    @property
    def providers(self) -> Set[Provider] :
        return {p for sq in self.plan for p in sq.providers}

    @property
    def response_time(self) -> float:
        return self.__response_time

    @property
    def input_size(self) -> float:
        return self.__input_size

    @property
    def monetary_perf_ratio(self) -> float:
        return (self.response_time + 0.0001) / (self.monetary_cost + 0.0001)

    def __wait_at_barrier(self, s, q, barriers):
        """s : successor, q:current sub query"""
        # Creating the view in the successor's provider
        logging.info(f"Importing view from {q.name} ({q.provider.name}) to {s.name} ({s.provider.name})")
        if q.provider != s.provider:
            logging.info(f"Running IMPORT FOREIGN SCHEMA for {q.name} ({q.provider.name}) to {s.name} ({s.provider.name})")
            try :
                s.provider.admin(
                    f"IMPORT FOREIGN SCHEMA public LIMIT TO ({q.name}) FROM SERVER {q.provider.name} INTO public;"
                )
            except DuplicateObjectError :
                print(to_plantuml(self.plan))
                print("LIGNE 262") # TODO : trouver d'où viennent les duplicats
                exit()
        barriers[s.name].wait()

    def __cost_query(self, q: SubQuery, barriers):
        # Waiting for requirements to be executed
        logging.info(f"COSTING SUBQUERY {q}")  # , {barriers[q].n_waiting}/{barriers[q].parties}")
        barriers[q.name].wait()

        try:
            q.cost()
        except Exception as e:
            print(q.name)
            print(to_plantuml(self.plan, self.by))
            raise e

        # Computing data transfer time and cost to be in this subquery
        for p in self.plan.predecessors(q) :
            tr_size = q.quotation.estimated_output_size

            # Computing transfer cost
            coef = q.provider.storage_cost
            if p.provider != q.provider :
                coef += p.provider.export_cost
            self.plan.edges[p, q]["cost"] = tr_size * coef

            # Computing transfer time
            self.plan.edges[p, q]["time"] = self.query.cost_model.estimate_transfer(self.plan, p, q, tr_size)

        # Create a view on the sub query's provider
        succs = {e for e in self.plan.successors(q)}
        if len(succs) > 0:
#            q.provider.admin(f"DROP VIEW IF EXISTS {q.name};")
            try :
                q.provider.admin(f"CREATE VIEW {q.name} AS {q.sql} ;")
            except DuplicateObjectError as e:
                print(to_plantuml(self.plan))
                print("LIGNE 300")
                raise DuplicateObjectError(q.name)

            # Notifying the successors
            threaded_function_on_collection(self.__wait_at_barrier, succs, q, barriers)

    def cost_tree(self):
        """Computes the costs for this execution plan"""
        logging.info(f"Costing tree {id(self)} ({self.__costed}) for query {self.query.qname}")

        if self.__costed :
            logging.info(f"Tree  {id(self)} for query {self.query.qname} already costed. Skipping.")
            return

        barriers = {
            s.name: Barrier(len(list(self.plan.predecessors(s))) + 1) for s in self.plan.nodes
        }
        subqueries = {sq for sq in self.plan}
        threaded_function_on_collection(self.__cost_query, subqueries, barriers)

        def rec(n:SubQuery) :
            r_time, m_cost, in_size = n.quotation.estimated_execution_time, n.quotation.query_price, n.quotation.input_size

            preds = list(self.plan.predecessors(n))
            in_size = n.quotation.input_size if len(preds) == 0 else 0

            preds_res = [(p, rec(p)) for p in preds]

            m_cost += sum(e[1][1] for e in preds_res)
            in_size += sum(e[1][2] for e in preds_res)
            r_time += max([e[1][0] + self.plan.edges[e[0], n]["time"] for e in preds_res], default=0.0)

            return r_time, m_cost, in_size


        root:SubQuery = [n for n in self.plan if len(list(self.plan.successors(n))) == 0] [0]
        self.__response_time, self.__monetary_cost, self.__input_size = rec(root)
        self.__monetary_cost += root.quotation.query_storage_cost

        # self.__monetary_cost, self.__response_time = self.query.cost_model.postprocess_query({
        #    "input_size": self.__input_size,
        #    "total_cost": self.__monetary_cost,
        #    "estimated_execution_time": self.response_time
        #})

        self.__costed = True

    def __new_tree(self) -> Tuple[nx.DiGraph, Dict[SubQuery, SubQuery]]:
        n_t = nx.DiGraph()
        repl = {sq: copy.copy(sq) for sq in self.plan.nodes}
        n_t.add_edges_from([(repl[src], repl[tgt]) for src, tgt in self.plan.edges])
        # Flushing away projections
        for sq in n_t :
            sq:SubQuery
            sq.projection = set()
        return n_t, repl

    def __affected_providers(self, G, n):
        """Lists all predecessors providers"""
        providers = {n.provider}
        for p in G.predecessors(n):
            providers |= self.__affected_providers(G, p)
        return providers

    @classmethod
    def is_valid(cls, ep):
        ep:ExecutionPlan

        def rec(n) :
            preds = list(ep.plan.predecessors(n))
            if len(n.projection) == 0 :
                raise PlanInvalidError

            if len(preds) == 0 :
                return n.projection
            else :
                avail_attrs = {a for r in n.scans if type(r) is Relation for a in r.attributes}
                rec_res = list()
                for p in preds :
                    rec_res.append(rec(p))

                for e in rec_res : # Avoiding cartesian products between two branches percolating the same relations
                    if any(len(e & e2) > 0 for e2 in rec_res if e != e2) :
                        raise PlanInvalidError
                    avail_attrs |= e
                if not n.needed_attributes <= avail_attrs :
                    raise PlanInvalidError

                return n.projection

        try :
            root: SubQuery = [n for n in ep.plan if len(list(ep.plan.successors(n))) == 0][0]
            rec(root)
        except PlanInvalidError :
            return False
        finally:
            return True

    def rule_1(self):
        """ Rule Rho1.

        The first rule $\varrho_1$ enables the exploration of different placement schemes over the providers.
        Namely, for each vertex $v \in V$, placed on a provider $P_v$, whose involved provider set is noted as
        $\mathcal{P}_v = \bigcup_{c \in C_v} \bigcup_{R \in \mathcal{R}_c} P_R$, a new plan is generated with a
        different $P_v$ for each provider in $\mathcal{P}_v - P_v$. This rule may lead to the discovery of execution
        plans with smaller inter-provider data transfers as compared to the deriving plan while preserving the
        distributed processing of the multi-cloud query.
        """
        logging.info("Rule 1")
        neighbours = set()

        for s in self.sub_queries :
            # Ignoring sub-queries with only 1 provider
            if len(s.providers) == 1 :
                continue

            # Computing eligible providers for the sub-query
            eligible_providers = {r.provider for r in s.scans if type(r) is Relation}
            if len(eligible_providers) == 0 :
                eligible_providers = s.providers

            for p in eligible_providers - {s.provider} :
                # Creating the new tree
                n_t, repl = self.__new_tree()
                repl[s].provider = p
                if repl[s].providers <= self.__affected_providers(n_t, repl[s]) :
                    by = copy.copy(self.by)
                    by.append("R1")
                    n_plan = ExecutionPlan(n_t, self.query, by=by)
                    if ExecutionPlan.is_valid(n_plan) :
                        neighbours.add(n_plan)

        return neighbours

    def rule_2(self):
        """Rule Rho2.

        The second rule $\varrho_2$ is akin to considering a different join order.
        Namely, for each of the pairwise successions of vertices containing inter-provider clauses defined by
        \eqref{eq:h2_def}, an execution plan is generated where $v_1$ and $v_2$ are swapped.
        This rule may lead to the discovery of execution plans handling a smaller total amount of data as compared to
        the derived plan.
        \begin{equation}
            \{\tuple{v_1, v_2}\ |\ v_1, v_2 \in V\ :\ \exists\ \tuple{v_1, v_2} \in E \land \card{\mathcal{P}_{v_1}}
            \geq 2 \leq \card{\mathcal{P}_{v_2}} \}
            \label{eq:h2_def}
        \end{equation}
        """
        logging.info("Rule 2")
        neighbours = set()

        for s in self.sub_queries :
            # Cannot switch with initial sub-queries
            successors = list(self.plan.successors(s))
            if len(s.providers) == 1 or len(successors) == 0:
                continue

            succ:SubQuery = successors[0]

            # Copying the current plan
            n_t, repl = self.__new_tree()

            # Keeping track of dependencies
            s = repl[s]
            succ = repl[succ]
            all_preds = set(n_t.predecessors(s)) | set(n_t.predecessors(succ)) - {s, succ}
            successors = list(n_t.successors(succ))

            # Swapping the nodes
            n_t.add_edge(succ, s)
            n_t.remove_edge(s, succ)

            # Rewiring stuff properly
            for s2 in successors: # Triggers RuntimeError w/o list(...) because the graph is altered
                n_t.remove_edge(succ, s2)
                n_t.add_edge(s, s2)

            n_t.remove_edges_from([(src, tgt) for src in all_preds for tgt in [s, succ]])
            for p in all_preds :
                if len(p.providers & succ.providers) > 0 :
                    n_t.add_edge(p, succ)
                else:
                    n_t.add_edge(p, s)

            # Making sure all data is properly available in the plan
            by = copy.copy(self.by)
            by.append("R2")
            n_plan = ExecutionPlan(n_t, self.query, by=by)
            if succ.providers <= self.__affected_providers(n_t, succ) and ExecutionPlan.is_valid(n_plan) :
                neighbours.add(n_plan)

        return neighbours

    def rule_3(self):
        """Rule Rho3.

        The third rule $\varrho_3$ considers a loss of distributed parallelism by merging of queries hosted on the
        same provider. Namely, for each couple of successive vertices placed on the same provider defined by
        \eqref{eq:h3_def}, a new execution plan is generated where $v_1$ and $v_2$ are merged into a vertex $v^\prime$
        having a clause set $C_{v^\prime} = C_{v_1} \cup C_{v_2}$.
        This rule may lead to the discovery of execution plans with lower idle times than the derived plan.
        \begin{equation}
            \{\tuple{v_1, v_2}\ |\ v_1, v_2 \in V\ :\ \exists\ \tuple{v_1, v_2} \in E \land P_{v_1} = P_{v_2} \}
            \label{eq:h3_def}
        \end{equation}
        """
        logging.info("Rule 3")
        neighbours = set()
        for src, tgt in self.plan.edges:
            src: SubQuery
            tgt: SubQuery

            if src.provider == tgt.provider :
                n_t, repl = self.__new_tree()
                src, tgt = repl[src], repl[tgt]

                # Merging the sub-queries
                n_t.remove_edge(src, tgt)
                tgt.selections |= src.selections

                # Fixing the connexions
                preds = list(n_t.predecessors(src))
                for p in preds :
                    n_t.add_edge(p, tgt)
                    n_t.remove_edge(p, src)

                n_t.remove_node(src)

                by = copy.copy(self.by)
                by.append("R3")
                neighbours.add(ExecutionPlan(n_t, self.query, by=by))

        return neighbours

    @property
    def neighbors(self):
        """Computes all the neighbours of current plan following previous rules"""
        logging.info(f"Computing neighbours for {repr(self)}")
        neighbors = self.rule_3() # Trick to get better plans faster
        neighbors |= self.rule_1()
        neighbors |= self.rule_2()
        logging.info(f"Neighbours computed for {repr(self)}")
        return neighbors

    @property
    def sub_queries(self) -> Iterator[SubQuery]:
        return self.plan.nodes

    def to_plantuml(self) -> str:
        """Debugging purposes"""
        return to_plantuml(self.plan, title=self.by)

    @property
    def signature(self):
        def rec(n) :
            sign = {s.predicate for s in n.selections}
            sign.add(n.provider.name)
            for p in self.plan.predecessors(n) :
                sign.add(rec(p))
            return frozenset(sign)

        if self.__signature is None :
            self.__signature = rec(
                [n for n in self.plan if len(list(self.plan.successors(n))) == 0][0]
            )

        return self.__signature

    def __hash__(self):
        return hash(self.signature)

    @property
    def leaves(self) -> List[SubQuery] :
        return [e for e in self.plan if len(list(self.plan.predecessors(e))) == 0]

    @property
    def quotation(self):
        if self.__quotation is None :

            def rec(n) -> Quotation :
                quot = copy.copy(n.quotation)
                for p in self.plan.predecessors(n) :
                    quot += rec(p)
                return quot

            self.__quotation = rec(self.root)
            self.__quotation.execution_plan = self
        return self.__quotation

    def __getitem__(self, item):
        """Implemented for legacy purpose"""
        if item == 0 :
            return self.root.provider

    def __str__(self):
        return to_plantuml(self.plan, self.by)

###############################################################################

# Objective functions from section 4.3

###############################################################################

def objective_function_monetary_cost(plan: ExecutionPlan):
    return plan.monetary_cost


def objective_function_response_time(plan: ExecutionPlan):
    return plan.response_time


def objective_function_best_value(plan: ExecutionPlan):
    return plan.monetary_perf_ratio

def objective_function_monetary_priority_cost(plan: ExecutionPlan, q_rt, l=0.1):
    if plan.response_time <= q_rt * l:
        return plan.monetary_cost
    return plan.monetary_perf_ratio


def objective_function_response_time_priority(plan: ExecutionPlan, q_money, l=0.1):
    if plan.monetary_cost <= q_money * l:
        return plan.response_time
    return plan.monetary_perf_ratio

###############################################################################

class RandomisedSearchSpace():
    def __init__(self, query, plan, nb_iterations=1, max_size_hypergraph=100, max_depth=15):
        self.query = query
        self.plan = plan
        self.nb_iterations = int(nb_iterations)
        self.hypergraph = nx.Graph()
        self.subqueries = set()
        self.max_size_hypergraph = int(max_size_hypergraph)
        self.max_depth = int(max_depth)

    #
    # Neighbourhood expansion
    #
    def __inner_expanding_neighbourhood(self, neighbour:ExecutionPlan, origin_node:ExecutionPlan, costs, cost_functions):
        try:
            neighbour.cost_tree()
        except BrokenBarrierError:
            logging.warning(f"Execution plan rotten! {neighbour.by}")
            neighbour.consider_neighbours = False
            neighbour.__costed = True
        except:
            logging.warning(f"Execution plan rotten but IDK how to catch this! {neighbour.by}")
            neighbour.consider_neighbours = False
            neighbour.__costed = True
        else:
            self.hypergraph.add_edge(origin_node, neighbour)
            costs[neighbour] = {f: f(neighbour) for f in cost_functions}

    def __expanding_neighbourhood(self, node:ExecutionPlan, costs, cost_functions):
        neighbours = list()
        for e in node.neighbors :
            for other in self.hypergraph :
                if e.signature == other.signature :
                    self.hypergraph.add_edge(node, other)
                    break
            else :
                if len(self.hypergraph) < self.max_size_hypergraph :
                    neighbours.append(e)
                    logging.info(f"Adding neighbour in hypergraph. {len(self.hypergraph)} / {self.max_size_hypergraph}")
                else:
                    logging.info("Limit in the hypergraph was hit")
                    break

        threaded_function_on_collection(self.__inner_expanding_neighbourhood, neighbours, node, costs, cost_functions)
        node.consider_neighbours = False

    def optimise_query(self, cost_functions: Collection[Callable]) -> Dict[Callable, ExecutionPlan] :
        """Implements Algorithm 1 from the article"""

        # Setting up default values
        best_plans = {
            f: self.plan for f in cost_functions
        }
        costs = {
            self.plan : {
                f: f(self.plan) for f in cost_functions
            }
        }
        self.hypergraph.add_node(self.plan)

        def explore_neighbourhood(nodes_iterator) :
            change = False

            if len(self.hypergraph) < self.max_size_hypergraph:
                return change

            # Generate neighbourhood of existing execution plans
            nodes = [e for e in nodes_iterator if e.consider_neighbours]
            threaded_function_on_collection(self.__expanding_neighbourhood, nodes, costs, cost_functions)

            # Finding next minimal execution plans
            for f in cost_functions:
                best_plan = min(costs, key=lambda k: costs[k][f])
                if costs[best_plan][f] < costs[best_plans[f]][f]:
                    best_plans[f] = best_plan
                    change = True

            return change

        cpt_iterations = 0
        cpt_depth = 0
        while cpt_depth < self.max_depth and cpt_iterations < self.nb_iterations :
            change = explore_neighbourhood(best_plans.values())

            if not change :
                logging.info(f"Using jocker iteration {cpt_iterations}/{self.nb_iterations}")
                explore_neighbourhood(self.hypergraph)
                cpt_iterations += 1
            else :
                cpt_depth += 1

        res = dict()
        for k, v in best_plans.items() :
            res[k] = v.quotation
            res[k].objective_function = k
            res[k].nb_providers = len(self.providers_involved)

        return res

    @property
    def providers_involved(self) :
        res = set()
        for sub_query in self.plan.plan :
            sub_query : SubQuery
            res |= sub_query.providers

        return res

class RandomisedStrategyQuery(Query):
    """Inherits query to avoid breaking everything"""

    def __init__(self, root, projection, read_tables, selection, group_by, having, name, schema, cost_model,
                 disable_cost_model=False, nb_iterations=1, max_size_hypergraph=100, max_depth=15):
        super(RandomisedStrategyQuery, self).__init__(root, projection, read_tables, selection, group_by, having, name,
                                                      schema, cost_model)
        # Add idempotent selections in order to avoid adding scan sub-queries
        self.selection.clauses |= {
            Clause({AliasedAttribute(k, r) for k in r.relation.primary_key},
                   f"{r.relation.primary_key_str} = {r.relation.primary_key_str}",
                   idempotent=True) for r in read_tables
        }
        self.search_space = None
        self.dependency_graph: nx.DiGraph = None
        self.subqueries = set()
        self.nb_iterations = nb_iterations
        self.max_size_hypergraph = max_size_hypergraph
        self.max_depth = max_depth
        self.disable_cost_model = disable_cost_model
        self.cost_model.disable_cost_model = disable_cost_model

    def __relations_dependency_graph(self):
        """
        :returns D_Q graph from the article"""

        # Keeping only the selections in the first time
        clauses: Set[Clause] = {c for c in self.get_clauses() if c.op_type is Selection}

        # Building the initial graph
        dependency_graph = nx.DiGraph()
        dependency_graph.add_nodes_from(clauses)

        for c1 in clauses:
            for c2 in clauses:
                if c1 != c2:
                    c1_rels = {a.relation for a in c1.attributes}
                    c2_rels = {a.relation for a in c2.attributes}

                    if len(c1_rels) <= len(c2_rels) \
                            and len(c1_rels & c2_rels) > 0:
                        dependency_graph.add_edge(c1, c2)

        return dependency_graph

    def __providers_dependency_graph(self, dependency_graph: nx.DiGraph):
        """:returns D_Q^prime graph from the article"""

        # Grouping together the clauses
        def find_nodes_rec(n, prov_set, acc):
            acc.add(n)
            neighbours = set(dependency_graph.successors(n)) | set(dependency_graph.predecessors(n))
            for s in neighbours:
                s_pset = {a.relation.provider for a in s.attributes}
                if s not in acc and prov_set == s_pset:
                    find_nodes_rec(s, s_pset, acc)

        dep_graph_tmp = dependency_graph.copy()
        all_nodes = list(dep_graph_tmp.nodes)
        nodes_groups = list()

        while len(all_nodes) > 0:
            n: Clause = all_nodes[0]
            prov_set = {a.relation.provider for a in n.attributes}
            acc = set()
            find_nodes_rec(n, prov_set, acc)
            nodes_groups.append(acc)
            dep_graph_tmp.remove_nodes_from(acc)

            all_nodes = list(dep_graph_tmp.nodes)

        # Building the graph
        merged_graph = nx.DiGraph()

        # Nodes are clauses gouped together
        self.subqueries = set(SubQuery(g, multi_cloud_query_name=self.qname) for g in nodes_groups)
        merged_graph.add_nodes_from(self.subqueries)

        # Edges are links between nodes in those groups
        for n in merged_graph.nodes:
            n: SubQuery  # Just to get the autocompletion

            # Finding all succession clauses of current subquery
            all_succ = {s for c in n.selections for s in dependency_graph.successors(c)} - n.selections

            for n2 in merged_graph.nodes:
                n2: SubQuery
                if n != n2 and any(c in all_succ for c in n2.selections) \
                        and not n.providers > n2.providers:
                    merged_graph.add_edge(n, n2)

        return merged_graph

    def __first_tree(self):
        T = nx.DiGraph()
        roots = [n for n in self.dependency_graph if
                 set(nx.ancestors(self.dependency_graph, n)) - {n} == set(self.dependency_graph) - {n}]

        def rec(n):
            logging.info(f"Processing node {n}")

            try:
                next = random.choice(list(set(roots) - set(T)))
                T.add_edge(next, n)
                rec(next)

            except IndexError:
                # Linear backbone generated, adding remaining predecessors to the tree and returning them, just as usual
                ...

        # Building the linear backbone
        root = random.choice(roots)
        T.add_node(root)
        rec(root)

        # Adding the other sub-queries
        node = [e for e in T if [e for e in T.predecessors(e)] == []][0]
        while True :
            nodes_to_add = {n for n in set(self.dependency_graph) - set(T) if n.providers < node.providers}
            T.add_edges_from([(n, node) for n in nodes_to_add])

            succs = [e for e in T.successors(node)]
            if len(succs) == 0 :
                break
            else:
                node = succs[0]

        # The tree is done

        return T

    def __random_plan(self):
        # Getting a random tree
        t_0 = self.__first_tree()
        ex_plan = ExecutionPlan(t_0, self)

        # Affecting sub-queries to random providers
        for n in ex_plan.plan.nodes:
            n.provider = random.choice(list(n.providers))

        # print(ex_plan.to_plantuml())

        # Initial costing
        ex_plan.cost_tree()

        return ex_plan

    def graph(self, nb_iteration=1, max_size_hypergraph=100):

        self.dependency_graph = self.__providers_dependency_graph(
            self.__relations_dependency_graph()
        )

        random_plan = self.__random_plan()

        # Initializing the search space
        self.search_space = RandomisedSearchSpace(self, random_plan, self.nb_iterations, self.max_size_hypergraph, self.max_depth)

        return self

    def quotation(self) -> Dict[tuple, Quotation]:
        # Add function definition to get best trees w.r.t. money, resp time or "best value"
        try :
            plans = self.search_space.optimise_query([
                objective_function_monetary_cost,
                objective_function_response_time,
                objective_function_best_value
            ])
        except Exception as e :
            logging.error(f"SIZE OF THE GRAPH = {len(self.search_space.hypergraph)}")
            raise e

        # Dropping equivalent or dumb quotations
        change = True
        while change :
            for k, v in plans.items() :
                v: Quotation
                if any(v.estimated_execution_time >= v2.estimated_execution_time and v.total_cost >= v2.total_cost for k2, v2 in plans.items() if k != k2) :
                    del plans[k]
                    break
            else :
                change = False

        self.quotations = plans

        return plans

    def clean_db(self):
        """TODO : Implement this"""
        ...
