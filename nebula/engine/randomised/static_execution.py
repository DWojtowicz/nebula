import logging
import time
from datetime import datetime
from threading import Barrier, Condition, Lock

from psycopg.errors import WrongObjectType

from nebula.cost_models.cost_model import CostModel
from nebula.database.utils import AdminError
from nebula.engine.quotation import Quotation
from nebula.engine.utils import Bill
from nebula.engine.randomised.space_search_exploration import SubQuery
from nebula.utils import threaded_function_on_collection


class StaticRandomisedExecution() :
    def __init__(self, quotation:Quotation, cost_model: CostModel):
        self.plan = quotation.execution_plan
        self.quotation = quotation
        self.cost_model = cost_model
        self.barriers = {
            n : {
                "preds": Barrier(
                    1 + len([e for e in self.plan.plan.predecessors(n)])
                ),
                "succs": Barrier(
                    1 + len([e for e in self.plan.plan.successors(n)])
                ),
            } for n in self.plan.plan
        }

        self.bills = {
            n: Bill(n.providers) for n in self.plan.plan
        }
        self.bill = None

    #####################################################################################
    # Cleaning
    #####################################################################################
    def __clean_prerequisites(self, predecessor, sub_query):
        try :
            sub_query.provider.admin(f"DROP VIEW IF EXISTS {predecessor.name};")
        except WrongObjectType :
            sub_query.provider.admin(f"DROP FOREIGN TABLE IF EXISTS {predecessor.name};")

        self.barriers[predecessor]["succs"].wait()

    def __pre_cleaning(self, sub_query:SubQuery):
        self.barriers[sub_query]["succs"].wait()

        # Cleans stuff
        sub_query.provider.admin(f"DROP VIEW IF EXISTS {sub_query.name};")
        sub_query.provider.admin(f"DROP FOREIGN TABLE IF EXISTS {sub_query.name};")

        threaded_function_on_collection(
            self.__clean_prerequisites,
            [e for e in self.plan.plan.predecessors(sub_query)],
            sub_query
        )

    #####################################################################################
    # Execution
    #####################################################################################
    def __execute_one_query(self, sub_query: SubQuery):
        self.barriers[sub_query]["preds"].wait()

        logging.info(f"Executing subquery {sub_query.name} on {sub_query.provider.name}")
        # Runs the execution
        costs = sub_query.provider.execute(sub_query.sql, sub_query.name, sub_query.name)

        # Notify the predecessors that cleaning can happen
        for sq in self.plan.plan.predecessors(sub_query) :
            self.barriers[sq]["succs"].wait()

        self.bills[sub_query].query_total_execution_time += costs["query"]["time"]
        self.bills[sub_query].query_execution_cost += costs["query"]["price"]
        self.bills[sub_query].total_cost += costs["query"]["price"]

        for p in self.plan.plan.predecessors(sub_query) :
            self.bills[sub_query] += self.bills[p]

        logging.info(f"Subquery {sub_query.name} executed on {sub_query.provider.name}")

        # Transfer the data if needed
        try :
            succ: SubQuery = [e for e in self.plan.plan.successors(sub_query)][0]
        except IndexError :
            ... # Normal here, happens for the root
        else :
            # Storage cost at destination
            storage_cost = costs["storage"]["size"] * succ.provider.storage
            self.bills[sub_query].query_storage_cost += storage_cost
            self.bills[sub_query].total_cost += storage_cost

            # Data transfers logging
            self.bills[sub_query].transferred_data += costs["storage"]["size"]

            if succ.provider != sub_query.provider :
                logging.info(f"Subsequently to {sub_query.name} on {sub_query.provider.name}, transfering data to {succ.provider.name} to prepare {succ.name}")
                start = datetime.now()

                sub_query.provider.dump_table(sub_query.name)
                logging.info(f"Dumped table {sub_query.name} on {sub_query.provider.name}")
                succ.provider.load_table(sub_query.name, sub_query.provider)
                time.sleep(1)
                logging.info(f"Transfered table {sub_query.name} from {sub_query.provider.name} to {succ.provider.name}")
                sub_query.provider.delete_table_file(sub_query.name)
                logging.info(f"Removed file for  {sub_query.name} on {sub_query.provider.name}")

                ex_time = datetime.now() - start
                self.bills[sub_query].query_total_transfer_time += ex_time.total_seconds() * 1000
                self.bills[sub_query].total_cost += costs["query"]["export_cost"]
                self.bills[sub_query].transferred_data += costs["storage"]["size"]

                logging.info(f"Transfered {sub_query.name} from {sub_query.provider.name} to {succ.provider.name} to prepare {succ.name}.")

        # Notifying for the successor
        succs = [e for e in self.plan.plan.successors(sub_query)]
        for s in succs :
            self.barriers[s]["preds"].wait()

        # Waiting for the successor to finish their execution
        logging.info(f"{sub_query.name} is waiting to clean...")
        self.barriers[sub_query]["succs"].wait()
        logging.info(f"{sub_query.name} can clean!")

        # Does the cleaning
        sub_query.provider.admin(f"DROP TABLE IF EXISTS {sub_query.name};", no_update=True, asynchronous=True)
        for sq in self.plan.plan.predecessors(sub_query) :
            sq: SubQuery
            if p.provider != sub_query.provider :
                # sub_query.provider.admin(f"DROP FOREIGN TABLE IF EXISTS {sq.name};", no_update=True, asynchronous=True)
                sub_query.provider.admin(f"DROP TABLE IF EXISTS {sq.name};", no_update=True, asynchronous=True)

    def execute(self):
        # Cleans stuff just in case
        start = datetime.now()
        threaded_function_on_collection(self.__pre_cleaning, [e for e in self.plan.plan])
        threaded_function_on_collection(self.__execute_one_query, [e for e in self.plan.plan])
        root = [e for e in self.plan.plan if len(list(self.plan.plan.successors(e))) == 0][0]
        response_time = datetime.now() - start

        # Finishing the bill
        bill = self.bills[root]
        bill.response_time = response_time.total_seconds() * 1000
        bill.input_size = sum(e.quotation.input_size for e in self.plan.leaves)
        self.bill = bill

        if not self.cost_model.disable_cost_model :
            self.cost_model.learn_postprocess(self.quotation, self.bill)

        return bill