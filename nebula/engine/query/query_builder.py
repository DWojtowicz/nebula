from typing import Set, List

from sqlalchemy.exc import NoResultFound, MultipleResultsFound

from nebula.compiler.tmp_operators import TmpRelation, TmpAttribute
from nebula.database.metabase import Relation, Attribute, Session
from nebula.engine.query.operators import Projection, ReadTable, Selection, GroupBy, Having, AliasedAttribute, Clause
from nebula.engine.query.query import Query
from nebula.engine.randomised.space_search_exploration import RandomisedStrategyQuery


class QueryBuilder:
    """Implementation of the semantic checks of a query"""

    def __init__(self, clauses, alias_mapping, query_name, cost_model, exploration_strategy,
                 nb_iterations=1, max_size_hypergraph=150, max_depth=15, disable_cost_model=False):
        self.clauses = clauses
        self.alias_mapping = alias_mapping
        self.qname = query_name
        self.cost_model = cost_model
        self.exploration_strategy = exploration_strategy
        self.disable_cost_model = disable_cost_model
        self.alias_mapping_read_tables = dict()
        self.projections: Projection = None
        self.read_tables: Set[ReadTable] = None
        self.selections: Selection = None
        self.group_by: GroupBy = None
        self.having: Having = None
        self.session = None
        # Randomised search parameters
        self.nb_iterations_randomised = nb_iterations
        self.max_size_hypergraph = max_size_hypergraph
        self.max_depth = max_depth


    def __load_relations(self, relations: List[TmpRelation]) -> Set[ReadTable]:
        """Checks whether relations in the FROM clause exists or not in the relational schema.
        Populates mapping alias -> ReadTable
        :raises: UnknownRelationError
        """
        # rel = self.session.query(Relation).filter(Relation.name == p.table_name).one()
        # Storage of all relations not found, in order to build the error if necessary
        relations_not_found = list()
        read_tables = set()
        for r in relations:
            try:
                rel = self.session.query(Relation).filter(Relation.name == r.name).one()
                read_table = ReadTable(rel, r.alias)
                read_tables.add(read_table)

                if r.alias is not None:
                    self.alias_mapping_read_tables[r.alias] = read_table
                else:
                    self.alias_mapping_read_tables[r.name] = read_table

            # When the relation does not exist
            except NoResultFound:
                relations_not_found.append(r.name)

        if len(relations_not_found) > 0:
            raise UnknownRelationError(relations_not_found)
        else:
            return read_tables

    def __fetch_attribute(self, attribute: TmpAttribute):
        """
        :param attribute:
        :return:
        :raises: AmbiguousAttributeError, NotDeclaredAliasError, UnknownAttributeError
        """

        # TODO avoid executing that for each attribute
        l_relation_names = [r.relation.name for r in self.read_tables]

        try:
            if attribute.alias is not None:
                # Checks whether the attribute alias is correct or not
                try:
                    relation_name = self.alias_mapping[attribute.alias]
                except KeyError:
                    if attribute.alias in l_relation_names:
                        relation_name = attribute.alias
                    else:
                        raise NotDeclaredAliasError(attribute)

                # Queries the metabase looking for the correct attribute.
                # A join is necessary because Attribute.relation.name cannot be used for filtering.
                attr = self.session.query(Attribute) \
                    .join(Relation) \
                    .filter(Attribute.name == attribute.name) \
                    .filter(Relation.name == relation_name) \
                    .one()

            else:
                # Same as above
                attr = self.session.query(Attribute) \
                    .join(Relation) \
                    .filter(Attribute.name == attribute.name) \
                    .filter(Relation.name.in_(l_relation_names)) \
                    .one()

        except NoResultFound:
            raise UnknownAttributeError(attribute)

        except MultipleResultsFound:
            raise AmbiguousAttributeError(attribute)
        if attribute.alias is not None:
            return AliasedAttribute(attr, self.alias_mapping_read_tables[attribute.alias])
        else:
            return AliasedAttribute(attr, self.alias_mapping_read_tables[attr.relation.name])

    def __load_clauses(self, parsed_clauses: list, clause_type):
        """Loads attributes and makes Clause objects for SELECT, WHERE, and HAVING clauses"""
        clauses = set()
        for c in parsed_clauses:
            attributes = set()
            # When a parsed clause is a single attribute
            if type(c) is TmpAttribute:
                attributes.add(self.__fetch_attribute(c))
                clauses.add(Clause(attributes, str(c)))

            # When a parsed clause has multiple attributes
            elif type(c) is list:
                for c1 in c[0]:
                    attributes.add(self.__fetch_attribute(c1))
                clauses.add(Clause(attributes, c[1]))

            # When a parsed clause is a constant
            else:
                clauses.add(Clause(None, c))

        return clause_type(clauses)

    def __load_operators(self):
        """Loads operators into the class's attributes"""
        self.read_tables = self.__load_relations(self.clauses[2])
        self.projections = self.__load_clauses(self.clauses[1], Projection)

        try:
            opt_clauses = self.clauses[3]
            if opt_clauses[1][0] == "where_statements":
                self.selections = self.__load_clauses(opt_clauses[1][-1], Selection)

                if opt_clauses[2][0] == "group_by_statements":
                    self.group_by = self.__load_clauses(opt_clauses[2][-1], GroupBy)

                    if opt_clauses[3][0] == "having_statements":
                        self.having = self.__load_clauses(opt_clauses[3][-1], Having)

            elif opt_clauses[1][0] == "group_by_statements":
                self.group_by = GroupBy([self.__fetch_attribute(a) for a in opt_clauses[1][-1]])

                if opt_clauses[2][0] == "having_statements":
                    self.having = self.__load_clauses(opt_clauses[2][-1], Having)

        except IndexError:
            # Case happens when some or all optional clauses are missing. That's fine.
            pass

    def build_query(self, schema):
        """Transforms components of an AST into a query"""
        self.session = Session
        # Operators creation
        self.__load_operators()

        # self.session.close()
        if self.exploration_strategy == "exhaustive":
            return Query(None, self.projections, self.read_tables, self.selections, self.group_by, self.having,
                         self.qname,
                         schema, self.cost_model)
        else:
            return RandomisedStrategyQuery(None, self.projections, self.read_tables, self.selections, self.group_by,
                                           self.having, self.qname,
                                           schema, self.cost_model, self.disable_cost_model,
                                           self.nb_iterations_randomised, self.max_size_hypergraph, self.max_depth)


class UnknownRelationError(Exception):
    def __init__(self, relations):
        super().__init__(f"Relation{'s' if len(relations) > 1 else ''} <{'>, <'.join(relations)}> unknown.")


class NotDeclaredAliasError(Exception):
    def __init__(self, attribute):
        super().__init__(f"Alias <{attribute.alias}> for attribute <{attribute.name}> has not been declared.")


class UnknownAttributeError(Exception):
    def __init__(self, attribute: TmpAttribute):
        super().__init__(f"Attribute <{str(attribute)}> unknown.")


class AmbiguousAttributeError(Exception):
    def __init__(self, attribute: TmpAttribute):
        super().__init__(f"Attribute <{str(attribute)}> ambiguously defined.")


class AmbiguousRelationError(Exception):
    def __init__(self, message):
        super().__init__(message)
