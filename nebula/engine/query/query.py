import logging
import subprocess
from abc import ABC
from concurrent.futures import ThreadPoolExecutor
from multiprocessing import Event

import networkx as nx
import psutil

from nebula.engine.query.operators import *

from nebula.engine.quotation import quotation, Quotation

"""Query number identifier"""

QUERY_NUMBER = 0
MEMORY_CONTROL = True

def disable_memory_control() :
    global MEMORY_CONTROL
    MEMORY_CONTROL = False


class AbstractQuery(ABC):
    def __init__(self):
        global QUERY_NUMBER
        self.id = (QUERY_NUMBER := QUERY_NUMBER + 1)


class Query(AbstractQuery):
    """Execution plan of a query"""

    def __init__(self, root: Operator, projection, read_tables, selection: Selection, group_by, having, name, schema,
                 cost_model):
        super().__init__()
        self.root = root
        self.projection = projection
        self.read_tables = read_tables
        self.selection = selection
        self.group_by = group_by
        self.having = having

        self.schema = schema

        self.qname = name

        self.cost_model = cost_model

        # Singleton-like attributes
        self._clauses = None
        self._levels = None
        self._graph = None

    def providers_involved(self):
        """Returns the set of __providers needed to execute the query."""
        return set(r.provider for r in self.read_tables)

    def __compute_clauses(self):
        try:  # There could be no selections in a query
            selections = self.selection.clauses.copy()
            for c in selections:
                c.op_type = Selection
        except AttributeError:
            selections = set()

        projections = self.projection.clauses.copy()
        for c in projections:
            c.op_type = Projection

        # Extracting implicit projection from selections
        clauses = selections | projections
        return clauses

    def get_clauses(self):
        # Singleton for attribute clauses
        if self._clauses is None:
            self._clauses = self.__compute_clauses()

        return self._clauses

    def __generate_levels(self, providers):
        global MEMORY_CONTROL
        levels = [[[p] for p in providers]]
        for _ in range(len(providers) - 1):
            if MEMORY_CONTROL and psutil.virtual_memory().percent > 85 :
                raise MemoryError()
            levels.append(
                [prev_lvl + single_e for prev_lvl in levels[-1] for single_e in levels[0] if
                 single_e[0] not in prev_lvl]
            )
        return levels

    def __useless_nodes(self, G:nx.DiGraph) -> List[tuple]:
        return [n for n in G.nodes
                if (len(G.nodes[n]["clauses"]) == 0)  # Nodes without clauses
                or (  # Nodes with solely projection requiring a cartesian product between intermediate results
                        not any([c.op_type is Selection for c in G.nodes[n]["clauses"]])
                        # and len(list(G.successors(n))) > 0
                        and len(list(G.predecessors(n))) > 0
                ) or (  # Nodes without proper predecessors
                        len(list(G.predecessors(n))) > 0
                        and set(p for n in G.predecessors(n) for p in n) != set(p for p in n)
                )]

    def __generate_sql(self, G:nx.DiGraph, n:tuple) -> str:
        clauses = G.nodes[n]["clauses"]

        # Projection
        sql = ', '.join(set(a.strip() for c in clauses for a in c.predicate.strip().split(',') if
                            (c.op_type is Projection or c.op_type is ProjectionImplicit) and a.strip() != ''))
        sql = f"SELECT {sql}\n"

        # FROM clause depends on wether the query is multi-cloud or not
        predecessors = list(G.predecessors(n))
        if len(predecessors) == 0:
            sql = f"{sql}FROM {', '.join(set(r.name for c in clauses for r in c.relations_involved()))} "
        else:
            sql = f"{sql}FROM {', '.join([G.nodes[n]['qname'] for n in predecessors])} "

        # WHERE clauses if any
        clauses = [c.predicate for c in clauses if c.op_type is Selection]
        if len(clauses) > 0:
            sql = f"{sql}\nWHERE {' AND '.join(clauses)}"

        return f"{sql} ;"

    ##### IMPLICIT PROJECTIONS CREATION

    def __add_implicit_clause(self, G, n, to_add):
        # Building clause and adding it to the node
        cl = Clause(
            {AliasedAttribute(a, a.relation) for a in to_add},
            ", ".join(str(a.name) for a in to_add)
        )
        cl.op_type = ProjectionImplicit
        G.nodes[n]["clauses"].add(cl)

    def __add_implicit_projections_terminal(self, G, n, future_attributes, already_projected):
        # attributes_to_add contains all the future attributes needed in the query
        to_add = future_attributes - already_projected  # Removing those already projected
        to_add &= {  # Keeping only the ones available with relations on this node
            a for r in n[0].relations for a in r.attributes
        }
        self.__add_implicit_clause(G, n, to_add)

        G.nodes[n]['sql'] = self.__generate_sql(G, n)
        # G.nodes[n]['one_hot'] = one_hot_node(G.nodes[n]["clauses"], self.schema)

        return to_add | already_projected

    def __add_implicit_projections_rec(self, G, n, future_attributes=None):
        """Returns the attributes available to be added"""
        if future_attributes is None:
            future_attributes = set()

        already_projected = {
            a.attribute
            for c in G.nodes[n]["clauses"]
            if c.op_type in (Projection, ProjectionImplicit)
            for a in c.attributes
        }

        predecessors = list(G.predecessors(n))

        if len(predecessors) == 0:
            return self.__add_implicit_projections_terminal(G, n, future_attributes, already_projected)

        else:
            # Passing down the needed arguments
            needed_attributes = {a.attribute for c in G.nodes[n]["clauses"] for a in c.attributes}
            needed_attributes |= future_attributes

            from_predecessors = set()  # Attributes made available by the predecessors
            for p in predecessors:
                from_predecessors |= self.__add_implicit_projections_rec(G, p, needed_attributes)

            to_add = (future_attributes & from_predecessors) - already_projected

            self.__add_implicit_clause(G, n, to_add)

            G.nodes[n]['sql'] = self.__generate_sql(G, n)
            # G.nodes[n]['one_hot'] = one_hot_node(G.nodes[n]["clauses"], self.schema)

            return to_add | already_projected

    def __add_implicit_projections(self, G) -> None:
        """
        Adds the projection needed in the intermediate sub-queries into the graph
        :param G:
        """
        all_sinks = [n for n in G.nodes if len(list(G.successors(n))) == 0]
        for n in G.nodes:
            if len(list(G.successors(n))) == 0:
                self.__add_implicit_projections_rec(G, n)

    def __make_graph_structure(self, levels, clauses) -> nx.DiGraph:
        global MEMORY_CONTROL
        """Generates a query execution graph"""
        G = nx.DiGraph()

        # Adding nodes
        for lvl in levels:
            for t in lvl:
                if MEMORY_CONTROL and psutil.virtual_memory().percent >= 98:
                    raise MemoryError()
                G.add_node(  # TODO switch to objects
                    tuple(t),
                    provider=t[0],
                    clauses=set(),
                    qname=f"msq_{self.qname}_{self.id}_on_{str(t[0].id)}___provs_{'__'.join(str(p.id) for p in t)}",
                    short_name=f"msq {', '.join(str(e.id) for e in t)}",
                    sql=None,
                    tender=dict(),
                    sla=None,
                    output_size=0,
                    input_size=0,
                    executed=False,
                    ready=False,
                    barrier=None,  # Used by dynamic engine
                    agent=None,
                    # one_hot=None,
                    estimation_counter=0,  # Number of predecessors to wait for
                    estimation_event=Event()  # Used in the parallel estimation process
                )

        # Adding edges
        for i in range(len(levels) - 1, 0, -1):
            lvl = levels[i]
            for n in lvl:
                if MEMORY_CONTROL and psutil.virtual_memory().percent >= 98:
                    raise MemoryError()
                G.add_edge(tuple(n[:-1]), tuple(n), size=None, cost=None, time=None, storage_cost=0,
                           export_cost=0,
                           moving_data=None)  # Used by dynamic model to keep track of the minimal data-moving path)
                G.add_edge(tuple(n[-1:]), tuple(n), size=None, cost=None, time=None, storage_cost=0,
                           export_cost=0, moving_data=None)

        # Adding clauses
        for n in G.nodes:
            for c in clauses:
                prov_clause = frozenset(c.providers_involved())
                prov_n = frozenset(n)
                # Acceptable clauses are projection or selections involving new parts of the query
                if prov_n == prov_clause \
                        or (c.op_type is Projection and prov_n >= prov_clause) \
                        or (n[-1] in prov_clause and len(prov_clause) > 1 and prov_n >= prov_clause):
                    G.nodes[n]["clauses"].add(c)

        # Adding implicit projection if missing
        self.__add_implicit_projections(G)

        """
        # Filtering useless clauses
        for n in G.nodes:
            if len(list(G.successors(n))) == 0:
                clauses_to_be_popped = [c for c in G.nodes[n]["clauses"] if type(c) is ProjectionImplicit]
                for c in clauses_to_be_popped:
                    G.nodes[n]["clauses"].pop(c)
        
        # Filtering useless nodes
        # Those nodes have either no clauses, are queries performing cartesian product and a projection
        # or are nodes lacking of proper predecessors
        to_remove = self.__useless_nodes(G)
        while len(to_remove) > 0:
            G.remove_nodes_from(to_remove)
            to_remove = self.__useless_nodes(G)
        """

        # Preparing nodes to be processed
        for n in G.nodes:
            preds = [p for p in G.predecessors(n)]
            G.nodes[n]["estimation_counter"] = len(preds)
            if len(preds) <= 0:
                G.nodes[n]['estimation_event'].set()

        return G

    def __nodes_per_depth(self, G) -> List[List]:
        """Returns a list of list"""
        # Collecting depth levels
        n_per_depth = dict()
        for n in G.nodes:
            try:
                n_per_depth[len(n)].append(n)
            except KeyError:
                n_per_depth[len(n)] = [n]

        # Turning into a list
        out = [None for _ in n_per_depth]
        for k in n_per_depth:
            out[k - 1] = n_per_depth[k]

        return out

    def __create_view(self, G, n):
        """Orders the provider of query n to create a view representing it
        Raises ViewCreationError if the provider wasn't able to create the view.

        It is assumed that all dependencies for the view to be created exist.
        """
        # Loading variables
        sql = G.nodes[n]["sql"]
        vname = G.nodes[n]["qname"]
        prov = n[0]

        print(f"CREATE VIEW ON {prov.name}")
        print(f"\t{vname}")

        # Generate instructions
        sql = f"CREATE VIEW {vname} AS {sql}"
        sql = sql.replace("\t", " ").replace("\\t", " ")
        sql = sql.replace("\n", " ").replace("\\n", " ")

        # Make it happen!
        res = prov.admin(sql)

        # In case of failure
        if res["status_code"] != 201:
            raise ViewCreationError(res["details"])

    def __load_foreign_view(self, G, n):
        """Imports the view of node n into its successors.
        Raises an InportForeignSchemaError in case of failure.

        It is assumed that all dependencies for the foreign table to be created exist.
        """
        successors = G.successors(n)
        prov = n[0]
        vname = G.nodes[n]["qname"]

        for s in successors:
            # No need to re-create the view on the host provider
            if s[0] == prov:
                continue

            # Generating the query
            query = f"DROP FOREIGN TABLE IF EXISTS {vname};"
            res = s[0].admin(query)

            query = f"IMPORT FOREIGN SCHEMA public LIMIT TO ({vname}) "
            query = f"{query} FROM SERVER {prov.name} INTO public;"

            print("For node ", n[0], " running on ", s[0], " query ", query)

            res = s[0].admin(query)

            # In case of failure
            if res["status_code"] != 201:
                raise ImportForeignSchemaError(prov, s[0], vname, res["details"])

    def update_input_size(self, G, n):
        G.nodes[n]["input_size"] = sum(G.nodes[p]["output_size"] for p in G.predecessors(n))

    def __estimate_view(self, G, n):
        """Makes an estimation for the node's view"""
        prov = n[0]
        vname = G.nodes[n]["qname"]

        G.nodes[n]["tender"] = prov.estimate(G.nodes[n]["sql"], query_name=vname)
        G.nodes[n]["tender"].nb_providers = len(n)


    def __compute_transfers_costs(self, G, n):
        """Populates edges of the graph with transfer costs"""

        for s in G.successors(n):

            # Case when there is no data transfer
            if s[0] == n[0]:
                storage_cost = G.nodes[n]["output_size"] * n[0].storage
                G.edges[(n, s)]["cost"] = storage_cost
                G.edges[(n, s)]["time"] = self.cost_model.estimate_transfer(G, n, s, G.nodes[n]["output_size"])
                G.edges[(n, s)]["size"] = G.nodes[n]["output_size"]
                G.edges[(n, s)]["total_time"] = G.nodes[n]["time"]

            else:
                transf_size = G.nodes[n]["output_size"]
                transf_cost = G.nodes[n]["tender"].export_cost
                storage_cost = transf_size * s[0].storage

                transf_time = self.cost_model.estimate_transfer(G, n, s, transf_size) * 1.0e6  # Seconds to microseconds
                G.edges[(n, s)]["cost"] = transf_cost + storage_cost
                G.edges[(n, s)]["export_cost"] = transf_cost
                G.edges[(n, s)]["storage_cost"] = storage_cost
                G.edges[(n, s)]["size"] = transf_size
                G.edges[(n, s)]["time"] = transf_time
                G.edges[(n, s)]["total_time"] = transf_time + G.nodes[n]["time"]

    def __finalise_node(self, G, n, card, time):
        """Populate needed keys in the node"""
        G.nodes[n]["output_size"] = card * G.nodes[n]["tender"].width
        G.nodes[n]["input_size"] = G.nodes[n]["tender"].input_size

        # Fixing query price value
        G.nodes[n]["tender"].query_price = n[0].query * G.nodes[n]["tender"].input_size

        # Final metadata
        G.nodes[n]["cost"] = G.nodes[n]["tender"].total_cost
        G.nodes[n]["time"] = time

    def __estimate_one_node(self, G, node):

        G.nodes[node]["estimation_event"].wait()
        self.__create_view(G, node)
        self.__estimate_view(G, node)
        card, time = self.cost_model.estimate_node(G, node)
        self.__finalise_node(G, node, card, time)
        self.__load_foreign_view(G, node)
        self.__compute_transfers_costs(G, node)

        # Raising barriers for the successors
        for s in G.successors(node):
            G.nodes[s]["estimation_counter"] -= 1
            if G.nodes[s]["estimation_counter"] == 0:
                G.nodes[s]["estimation_event"].set()

    def __generate_estimations(self, G):
        """Creates the views for each subquery and estimate those"""
        global MEMORY_CONTROL
        logging.info("GENERATE_ESTIMATIONS")
        if MEMORY_CONTROL and psutil.virtual_memory().percent >= 98:
            raise MemoryError()
        with ThreadPoolExecutor(max_workers=G.number_of_nodes()) as executor:  # os.cpu_count()) as executor:
            # Parallel estimation. Event mechanism is set up to coordinate the threads.
            for n in G.nodes:
                future = executor.submit(self.__estimate_one_node, G, n)
                err = future.exception()
                if err:
                    raise err

    def __generate_graph(self):
        clauses = self.get_clauses()
        providers = list(self.providers_involved())

        if MEMORY_CONTROL and len(providers) >= 6 :
            raise RuntimeError("Too many providers to deal with")

        self._levels = self.__generate_levels(providers)
        G = self.__make_graph_structure(self._levels, clauses)

        # Do not compute estimations if not enough threads are available
        max_threads = subprocess.run(["cat", "/proc/sys/kernel/threads-max"], capture_output=True)
        max_threads = int(max_threads.stdout)
        if MEMORY_CONTROL and len(G) > max_threads*0.75 :
            raise RuntimeError(f"Not enough threads available {len(G)}/{max_threads*0.75}")

        self.__generate_estimations(G)
        # self.__generate_image(G)
        return G

    def graph(self, nb_iteration=1) -> nx.DiGraph:
        if self._graph is None:
            self._graph = self.__generate_graph()

        return self._graph

    def quotation(self) -> Dict[tuple, Quotation]:
        """Computes the SLAs for this query"""
        return quotation(self.graph(), self.cost_model)

    def __drop_view(self, provider, r_type, view_name):
        res = provider.admin(
            f"DROP {r_type} IF EXISTS {view_name};"
        )

        if res["status_code"] != 201:
            raise ViewDeletionError(provider, view_name, res["details"])

    def clean_db(self):
        """Drops all views created in the process of computing SLAs"""
        G = self._graph
        nodes_p_depth = self.__nodes_per_depth(self._graph)

        # Iterating over the graph from the last level to the first
        for depth in reversed(list(nodes_p_depth)):
            for n in depth:
                vname = G.nodes[n]['qname']

                # Dropping the foreign views
                for s in G.successors(n):
                    if s[0] != n[0]:
                        self.__drop_view(s[0], "FOREIGN TABLE", vname)

                # Dropping the original view
                self.__drop_view(n[0], "VIEW", vname)

    def __str__(self):
        query = f"SELECT {str(self.projection)}\nFROM {', '.join([str(t) for t in self.read_tables])}"
        if self.selection is not None:
            query = f"{query}\nWHERE {str(self.selection)}"
        if self.group_by is not None:
            query = f"{query}\nGROUP BY {str(self.group_by)}"
        if self.having is not None:
            query = f"{query}\nHAVING {str(self.having)}"

        return f"{query};"

    # def __repr__(self):
    #    return f"QUERY {self.id}"#============\n{str(self)}\nEND QUERY {self.id} ========"


class ViewCreationError(Exception):
    def __init__(self, message):
        super().__init__(message)


class ImportForeignSchemaError(Exception):
    def __init__(self, source: Provider, destination: Provider, view_name: str, message: str):
        super().__init__(
            f"Error at {destination.name} in creating {view_name} from {source.name}. "
            f"Details: {message}"
        )


class ViewDeletionError(Exception):
    def __init__(self, provider: Provider, view_name: str, message: str):
        super().__init__(
            f"Error at {provider.name} in deleting view {view_name}. "
            f"Details: {message}"
        )


def one_hot_node(clauses: Set[Clause], schema: Dict[str, Dict]) -> Dict[str, int]:
    """Encode a sub-query as a one-hot vector of the multi-cloud schema's involved attributes"""
    schema_attrs = {f"{p} {r} {a[0]}": 0 for p, rels in schema.items() for r, atts in rels.items() for a in atts}

    for c in clauses:
        for a in c.attributes:
            schema_attrs[f"{a.attribute.relation.provider.name} {a.attribute.relation.name} {a.attribute.name}"] = 1

    return schema_attrs
