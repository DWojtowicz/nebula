from typing import Set, List, Collection, Dict

from nebula.database.metabase import Relation, Attribute, Provider


class AliasedAttribute() :

    def __init__(self, attribute:Attribute, read_table):
        self.attribute = attribute
        self.relation = read_table

    def __str__(self):
        return str(self.attribute)

    @property
    def name(self):
        return f"{self.relation.alias if self.relation.alias is not None else self.relation.name}__{self.attribute.name}"

class Clause():
    def __init__(self, attributes: Set[AliasedAttribute], predicate: str, idempotent=False):
        super().__init__()
        self.attributes = attributes
        self.predicate = predicate
        self.idempotent = idempotent
        self.op_type = None
        self.__providers = None
        self.subquery = None  # Only used in the randomised engine

    def relations_involved(self):
        """

        :return: The set of relations containing the attributes
        """
        try:
            return set(a.relation for a in self.attributes)
        except TypeError:
            return {self.attributes.relation}

    @property
    def relations(self):
        return self.relations_involved()

    def providers_involved(self):
        """

        :return: The set of relations containing the attributes
        """
        if self.__providers is None:
            self.__providers = set(r.provider for r in self.relations_involved())

        return self.__providers

    def relations_involved_per_provider(self):
        return {p : set(r for r in self.relations_involved() if r.provider == p) for p in self.providers_involved()}

    def __str__(self):
        return self.predicate

    def __hash__(self):
        return hash(str(self))

class Operator() :
    def __init__(self):
        self.prev_op = None
        self.next_op = None
        self.clauses = None

    def link_prev(self, prev):
        self.prev_op = prev
        prev.nex_op = self

    def link_next(self, next):
        self.next_op = next
        next.prev_op = self

    def root(self):
        if self.next_op is None :
            return self
        else :
            return self.next_op.root()

    def providers_involved(self):
        try :
            return set(p for c in self.clauses for p in c.providers_involved)

        except TypeError : # When self.clauses is None
            return set()

    def relations_involved(self):
        """

        :return: The set of relations containing the attributes
        """
        return set(a.relation for c in self.clauses for a in c.attributes)




class BinOperator(Operator) :
    def __init__(self, left:Operator, right:Operator):
        super().__init__()
        self.prev_op = (left, right)


class ReadTable(Operator) :
    """FROM clause"""
    def __init__(self, relation:Relation, alias:str=None):
        super().__init__()
        self.relation = relation
        self.alias = alias
        self.name = self.relation.name

        self.provider = relation.provider
        self.attributes = self.relation.attributes

    def __eq__(self, other):
        return type(other) is ReadTable \
               and self.relation == other.relation \
               and self.alias == other.alias

    def __repr__(self):
        return f"<ReadTable {str(self)}>"

    def __str__(self):
        if self.alias is None :
            return self.relation.name
        else :
            return f"{self.relation.name} {self.alias}"

    def __hash__(self):
        return hash(repr(self))


class Projection(Operator) :
    """SELECT clause"""
    def __init__(self, clauses:Collection[Clause]):
        super().__init__()
        self.clauses = clauses

    def __str__(self):
        return ", ".join([str(c) for c in self.clauses])

    def __repr__(self):
        return f"PROJECTION <{str(self)}>"

    def __hash__(self):
        return hash(str(self))

class ProjectionStar(Projection) :
    """SELECT * clause"""
    def __init__(self):
        """Takes the set of all relations from the query as a parameter"""
        super().__init__({Clause(None, "*")})

    def __str__(self):
        return "*"

class ProjectionImplicit(Projection) :
    """SELECT * clause"""
    def __init__(self, attribute):
        """Takes the set of all relations from the query as a parameter"""
        super().__init__({Clause(attribute, str(attribute.attribute.name))})

    def __repr__(self):
        return f"PROJECTION IMPLICIT <{str(self)}>"

    def __hash__(self):
        return hash(str(self))

class Selection(Operator) :
    """WHERE clause"""
    def __init__(self, clauses:Collection[Clause]):
        super().__init__()
        self.clauses = clauses

    def __str__(self):
        return " AND ".join([str(c) for c in self.clauses])

    def __repr__(self):
        return f"SELECTION <{str(self)}>"

    def clauses_per_provider(self) -> Dict[Provider, Operator]:
        # my_type = type(self) # Inheritance may lead to current object being a Selection or a Having

        # Mapping clauses for each provider
        d_provider_clauses = dict()
        for c in self.clauses :
            try :
                d_provider_clauses[frozenset(c.providers_involved())].append(c)
            except KeyError :
                d_provider_clauses[frozenset(c.providers_involved())] = [c]

        # Selection generation
        for p in d_provider_clauses :
            d_provider_clauses[p] = type(self)(d_provider_clauses[p])

        return d_provider_clauses

    def hidden_projections(self):
        return {ProjectionImplicit({a}) for c in self.clauses for a in c.attributes}


class Join(BinOperator) :
    def __init__(self, left:Operator, right:Operator, predicate:Selection):
        super().__init__(left, right)
        self.predicate = predicate

class NaturalJoin(Join) :
    def __init__(self, left:Operator, right:Operator):
        super().__init__(left, right, None)


class GroupBy(Operator) :
    """GROUP BY clause"""
    def __init__(self, attributes:List[Attribute]):
        self.attributes = attributes

    def __str__(self):
        return ", ".join([str(c) for c in self.attributes])

    def __repr__(self):
        return f"GROUP BY <{str(self)}>"


class Having(Selection) :
    """HAVING clause"""
    # Besoin de vérifier si l'opérateur précédents est un group by un having
    def __repr__(self):
        return f"HAVING <{str(self)}>"


class Ensembly(BinOperator) :
    def __init__(self, left:Operator, right:Operator, operation:str):
        super().__init__(left, right)
        assert operation in ("UNION", "INTERSECT", "MINUS")
        self.operation = operation

    def __str__(self):
        return f"ENSEMBLY OPERATOR '{self.operation}'\t{'; '.join(str(e) for e in self.prev_op)}"

class ExecutionPlan() :
    def __init__(self):
        ...

    def optimise(self):
        """Reorders the execution plan by moving projection and selections to the lowest position possible"""
        ...

if __name__ == '__main__':
    ...