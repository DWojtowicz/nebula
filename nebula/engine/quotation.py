import copy
from typing import List, Dict

import networkx as nx

from nebula import CONST_NETWORK_BANDWIDTH

class Quotation(object):
    """Container for Quotation"""

    def __init__(self, query, provider, estimation_dict=None, query_name=None):
        """

        :param kwargs: A dictionary containing  an estimation from a provider's /estimate function
        """
        self.query = query
        self.provider = provider
        self.query_name = query_name

        self.provider_dict = estimation_dict.copy()

        self.query_price = estimation_dict["query"]["price"]
        self.input_size = estimation_dict["query"]["input_size"]
        self.width = estimation_dict["query"]["width"]
        self.estimated_output_size = estimation_dict["query"]["est_output_size"]
        self.estimated_cardinality = estimation_dict["query"]["est_card"]
        self.estimated_execution_time = estimation_dict["query"]["est_time"]
        self.query_storage_cost = estimation_dict["query"]["storage_cost"]
        self.export_cost = estimation_dict["query"]["export_cost"]
        self.export_time = self.estimated_output_size / CONST_NETWORK_BANDWIDTH  # Seconds to microseconds

        self.total_time = self.estimated_execution_time
        self.total_cost = self.query_price + self.query_storage_cost

        self.response_time = None
        self.graph = None
        # self.one_hot = None

        self.plan_node = None

        self.nb_providers = 0

        self.execution_plan = None # Used by the randomised method
        self.objective_function = None  # Used by the randomised method

    def __add_costs(self, other):
        self.query_price += other.query_price
        self.query_storage_cost += other.query_storage_cost
        self.total_cost += other.total_cost

        if self.provider != other.provider:
            self.total_cost += other.export_cost

    def __iadd__(self, other):
        """

        :param other: considered as the Quotation if a predecessing query
        :return:
        """
        try:
            assert type(other) is Quotation

        except AssertionError:
            return self.__iconcat__(other)

        self.__add_costs(other)

        self.total_time += other.total_time
        if self.provider != other.provider:
            self.total_time += other.export_time  # Network speed

        # self.one_hot = {
        #    k: max(v, other.one_hot[k]) for k, v in self.one_hot.items()
        #}

        return self

    def __iconcat__(self, others):
        assert type(others) is List[Quotation]

        # Adding costs of other queries
        max_time = 0
        for e in others:
            self.__add_costs(e)

            if self.provider == e.provider:
                max_time = max(max_time, e.total_time)
            else:
                max_time = max(max_time, e.total_time + e.export_time)

        self.total_time += max_time

        return self

    def to_dict(self):
        return {
            "query_name": self.query_name,
            "query_price": self.query_price,
            "input_size": self.input_size,
            "width": self.width,
            "estimated_output_size": self.estimated_output_size,
            "estimated_cardinality": self.estimated_cardinality,
            "estimated_execution_time": self.estimated_execution_time,
            "query_storage_cost": self.query_storage_cost,
            "export_cost": self.export_cost,
            "export_time": self.export_time,
            "total_time": self.total_time,
            "total_cost": self.total_cost,
            "response_time": self.response_time,
            "nb_providers": self.nb_providers  # ,
            # "one_hot": self.one_hot
        }


def __min_time_sla(n, G):
    """Computes execution time minimal Quotation

    n: tuple
        A node key in the graph G

    G: nx.DiGraph
        A directed grah modelling a query
    """
    """
    This one is tricky, because we must find the maximal time-minimal path in the graph
    as a boundary to the maximal time other paths should have.
    """
    return None


def __node_sla(n, G, cost_model):
    """Computes financially minimal Quotation

    n: tuple
        A node key in the graph G

    G: nx.DiGraph
        A directed grah modelling a query
    """

    # Getting base Quotation and resetting it
    sla = copy.deepcopy(G.nodes[n]["tender"])
    # sla.one_hot = G.nodes[n]["one_hot"]
    sla.query_price = 0
    # sla.query_storage_cost = 0 #estimation_dict["query"]["storage_cost"]
    sla.export_cost = 0  # estimation_dict["query"]["export_cost"]
    sla.total_cost = sla.query_storage_cost

    # Computing tree leading to n
    G_rev = G.reverse(copy=False)
    T = nx.traversal.dfs_tree(G_rev, n)

    # Computing price values
    for n in T.nodes:
        sla.query_price += G_rev.nodes[n]["tender"].query_price
        sla.total_cost += G_rev.nodes[n]["tender"].total_cost

    for e in T.edges:
        sla.export_cost += G_rev.edges[e]["export_cost"]
        sla.query_storage_cost += G_rev.edges[e]["storage_cost"]
        sla.total_cost += G_rev.edges[e]["export_cost"] + G_rev.edges[e]["storage_cost"]
        T.edges[e]["total_time"] = G_rev.edges[e]["time"] + G_rev.nodes[e[1]]["tender"].estimated_execution_time
        # for i in range(len(sla.one_hot)) :
        #     sla.one_hot[i] = max(sla.one_hot[i], T.nodes[e[0]]["one_hot"][i])

    # Computing response time
    sla.estimated_execution_time += nx.algorithms.dag.dag_longest_path_length(T, weight="total_time")

    # Postprocessing the quotation
    sla.total_cost, sla.estimated_execution_time = cost_model.postprocess_query(sla.to_dict())

    # Default case
    return sla


# noinspection PyUnreachableCode
def quotation(query_graph, cost_model, sensivity_factor=1.1) -> Dict[tuple, Quotation]:
    """Finds the best Quotation for each category in a query graph.

    query_graph: nx.DiGraph

    sensivity_factor : float (Optionnal, Default = 1.1)
        Tolerance threshold described in the article.
    """

    # Computing SLAs for all nodes
    candidates_sla = dict()
    for n in query_graph :
        query_graph.nodes[n]["sla"] = __node_sla(n, query_graph, cost_model)
        if len(list(query_graph.successors(n))) == 0 :
            candidates_sla[n] = query_graph.nodes[n]["sla"]

    slas = {}
    for n, sla in candidates_sla.items():
        if all(  # Filtering out already registered slas with same price and response time
                sla.total_cost != sla_2.total_cost
                and sla.estimated_execution_time != sla_2.estimated_execution_time
                for n_2, sla_2 in slas.items()
        ) and not any(  # Filtering out uncompetitive slas
            sla_2.total_cost < sla.total_cost
            and sla_2.estimated_execution_time <= sla.estimated_execution_time * sensivity_factor
            for n_2, sla_2 in candidates_sla.items()
            if n != n_2
        ):
            slas[n] = sla

    return slas
