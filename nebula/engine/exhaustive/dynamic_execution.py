import datetime
import logging

from nebula import CONST_NETWORK_BANDWIDTH
from nebula.engine.exhaustive.base_execution import ExecutionEngine, TaskWorker

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)-5s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

from threading import Barrier, BrokenBarrierError
from typing import Tuple, List, Dict

import networkx as nx

from nebula.database.metabase import Provider


class ReasoningError(Exception):
	def __init__(self, worker):
		super().__init__(f"Worker {worker} (or one of their friends) was dumb...")


class DynamicEngine(ExecutionEngine):
	def __init__(self,
				 cost_model, query_name, estimation, quotation, tolerance=1.1):
		super().__init__(
			cost_model, query_name, estimation, quotation)

		quotation_obj = estimation["tender"][quotation]
		self.agents: List[DynamicTaskWorker] = list()
		self.last_agent: DynamicTaskWorker = None
		self.barrier_start: Barrier = None
		self.quotation = quotation_obj
		self.tolerance = tolerance
		self.target_m = quotation_obj.total_cost
		self.target_d = quotation_obj.estimated_execution_time
		self.graph = quotation_obj.graph

		self.starting_time = None # Initialised during execute

		self.bill.estimated_execution_time = quotation_obj.estimated_execution_time


	def update_after_execution(self, node, relation_size, executed=False):
		"""Updates statistics for forthcoming queries"""
		# self.graph_lock.acquire()
		ratio = relation_size / self.graph.nodes[node]["output_size"]
		self.graph.nodes[node]["output_size"] = relation_size

		for s in self.graph.successors(node):
			self.update_values_rec(node, ratio)

		if executed:
			self.graph.nodes[node]["executed"] = True

	# self.graph_lock.release()

	def update_values_rec(self, node, base_ratio):
		successors = self.graph.successors(node)

		for s in successors:
			self.graph.edges[(node, s)]["total_time"] *= base_ratio
			self.graph.edges[(node, s)]["moving_data"] *= base_ratio
			self.graph.edges[(node, s)]["cost"] *= base_ratio
		# self.graph.nodes[s]["output_size"] *= (base_ratio / depth)
		# self.graph.nodes[s]["input_size"] *= (base_ratio / depth)
		# self.graph.nodes[s]["total_time"] *= (base_ratio / depth)
		# self.graph.nodes[s]["total_cost"] *= (base_ratio / depth)
		# Update
		# self.update_values_rec(s, base_ratio, depth+1)

	def compute_migrate_data(self, node):
		"""To be used under a lock"""

		succs = self.graph.successors(node)

		# Default case
		logging.info(self.graph.nodes[node]["output_size"])
		moved_data = self.graph.nodes[node]["output_size"]
		forecasted_data = moved_data
		for s in succs:
			if node[0] == s[0]:
				self.graph.edges[(node, s)]["moving_data"] = moved_data
				self.graph.edges[(node, s)]["output_size"] = moved_data
			else:
				self.graph.edges[(node, s)]["moving_data"] = moved_data * 2
				self.graph.edges[(node, s)]["output_size"] = moved_data

	# self.update_values_rec(node, change_ratio)

	def __setup_agents(self):
		"""Populates self.agents list"""

		# Exploring graph
		for n in self.graph.nodes:
			node_preds = list(self.graph.predecessors(n))
			# self.compute_migrate_data(n)

			# Finding base queries
			if len(node_preds) == 0:
				# Computing edges weight

				# Create agents
				self.agents.append(
					DynamicTaskWorker(
						self,
						n[0],  # Provider
						self.graph.nodes[n]["sql"],  # Subquery,
						self.graph.nodes[n]["qname"],  # Query name,
						n  # Node
					)
				)

				self.graph.nodes[n]["barrier"] = Barrier(1)

			else:
				self.graph.nodes[n]["barrier"] = Barrier(2)

	def init_edges(self):
		for e in self.graph.edges:
			data = self.graph.nodes[e[0]]["output_size"]
			self.graph.edges[e]["output_size"] = data
			if e[0][0] == e[1][0]:
				self.graph.edges[e]["moving_data"] = data
			else:
				self.graph.edges[e]["moving_data"] = data * 2

	def execute(self):
		"""Executes the query. Returns a bill"""
		self.starting_time = datetime.datetime.now()
		self.__setup_agents()
		self.barrier_start = Barrier(len(self.agents) + 1)

		# Init edges
		self.init_edges()

		# Runs agents
		for a in self.agents:
			a.start()

		start = datetime.datetime.now()
		self.barrier_start.wait()
		# Wait for agents to be done
		for a in self.agents:
			a.join()

		if any(a.error is not None for a in self.agents) :
			raise RuntimeError("An error occured during the execution...")

		ex_time = datetime.datetime.now() - start
		self.bill.response_time = ex_time.total_seconds() * 1000

		for a in self.agents:
			self.bill += a.bill

		# Cleaning
		self.clean(self.last_agent)

		self.plan = self.last_agent.node
		return self.bill


class DynamicTaskWorker(TaskWorker):

	def __init__(self, engine: DynamicEngine, p_origin: Provider, subquery: str, subquery_name: str,
				 node: Tuple[Provider]):
		super().__init__(engine, p_origin, subquery, subquery_name, node)
		self.work = True
		self.error = None
		self.cleaning_threads = list()

	def __reset(self, new_node):
		self.p_origin = new_node[0]
		self.subquery = self.engine.graph.nodes[new_node]["sql"]
		self.subquery_name = self.engine.graph.nodes[new_node]["qname"]
		self.end_name = self.subquery_name
		self.p_destination = None
		self.node = new_node

	def __tree_sats(self, e):
		trav = list(nx.traversal.bfs_edges(self.engine.graph, e, 'reverse'))

		# Compute cost & time values
		cost = sum(self.engine.graph[t[1]][t[0]]["cost"] for t in trav)
		cost += self.engine.graph.nodes[e]["cost"]

		time = max(
			nx.algorithms.shortest_paths.shortest_path_length(
				self.engine.graph, source=p, target=e, weight="total_time"
			) for p in self.engine.graph.nodes
			if len(list(self.engine.graph.predecessors(p))) == 0
			and len(set(p) & set(e)) > 0  # Prevent unreachable paths to be computed
		)

		ratio = abs(time / cost - self.engine.target_d / self.engine.target_m)

		# Finding the next node within the tree
		potential_nexts = set(self.engine.graph.successors(self.node))
		potential_nexts &= set(e for t in trav for e in t)
		next_n = list(potential_nexts)[0]

		# Check for first-choice eligibility
		is_first_choice = time <= 1.1 * self.engine.target_d \
						  and cost <= 1.1 * self.engine.target_m
		return {
			"cost": cost,
			"time": time,
			"ratio": ratio,
			"first_choice": is_first_choice,
			"next": next_n
		}

	def towards_best_tree(self):
		# Finding all execution plan end to try to reach
		n_anti_tree = nx.traversal.dfs_tree(self.engine.graph, self.node)
		eligible_sinks = [n for n in n_anti_tree.nodes if len(list(n_anti_tree.successors(n))) == 0]

		# Building the trees & computing their values
		trees = {e: self.__tree_sats(e) for e in eligible_sinks}

		try:
			next_n = min([v for v in trees.values() if v["first_choice"]], key=lambda x: x["time"])
		except ValueError:
			next_n = min([v for v in trees.values()], key=lambda x: x["ratio"])

		return next_n["next"]

	def migrate_decision(self):
		"""Selects the next node of the graph. To be used under a lock"""
		choice = None

		# Fetching next possible subqueries
		next_steps = list(self.engine.graph.successors(self.node))
		succ_ready = [s for s in next_steps if self.engine.graph.nodes[s]["ready"]]

		# There is no alternative
		if len(next_steps) == 1:
			choice = next_steps[0]

		# Checking for already ready successors
		elif len(succ_ready) > 0:
			choice = succ_ready[0]

		# Last case : determining which next choice is best
		elif len(next_steps) > 1:
			choice = self.towards_best_tree()
		logging.debug(f"{str(self)} Choice for {self.node} : {choice}")
		return choice

	def migration_reasoning(self) -> Tuple[Provider]:
		"""Returns the migration destination as a node in the query's graph"""

		logging.info(f"{str(self)} for {self.subquery_name} is taking a decision.")
		choice = self.migrate_decision()

		if choice is not None:
			self.engine.graph.nodes[choice]["ready"] = True
			self.upate_graph_migration(self.node, choice)

		logging.info(f"{str(self)} for {self.subquery_name} choose {str(choice)}")

		# self.engine.graph_lock.release()
		return choice

	def upate_graph_migration(self, source, destination):
		"""To be used under a a lock with self.engine.graph_lock!"""

		# Keeping the destination th the graph
		whitelist = {destination}

		# Keeping the destination's ancestors
		whitelist |= nx.algorithms.dag.ancestors(self.engine.graph, destination)

		# Keeping the destination's descendants
		descendants = nx.algorithms.dag.descendants(self.engine.graph, destination)
		whitelist |= descendants

		# Keeping all the ancestors of the descendants
		whitelist |= {p for d in descendants for p in nx.algorithms.dag.ancestors(self.engine.graph, d)}

		# Removing all other nodes
		self.engine.graph.remove_nodes_from(set(self.engine.graph.nodes) - whitelist)

	def migrate(self, costs, next_node) -> bool:
		"""Returns true if data transfer happened"""
		logging.info(f"{self} - migrate - {self.p_destination} - {self.p_origin}")
		if not self.p_destination in (None, self.p_origin):

			start = datetime.datetime.now()
			self.transfer_data(self)
			ex_time = datetime.datetime.now() - start
			self.bill.query_total_transfer_time += ex_time.total_seconds() * 1000

			# Adding transfert cost
			self.bill.query_transfer_cost += costs["query"]["export_cost"]
			self.bill.total_cost += costs["query"]["export_cost"]

			# Storage cost at destination
			storage_cost = costs["storage"]["size"] + self.p_destination.export
			self.bill.query_storage_cost += storage_cost
			self.bill.total_cost += storage_cost

			# Data transfers logging
			self.bill.transferred_data += costs["storage"]["size"]

			return True

		else:
			# Store query results
			self.bill.query_storage_cost += costs["storage"]["price"]
			self.bill.total_cost += costs["storage"]["price"]

			return False

	def __update_node(self, n, new_m, new_t, new_s):
		self.engine.graph.nodes[n]["cost"] = new_m
		self.engine.graph.nodes[n]["time"] = new_t
		self.engine.graph.nodes[n]["output_size"] = new_s

	def __update_edge(self, n, s, new_t, new_s):
		# Re-compute the time
		self.engine.graph[n][s]["total_time"] = new_t
		if n[0] != s[0]:
			time = new_s * CONST_NETWORK_BANDWIDTH
			self.engine.graph[n][s]["total_time"] += time
			self.engine.graph[n][s]["time"] += time

		# Re-compute the cost
		if n[0] == s[0]:
			self.engine.graph[n][s]["cost"] = 0
		else:
			coef = n[0].export + s[0].storage
			self.engine.graph[n][s]["cost"] = new_s * coef

		# Update remaining values
		self.engine.graph[n][s]["size"] = new_s

	def __graph_update(self, n, new_m, new_t, new_s, impact=1.0):
		"""Updates the DAG using the newly computed values"""

		# Computing change ratios
		old_s = self.engine.graph.nodes[n]["output_size"]

		# Set new values fot the node
		self.__update_node(n, new_m, new_t, new_s)

		# Re-estimate following nodes
		for s in self.engine.graph.successors(n):
			self.__update_edge(n, s, new_t, new_s)

			# Compute re-estimation impact
			old_is = self.engine.graph.nodes[s]["input_size"]
			self.engine.graph.nodes[s]["input_size"] = sum(
				self.engine.graph.nodes[p]["output_size"] for p in self.engine.graph.predecessors(s))
			impact = (self.engine.graph.nodes[s]["input_size"] + 10e-10) / (old_is + 10e-10)

			# Re-evaluate reccursively
			self.__graph_update(
				s,
				self.engine.graph.nodes[s]["cost"] * impact,
				self.engine.graph.nodes[s]["time"] * impact,
				self.engine.graph.nodes[s]["output_size"] * impact
			)

	def __update_cost_model_postprocessor(self):
		# Building the Y vector
		response_time = datetime.datetime.now() - self.engine.starting_time
		response_time = response_time.total_seconds() * 1000

		self.engine.cost_model.learn_postprocess(
			self.engine.graph.nodes[self.node]["sla"].to_dict(),
			(self.bill.total_cost, response_time)
		)

	def run(self):
		try :
			self.engine.barrier_start.wait()
		except BrokenBarrierError :
			return
		logging.info(f"Agent {str(self)} is running")
		try :
			while self.work:
				logging.info(f"{str(self)} running on node {str(self.node)}")
				# Wait for node to be ready
				self.engine.graph.nodes[self.node]["barrier"].wait()

				# Query execution
				logging.info(f"{str(self)} executing {self.subquery_name}")
				costs = self.execute()
				logging.info(f"{str(self)} DEBUG --- {costs}")

				self.bill.query_total_execution_time += costs["query"]["time"]
				self.bill.query_execution_cost += costs["query"]["price"]
				self.bill.total_cost += costs["query"]["price"]
				relation_size = costs["query"]["output_size"]

				self.__update_cost_model_postprocessor()

				# Graph update
				self.engine.graph_lock.acquire()
				logging.info(f"{str(self)} UPDATING GRAPH ({self.node})")
				self.__graph_update(self.node, costs["query"]["price"], costs["query"]["time"], relation_size)
				self.engine.graph_lock.release()

				# Migration decision
				logging.info(f"{str(self)} Entering migration reasoning ({self.node})")
				self.engine.graph_lock.acquire()
				next_node = self.migration_reasoning()
				self.engine.graph_lock.release()
				logging.info(f"{str(self)} Next node for  {self.node} is {next_node}")

				if next_node == None:

					# Someone was dumb and made a choice leading to no query execution possible...
					if len(self.node) < len(self.engine.agents):
						raise ReasoningError(self)
					self.engine.last_agent = self  # In this case, the last agent who changes this is the last one remaining
					self.p_destination = None
				else:
					self.p_destination = next_node[0]

				# Transfer data. If migration happened, this thread should stop
				self.work = not self.migrate(costs, next_node)

				# Getting ready for next turn
				if next_node is not None:
					if self.work:
						self.__reset(next_node)
					# Updating the query graph
					else:
						self.engine.graph.nodes[next_node]["barrier"].wait()
				else:
					self.work = False  # In case a thread is stuck
		except Exception as e :
			self.error = e
			logging.exception("An agent failed...")
			# Killing everyone soon with a broken barrier
			for n in self.engine.graph.nodes :
				self.engine.graph.nodes[n]["barrier"].abort()

		logging.info(f"{str(self)} waits for cleaners.")
		for t in self.cleaning_threads:
			try:
				t.join()
			except RuntimeError:
				try:
					t.start()
				except RuntimeError:
					pass  # That's okay
				t.join()
		logging.info(f"{str(self)} is done.")
