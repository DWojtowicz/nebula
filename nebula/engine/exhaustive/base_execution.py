import datetime
import threading
from threading import Lock, Thread
from typing import Tuple

from nebula.database.metabase import Provider
from nebula.engine.query.query import Query
from nebula.engine.utils import Bill


class ExecutionEngine:
	def __init__(self, cost_model, query_name, estimation, plan):
		self.cost_model = cost_model
		self.query_name = query_name

		# From the estimation
		self.query: Query = estimation["query"]
		self.graph = estimation["graph"]

		# Keep track of the costs
		self.bill = Bill(providers=set(e for e in plan.replace("(", "").replace(")", "").split(", ") if len(e) > 0))
		self.bill.input_size = estimation["tender"][plan].input_size
		self.cleaning_threads = list()  # List of cleaning threads

		# Lock to protect the graph and the cost model when doing changes on them
		self.graph_lock = Lock()

	def execute(self) -> Bill:
		pass

	def clean(self, last_worker) -> None:
		# Drop intermediate tables
		print("__clean - ", last_worker.subquery.casefold())
		commands = [
			s.strip() for s in last_worker.subquery.casefold() \
				.split("from ")[1].strip() \
				.split("where ")[0].strip() \
				.split("group by ")[0].strip() \
				.split(";")[0].strip() \
				.split(",")
		]

		for t in commands:
			if t.startswith("msq_"):
				last_worker.p_origin.admin(f"DROP TABLE IF EXISTS {t};", no_update=True)

		# Rename last intermediate results to final query name
		sql = f"ALTER TABLE {last_worker.subquery_name} RENAME TO {self.query_name};"
		last_worker.p_origin.admin(sql, no_update=True)

		for t in self.cleaning_threads:
			try:
				t.join()
			except RuntimeError:
				try:
					t.start()
				except RuntimeError:
					pass  # That's okay
				t.join()


class TaskWorker(Thread):
	"""Abstract class for a task worker"""

	def __init__(self, engine: ExecutionEngine, p_origin: Provider, subquery: str, subquery_name: str,
				 node: Tuple[Provider], p_destination: Provider = None,
				 end_name: str = None):
		super().__init__()
		self.engine = engine
		self.node = node
		self.p_origin = p_origin
		self.p_destination = p_destination
		self.subquery = subquery
		self.subquery_name = subquery_name
		self.end_name = end_name if end_name is not None else self.subquery_name
		self.bill = Bill({p_origin})
		# self.bill.one_hot = self.engine.graph.nodes[tuple(e for e in [p_origin])]["one_hot"]

	def __clean_after_transfer(self, origin, destination, subquery_name):
		# Drop foreign table
		query = f"DROP FOREIGN TABLE {subquery_name}_TMP ;"
		destination.admin(query, no_update=True)

		# drop current table
		query = f"DROP TABLE {subquery_name} ;"
		origin.admin(query, no_update=True)

	def transfer_data(self, agent=None):
		"""Loads data on p_destination and deletes local table"""

		# Creates foreign table with _tmp
		query = f"IMPORT FOREIGN SCHEMA public LIMIT TO ({self.subquery_name}) "
		query = f"{query} FROM SERVER {self.p_origin.name} INTO public;"
		print("===", "ORIGIN", str(self.p_origin.name), "DESTINATION", str(self.p_destination.name), query)
		self.p_destination.admin(query, no_update=True)

		query = f"ALTER FOREIGN TABLE {self.subquery_name} RENAME TO {self.subquery_name}_tmp;"
		self.p_destination.admin(query, no_update=True)

		# Creates a table based on the foreign table
		start = datetime.datetime.now()
		query = f"CREATE UNLOGGED TABLE {self.subquery_name} WITH (autovacuum_enabled=false) AS SELECT * FROM {self.subquery_name}_tmp ;"
		self.p_destination.admin(query, no_update=True, debug=True)
		ex_time = datetime.datetime.now() - start
		ex_time = ex_time.total_seconds() * 1000
		size = self.p_destination.object_size(self.subquery_name)

		self.engine.graph_lock.acquire()
		self.engine.cost_model.learn_transfer(
			self.engine.graph, self.p_origin, self.p_destination, ex_time, x=size
		)
		self.engine.graph_lock.release()

		# Cleaning in another thread, in order to make it in parallel
		th = threading.Thread(target=self.__clean_after_transfer,
							  args=(self.p_origin, self.p_destination, self.subquery_name))
		if agent is None:
			self.engine.cleaning_threads.append(th)
		else:
			agent.cleaning_threads.append(th)
		try:
			th.start()
		except RuntimeError:
			pass  # That's okay, the thread is already somehow running

	def execute(self):
		"""Runs the query and stores its intermediate results

		Returns the costs of the query
		"""
		print("Running", self.subquery_name, "on", self.p_origin.name)
		start = datetime.datetime.now()
		res = self.p_origin.execute(
			self.subquery,
			self.end_name,
			self.subquery_name
		)

		# Training the cost model
		self.engine.graph_lock.acquire()
		self.engine.cost_model.learn_many_node(
			self.engine.graph,
			self.node,
			res["execution_plan_stats"]
		)
		self.engine.graph_lock.release()

		return res

	def execute_subquery(self):
		pass
