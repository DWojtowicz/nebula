import datetime
from typing import List, Tuple

import networkx as nx

from nebula.database.metabase import Provider
from nebula.database.utils import AdminError
from nebula.engine.exhaustive.base_execution import TaskWorker, ExecutionEngine


class StaticTaskWorker(TaskWorker):
	"""Abstract class for a task worker"""

	def __init__(self, engine, p_origin: Provider, subquery: str, subquery_name: str, node: Tuple[Provider],
				 p_destination: Provider,
				 end_name=None):
		super().__init__(engine, p_origin, subquery, subquery_name, node, p_destination)
		self.subquery_name = subquery_name
		self.end_name = end_name if end_name is not None else self.subquery_name

	def execute_subquery(self):
		"""Execute the subquery.

		Views created during Quotation generation are assumed to be already destroyed.
		"""
		# Execution of subquery
		costs = self.execute()
		self.bill.query_total_execution_time += costs["query"]["time"]
		self.bill.query_execution_cost += costs["query"]["price"]
		self.bill.total_cost += costs["query"]["price"]

		## self.engine.cost_model.learn_node()

		# Training the cost model

		# If needed, make data transfer
		if not self.p_destination in (None, self.p_origin):

			start = datetime.datetime.now()
			self.transfer_data()
			ex_time = datetime.datetime.now() - start
			self.bill.query_total_transfer_time += ex_time.total_seconds() * 1000

			# Adding transfer cost
			self.bill.query_transfer_cost += costs["query"]["export_cost"]
			self.bill.total_cost += costs["query"]["export_cost"]

			# Storage cost at destination
			storage_cost = costs["storage"]["size"] + self.p_destination.export
			self.bill.query_storage_cost += storage_cost
			self.bill.total_cost += storage_cost

			# Data transfers logging
			self.bill.transferred_data += costs["storage"]["size"]

		# Training the cost model

		else:
			# Store query results
			self.bill.query_storage_cost += costs["storage"]["price"]
			self.bill.total_cost += costs["storage"]["price"]

		print(f"ON f{self.p_origin.name}, query {self.subquery_name} has been executed.")


class SingleTaskWorker(StaticTaskWorker):
	"""Worker for single-provider sub-queries. There can be many in one query."""

	def __init__(self, engine, p_origin: Provider, subquery: str, subquery_name: str, node: Tuple[Provider],
				 p_destination: Provider, end_name=None):
		super().__init__(engine, p_origin, subquery, subquery_name, node, p_destination, end_name)

	def run(self):
		self.execute_subquery()


class JoinTaskWorker(StaticTaskWorker):
	"""Worker for multi-cloud sub-queries.
	When origin and destination provider are the same, it means the query ends"""

	def __init__(self, engine, p_origin: Provider, subquery: str, subquery_name: str, node: Tuple[Provider],
				 p_destination: Provider,
				 predecessors: List[TaskWorker], end_name=None):
		super().__init__(engine, p_origin, subquery, subquery_name, node, p_destination, end_name)
		self.predecessors = predecessors

	def __clean(self):
		"""Deletes intermediate results"""

		# Getting intermediate relations names
		print("CLEANING - ", self.subquery)

		q = self.subquery.casefold() \
			.split("from ")[1] \
			.split(";")[0].strip() \
			.split("where")[0].strip() \
			.split("group by")[0].strip()

		relations = [s[0].strip().split(" ")[0].strip() for s in q.split(",")]

		# Getting real Relation objects for this
		relations = [r for r in self.p_origin.relations if r.name in relations]
		for r in relations:
			self.p_origin.admin(f"DROP {r.rel_type} {r.name} ;")

	def run(self):
		# Wait for the predecessors to end
		for t in self.predecessors:
			t.join()
			# Collecting cost data
			self.bill += t.bill

		# Execute the query
		self.execute_subquery()

		# Cleans predecessors stuff
		self.__clean()


class StaticEngine(ExecutionEngine):
	def __init__(self, cost_model, query_name, estimation, plan):
		super().__init__(cost_model, query_name, estimation, plan)
		self.plan = estimation["tender"][plan].plan_node
		self.bill.estimated_execution_time = estimation["tender"][plan].estimated_execution_time

	def __add_stuff_rec(self, T, n, depth=0):
		"""Recursively extracts data from the query graph into the execution tree"""
		predecessors = list(self.graph.predecessors(n))

		# Adding current node
		T.add_node(n)
		T.nodes[n]["depth"] = depth

		for k in self.graph.nodes(data=True)[n]:
			T.nodes[n][k] = self.graph.nodes(data=True)[n][k]

		# Recursive call for building the tree
		depth += 1
		new_depth = depth
		for p in predecessors:
			new_depth = self.__add_stuff_rec(T, p, depth)

			# Linking current node with predecessors
			T.add_edge(p, n)
			for k in self.graph[p][n]:
				T[p][n][k] = self.graph[p][n][k]

		return new_depth

	def __execution_tree(self) -> Tuple[nx.DiGraph, int]:
		"""Builds the execution tree"""
		T = nx.DiGraph()
		depth = self.__add_stuff_rec(T, self.plan)
		return T, depth

	def __generate_workers(self, T, n, workers=list(), successor=None):
		"""Creates TaskWorker objects and store them in workers parameter. Returns the last worker created.
		Params
		------
		T: nx.DiGraph
			Execution tree of the query
		n: Tuple[Provider]
			Node in the execution tree
		workers: list
			Default: list()
			List of the workers generated so far
		successor: Provider
			Default: None
			The provider which will receive the sub-query's results
		"""
		pred_workers = list()  # Contains the predecessors
		for p in T.predecessors(n):
			pred_workers.append(self.__generate_workers(T, p, workers, successor=n[0]))

		# Case when this is a JoinQuery
		if len(pred_workers) > 0:
			cur_worker = JoinTaskWorker(self, n[0], T.nodes[n]["sql"], T.nodes[n]["qname"], n, successor, pred_workers)
		else:
			cur_worker = SingleTaskWorker(self, n[0], T.nodes[n]["sql"], T.nodes[n]["qname"], n, successor)

		workers.append(cur_worker)
		return cur_worker

	def __clean(self, last_worker) -> None:
		# Drop intermediate tables
		print("__clean - ", last_worker.subquery.casefold())
		tables = [
			s.strip() for s in last_worker.subquery.casefold()
				.split("from ")[1].strip()
				.split("where ")[0].strip()
				.split("group by ")[0].strip()
				.split(";")[0].strip()
				.split(",")
		]

		for t in tables:
			if t.startswith("msq_"):
				last_worker.p_origin.admin(f"DROP TABLE IF EXISTS {t};", no_update=True)

		# Rename last intermediate results to final query name
		sql = f"ALTER TABLE {last_worker.subquery_name} RENAME TO {self.query_name};"
		last_worker.p_origin.admin(sql, no_update=True)

	def execute(self):
		"""Executes the query.

		1. Builds the execution tree
		2. Spawn workers
		3. Returns the last worker's costs
		"""
		# Extracting execution tree
		T, depth = self.__execution_tree()

		# Thread preparation
		workers = list()
		last_worker = self.__generate_workers(T, self.plan, workers)

		# Run workers
		start = datetime.datetime.now()
		for w in workers:
			w.start()
		last_worker.join()
		ex_time = datetime.datetime.now() - start
		self.bill.response_time = ex_time.total_seconds() * 1000

		# Bills compilation
		for w in workers:
			self.bill += w.bill

		# Cleaning stuff
		try:
			self.__clean(last_worker)
		except AdminError:
			pass  # Already cleaned somehow

		return self.bill
