"""Comparison point for the cost model
    - Selectivity estimation : Max's Bayesian inference
    - Response time estimation : 5-NN regression
"""
import logging
from statistics import mean

from river import base
from river.base import Regressor
from river.compose import Select, Renamer
from river.linear_model import LinearRegression
from river.neighbors import KNNRegressor
from river.optim import AdaDelta
from river.optim.losses import Cauchy
from river.preprocessing import StandardScaler
from river.tree import HoeffdingTreeClassifier


class AverageRegressor(Regressor):
    """Bagging step returning the average prediction from several models."""

    def learn_one(self, x: dict, y: base.typing.RegTarget, **kwargs) -> "Regressor":
        """Does nothing since the only value returned is an average"""
        return self

    def predict_one(self, x: dict) -> base.typing.RegTarget:
        return mean([i for i in x.values()])


class ProviderRegressor(Regressor):
    def __init__(self, feature):
        """Always returns the providers' DBMS estimate.

        :param feature: Feature to be returned
        """
        self.feature = feature

    def learn_one(self, x: dict, y: base.typing.RegTarget, **kwargs) -> "Regressor":
        """Does nothing since it returns the provider's DBMS estimate"""
        return self

    def predict_one(self, x: dict) -> base.typing.RegTarget:
        return x[self.feature]


class MetaHoeffdingRegressor(Regressor):
    """Meta-classifier picking the best """

    def __init__(self, mapping):
        super().__init__()

        logging.info(f"Metamodel mapping: {mapping}")

        self.classifier = StandardScaler() | HoeffdingTreeClassifier(
            grace_period=20,
            split_confidence=0.55,
            split_criterion='gini',
            remove_poor_attrs=True
        )

        self.mapping = mapping

    def __vectorize(self, X):
        # return np.array(X[k] for k in self.mapping)
        return {k: v for k, v in X.items() if k[-2] != "_"}

    def learn_one(self, x, y, **kwargs) -> Regressor:
        preds = {k: abs(float(y) - float(v)) for k, v in x.items() if k[-1] == "y"}
        best_model = min(preds, key=preds.get)
        y = self.mapping.index(best_model)
        x_vect = self.__vectorize(x)
        self.classifier.learn_one(x_vect, y)

        return self

    def predict_one(self, x):
        """Returns PostgreSQL's output cardinality estimation."""
        x_vect = self.__vectorize(x)

        classifier_out = self.classifier.predict_one(x_vect)

        try:
            best_model = self.mapping[classifier_out]
            return (x[best_model], best_model)

        except TypeError:  # The first time the tree must predict something, it returns None
            logging.warning("HoeffdingTree returning a default value")
            return (x["x_0"], "cold_start")


class EnsembleLearningStack(Regressor):
    def __init__(self, default_feature, smart_threshold=20):
        self.default_feature = default_feature
        self.__cpt = 0
        self.smart_threshold = smart_threshold

        self.renamer = Renamer({
            "old_output_size": "x_0",
            "prov_card": "x_0",
            "est_card": "x_0",
            "rows": "x_0",
            "estimated_cardinality": "x_0",
            "est_resp_time": "x_1",
            "estimated_execution_time": "x_1",
            "est_time": "x_1",
            "pg_join_count": "x_2",
            "joins": "x_2",
            "width": "x_3",
            "pg_plan_depth": "x_4",
            "depth": "x_4",
            "input_size": "x_5"
        })

        l_1_select = Select("x_0", "x_1", "x_2")

        self.l_1_linreg = (
                l_1_select
                | StandardScaler()
                | LinearRegression(optimizer=AdaDelta(), loss=Cauchy(), l2=2.95)
        )
        self.l_1_knn = (
                l_1_select
                | KNNRegressor(n_neighbors=6, aggregation_method="weighted_mean")
        )
        self.l_1_prov = (
                l_1_select
                | ProviderRegressor(self.default_feature)
        )

        l_2_select = Select("l_1_prov_y", "l_1_linreg_y", "l_1_knn_y")

        self.l_2_knn = (
                l_2_select
                | KNNRegressor(n_neighbors=3, aggregation_method="weighted_mean")
        )
        self.l_2_avg = (
                l_2_select
                | AverageRegressor()
        )

        self.l_3_avg = (
                Select("l_1_prov_y", "l_1_linreg_y", "l_1_knn_y", "l_2_knn_y", "l_2_avg")
                | AverageRegressor()
        )

        self.meta_model_select = Select("x_0", "x_1", "x_2", "x_3", "x_4", "x_5", "l_1_prov_y", "l_1_linreg_y",
                                        "l_1_knn_y", "l_2_knn_y", "l_2_avg", "l_3_avg")

        self.meta_model = (
                self.meta_model_select
                | MetaHoeffdingRegressor(
            mapping=["l_1_prov_y", "l_1_linreg_y", "l_1_knn_y", "l_2_knn_y", "l_2_avg", "l_3_avg"])
        )

        self.history = list()

    def learn_one(self, x, y, add_to_history=True, **kwargs) -> Regressor:
        # In case feature names are off
        self.__cpt += 1
        x = self.renamer.transform_one(x)

        l_1_y = {
            "l_1_linreg_y": self.l_1_linreg.predict_one(x),
            "l_1_knn_y": self.l_1_knn.predict_one(x),
            "l_1_prov_y": self.l_1_prov.predict_one(x),
        }

        self.l_1_linreg.learn_one(x, y)
        self.l_1_knn.learn_one(x, y)

        l_2_y = {
            "l_2_knn_y": self.l_2_knn.predict_one(l_1_y),
            "l_2_avg": self.l_2_avg.predict_one(l_1_y)
        }

        self.l_2_knn.learn_one(l_1_y, y)

        l_3_y = {
            "l_3_avg": self.l_3_avg.predict_one(l_1_y | l_2_y)
        }

        x = x | l_1_y | l_2_y | l_3_y

        y_pred, model = self.meta_model.predict_one(x)
        self.meta_model.learn_one(x, y)

        x_bis = self.meta_model_select.transform_one(x)
        x_bis |= {
            "y_pred": y_pred,
            "chosen_model": model,
            "y_truth": y,
            "default": self.__cpt < self.smart_threshold
        }

        if add_to_history:
            self.history.append(x_bis)

        return self

    def predict_one(self, x):

        # In case feature names are off
        x = self.renamer.transform_one(x)

        if self.__cpt < self.smart_threshold:
            y = x[self.default_feature]
            model = "default"

        else:
            l_1_y = {
                "l_1_linreg_y": self.l_1_linreg.predict_one(x),
                "l_1_knn_y": self.l_1_knn.predict_one(x),
                "l_1_prov_y": self.l_1_prov.predict_one(x),
            }

            l_2_y = {
                "l_2_knn_y": self.l_2_knn.predict_one(l_1_y),
                "l_2_avg": self.l_2_avg.predict_one(l_1_y)
            }

            l_3_y = {
                "l_3_avg": self.l_3_avg.predict_one(l_1_y | l_2_y)
            }

            x = x | l_1_y | l_2_y | l_3_y

            y, model = self.meta_model.predict_one(x)

        return y
