from river.linear_model import LinearRegression
from river.linear_model import LinearRegression
from river.neighbors import KNNRegressor
from river.optim.initializers import Constant, Zeros

from nebula.cost_models.cost_model import CostModel
from nebula.cost_models.cost_model_utils import EnsembleLearningStack
from nebula.cost_models.quotation_postprocessing import QuotationPostprocessor
from nebula.engine.quotation import Quotation
from nebula.engine.utils import Bill


class LiteratureCostModel(CostModel):
    """Comparison point for the cost model
        - Selectivity estimation : 5-NN regression from Kleerekoper et al. (2019)
        - Response time estimation : 5-NN regression from Kleerekoper et al. (2019)
    """

    def __init__(self, providers):
        super().__init__(providers)
        self.sub_query_models = {
            p: {
                "cardinality": KNNRegressor(n_neighbors=5),
                "response_time": KNNRegressor(n_neighbors=5)
            }
            for p in providers
        }

        self.card_history = {p : [] for p in providers}
        self.rt_history = {p : [] for p in providers}
        self.net_history = {p1 : {p2 : list() for p2 in providers} for p1 in providers}

    def learn_transfer(self, G, s, t, y, x=None):
        """Nothing to learn, just storing the values"""
        res = self.estimate_transfer(G, s, t, x)
        self.net_history[s[0].name][t[0].name].append((x, res, y))
        pass

    def learn_many_node(self, G, n, execution_plan_data):
        pass

    def estimate_node(self, G, n):
        return (
            self.sub_query_models[n[0].name]["cardinality"].predict_one({"x": G.nodes[n]["tender"].estimated_cardinality}),
            self.sub_query_models[n[0].name]["response_time"].predict_one({"x": G.nodes[n]["tender"].estimated_execution_time})
        )

    def learn_node(self, G, n, y):
        print("LEARNIG NODE")
        self.card_history[n[0].name].append(({"x": G.nodes[n]["tender"].estimated_cardinality}, y[0]))
        self.rt_history[n[0].name].append(({"x": G.nodes[n]["tender"].estimated_execution_time}, y[1]))

        self.sub_query_models[n[0].name]["cardinality"].learn_one({"x": G.nodes[n]["tender"].estimated_cardinality}, y[0])
        self.sub_query_models[n[0].name]["response_time"].learn_one({"x": G.nodes[n]["tender"].estimated_execution_time}, y[1])


    def learn_many_node(self, G, n, execution_plan_data):
        self.learn_node(G, n, (execution_plan_data[-1]["actual_rows"], execution_plan_data[-1]["actual_time"]))
        in_size = G.nodes[n]["tender"].provider_dict["query"]["input_size"]

        for e in reversed(execution_plan_data[:-1]):  # Whole sub-query into is the last element
            e["input_size"] = in_size
            self.sub_query_models[n[0].name]["cardinality"].learn_one({"x": e["rows"]}, e["actual_rows"])
            self.sub_query_models[n[0].name]["response_time"].learn_one({"x": e["est_time"]}, e["actual_time"])


class StackedCostModel(CostModel):
    def __init__(self, provider_list, query_threshold=25):
        super().__init__(provider_list)
        self.sub_query_models = {
            p: {
                "cardinality": EnsembleLearningStack("x_0"),
                "response_time": EnsembleLearningStack("x_1")
            }
            for p in provider_list
        }

        self.query_model = None
        self.network_model = dict()

        for p1 in provider_list:
            for p2 in provider_list:
                self.net_history[(p1, p2)] = list()
                if p1 == p2:
                    self.network_model[(p1, p2)] = LinearRegression(intercept_init=0.0, initializer=Zeros())
                else:
                    self.network_model[(p1, p2)] = LinearRegression(intercept_init=0.0,
                                                                    initializer=Constant(self.network_bandwidth))

    def estimate_node(self, G, n):
        x = G.nodes[n]["tender"].provider_dict["query"]
        return (
            self.sub_query_models[n[0].name]["cardinality"].predict_one(x.copy()),
            self.sub_query_models[n[0].name]["response_time"].predict_one(x.copy())
        )

    def estimate_transfer(self, G, s, t, x):
        try : # Compatibility for respectively exhaustive and randomised strategies
            return self.network_model[(s[0].name, t[0].name)].predict_one({"x": x})
        except :
            return self.network_model[(s.provider.name, t.provider.name)].predict_one({"x": x})

    def learn_node(self, G, n, y):
        x = G.nodes[n]["tender"].provider_dict["query"]
        est_card, est_rt = self.estimate_node(G, n)

        self.card_history[n[0].name].append((est_card, y[0]))
        self.rt_history[n[0].name].append(est_rt, y[1])

        self.sub_query_models[n[0].name]["cardinality"].learn_one(x.copy(), y[0])
        self.sub_query_models[n[0].name]["response_time"].learn_one(x.copy(), y[1])

    def learn_many_node(self, G, n, execution_plan_data):
        remember = True
        in_size = G.nodes[n]["tender"].provider_dict["query"]["input_size"]
        for e in reversed(execution_plan_data):  # Whole sub-query into is the last element
            e["input_size"] = in_size
            self.sub_query_models[n[0].name]["cardinality"].learn_one(e.copy(), e["actual_rows"], remember)
            self.sub_query_models[n[0].name]["response_time"].learn_one(e.copy(), e["actual_time"], remember)
            remember = False

    def learn_transfer(self, G, s, t, y, x=None):
        y_pred = self.network_model[(s[0].name, t[0].name)].predict_one({"x": x})
        self.net_history[(s[0].name, t[0].name)].append({"x": x, "y_pred": y_pred, "y_truth": y})
        self.network_model[(s[0].name, t[0].name)].learn_one({"x": x}, y)


class PostprocessedStackedCostModel(StackedCostModel):
    def __init__(self, provider_list):
        super().__init__(provider_list)
        self.postprocessor = QuotationPostprocessor()

    def postprocess_query(self, X):
        """

        :param X: Dictionary with input_size, total_cost and estimated_execution_time
        :return: Tuple price, response time
        """
        return self.postprocessor.postprocess(X)

    def learn_postprocess(self, X, Y):
        """Y : Tuple price, response time"""
        if type(X) is Quotation:
            X = X.to_dict()

        if type(Y) is Bill :
            Y = (Y.total_cost, Y.response_time)

        self.postprocessor.learn_one(X, Y)
