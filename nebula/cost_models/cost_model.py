from abc import ABC, abstractmethod

from nebula import CONST_NETWORK_BANDWIDTH
from nebula.cost_models.quotation_postprocessing import QuotationPostprocessor
from nebula.database.metabase import Provider
from nebula.engine.quotation import Quotation
from nebula.engine.utils import Bill


class CostModel(ABC):
    def __init__(self, providers, network_bandwidth=CONST_NETWORK_BANDWIDTH):
        self.network_bandwidth = network_bandwidth

        self.card_history = {p : [] for p in providers}
        self.rt_history = {p : [] for p in providers}
        self.net_history = {p1 : {p2 : list() for p2 in providers} for p1 in providers}
        self.disable_cost_model = False

    @abstractmethod
    def estimate_node(self, G, n):
        """

        :param G: Query's graph
        :param n: A node modeling a sub-query
        :return: A tuple (response_time, output_cardinality)
        """
        pass

    def estimate_transfer(self, G, s, t, x):
        """Returns a response time for the transfer between two providers in seconds.

        :param G: Query's graph
        :param s:  Source node
        :param t:  Target node
        :param x: The amount of data to transfer
        :return:  A tuple (response_time, monetary_cost)
        """
        if type(s) in (tuple, Provider) and s[0] == t[0]:
            return 0.0
        else:
            return x * self.network_bandwidth


    def postprocess_query(self, X):
        """

        :param X: Dictionary with input_size, total_cost and estimated_execution_time
        :return: Tuple price, response time
        """
        return (X["total_cost"], X["estimated_execution_time"])

    def learn_postprocess(self, X, y):
        """
        :param X: Dictionary with input_size, total_cost and estimated_execution_time
        :param Y: Tuple price, response time
        """
        pass

    @abstractmethod
    def learn_transfer(self, G, s, t, y, x=None):
        """
        y in seconds !
        :param G: Query's graph
        :param s:  Source node
        :param t:  Target node
        :param y: Ground-truth in seconds
        """
        pass

    @abstractmethod
    def learn_node(self, G, n, y):
        """
        :param G: Query's graph
        :param n: A node modeling a sub-query
        :param y: A tuple (response_time, output_cardinality)
        """
        pass

    @abstractmethod
    def learn_many_node(self, G, n, execution_plan_data):
        """ Uses full execution plan to learn about a query
        :param G: Query's graph
        :param n: A node modeling a sub-query
        :param y: A tuple (response_time, output_cardinality)
        :param execution_plan_data: Field "execution_plan_stats" from the bills returned by providers' `execute`
        pass
        """

class NebulaCostModel(CostModel):

    def __init__(self, providers):
        super().__init__(providers)
        self.postprocessor = QuotationPostprocessor()

    def estimate_node(self, G, n):
        return (G.nodes[n]["tender"].estimated_execution_time, G.nodes[n]["tender"].estimated_cardinality)

    def postprocess_query(self, X):
        """

        :param X: Dictionary with input_size, total_cost and estimated_execution_time
        :return: Tuple price, response time
        """
        return self.postprocessor.postprocess(X)

    def learn_node(self, G, n, y):
        pass

    def learn_many_node(self, G, n, execution_plan_data):
        pass

    def learn_transfer(self, G, s, t, y, x=None):
        pass

    def learn_postprocess(self, X, Y):
        """y : Tuple price, response time"""

        if type(X) is Quotation:
            X = X.to_dict()

        if type(Y) is Bill :
            Y = (Y.total_cost, Y.response_time)

        self.postprocessor.learn_one(X, Y)
