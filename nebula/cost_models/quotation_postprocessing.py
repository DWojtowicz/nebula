import copy

from river.compose import Select
from river.linear_model import LinearRegression
from river.neighbors import KNNRegressor
from river.optim import AdaDelta
from river.optim.losses import Cauchy
from river.preprocessing import StandardScaler

from nebula.engine.quotation import Quotation
from nebula.engine.utils import Bill


class QuotationPostprocessor(object):
    """
    Singleton code inspired from https://blog.hbis.fr/2019/03/23/python-singleton/
    """

    def __init__(self):
        """Initialize the instance"""

        # Cold-start
        self.cpt_queries = 0
        self.corr_threshold = 25

        # Models
        selector = Select("input_size", "total_cost", "estimated_execution_time", "nb_providers")
        self.price_corrector = (selector
                                # | StandardScaler()
                                | LinearRegression(optimizer=AdaDelta(), loss=Cauchy(), l2=2.95)
                                )

        self.time_corrector = (selector | KNNRegressor(n_neighbors=5))
        self.history = list()

    def postprocess(self, X):
        """

        :param X: Dictionary with input_size, total_cost and estimated_execution_time
        :return: Tuple price, response time
        """
        if self.cpt_queries < self.corr_threshold:
            return X["total_cost"], X["estimated_execution_time"]

        return self.price_corrector.predict_one(X), self.time_corrector.predict_one(X)

    def learn_one(self, X, Y):
        """
        :param X: Dictionary with input_size, total_cost and estimated_execution_time
        :param Y: Tuple price, response time
        """
        x_hist = copy.deepcopy(X)
        # x_hist.pop("one_hot")
        self.history.append((x_hist, Y))
        self.price_corrector.learn_one(X, Y[0])
        self.time_corrector.learn_one(X, Y[1])
        self.cpt_queries += 1
