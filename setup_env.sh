#!/bin/bash

# Installs the venv required for Nebula

python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt