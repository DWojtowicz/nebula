# Nebula - A Middleware for Multi-Cloud Query Optimisation

Nebula is a prototype middleware developed for experimental purpose while working on
the [CLOUD 2021 research article](https://ieeexplore.ieee.org/abstract/document/9582268) [1] and its subsequent BigData 2022 short paper [2] published during my thesis
at the [Université Paul Sabatier](https://www.univ-tlse3.fr/), in
the [PYRAMIDE team](https://www.irit.fr/PYRAMIDE/site/) of the [IRIT laboratory](https://www.irit.fr/) (Toulouse,
France). As described in the CLOUD 2021 paper:

> The provision of public data through various Database-as-a-Service (DBaaS) providers has recently emerged as a significant trend, backed by major organisations.
> [Nebula is] a non-profit middleware providing multi-cloud querying capabilities by fully outsourcing its users' queries to the involved DBaaS providers.
> First, [it has] a quoting procedure for those queries, whose need stems from the pay-per-query policy of the providers.
> Those quotations contain monetary cost and response time estimations, and are computed using provider-generated tenders.
> Then, [...] an agent-based dynamic optimisation engine [...] orchestrates the outsourced execution of the queries.
> Agents within this engine cooperate in order to meet the quoted values.
> [Nebula was evaluated] over simulated providers by using the Join Order Benchmark (JOB).
> Experimental results showed Nebula's approach is, in most cases, more competitive in terms of monetary cost and response time than existing work in the multi-cloud DBMS literature.

## Overall architecture

The middleware as well as simulated DBaaS providers are both implemented as HTTP APIs.

## Setup

Experiments in the paper were run with Python 3.9 using dependencies listed in `requirements.txt`.

```shell
# Getting the code
git clone https://framagit.org/DWojtowicz/nebula

# Setting up the venv
cd nebula
bash setup_venv.sh
```

Simulated providers interact with PostgreSQL, it should therefore be installed on the same machine as the provider (it
was the case during the experiments) or available on a server somewhere. PostgresSQL version used during the experiments
was 14.

## Simulated providers

Explanations about setting up the providers and their API are at [nebula/provider/README.md](nebula/provider/README.md).

## Running the middleware

Explanations about setting up the middleware and its API are at [nebula/provider/README.md](nebula/middleware/README.md).

## CLOUD 2021 experiments

We evaluated the middleware using the Join Order Benchmark [3].
[See here](https://github.com/gregrahn/join-order-benchmark) to get the queries as well as the IMDb relational database.
Once your database is set up and populated, run [cloud_2021_experiments/rename.sql](cloud_2021_experiments/rename.sql)
to get table attributes expected by the queries listed
in [cloud_2021_experiments/queries](cloud_2021_experiments/queries).

As explained in the paper, we used 3 simulated providers, running on 3 laptops connected to each other using the lab's
WiFi. The middleware was running on the same laptop as one of the providers. IMDb tables were distributed among the
providers as listed in [cloud_2021_experiments/table_distribution.md](cloud_2021_experiments/table_distribution.md).

## BigData 2022 experiments

We evaluated the middleware using the Join Order Benchmark [3] using several nodes from the [Grid'5000](https://www.grid5000.fr/) [4] testbed.
[See here](https://github.com/gregrahn/join-order-benchmark) to get the queries as well as the IMDb relational database.

A copy of the IMDb database was deployed on each simulated providers.
An instance of Nebula was deployed on a node of the grid.
Queries were generated using the JOB queries as a template (see the code in [bigdata_2022_experiments/workload_preparation](bigdata_2022_experiments/workload_preparation)).
Experiments were executed on a separate node following the `run_experiments.sh` script using the code in [bigdata_2022_experiments/execute_workload](bigdata_2022_experiments/execute_workload).

## Acknowledgements

This work has been funded by (1) the LabEx CIMI through the MCD project and (2) the French Ministries of Europe and
Foreign Affairs and of Higher Education, Research and Innovation through the EFES project (Grant number 44086TD),
Amadeus program 2020 (French-Austrian Hubert Curien Partnership – PHC). We also thank the International Cooperation &
Mobility (ICM) of the Austrian Agency for International Cooperation in Education and Research (OeAD-GmbH).

## Footnotes

[1] D. T. Wojtowicz, S. Yin, F. Morvan and A. Hameurlain, "Cost-Effective Dynamic Optimisation for Multi-Cloud
Queries," _2021 IEEE 14th International Conference on Cloud Computing (CLOUD)_, Chicago (USA, Online), 2021, pp.
387-397 [[PDF](https://hal.archives-ouvertes.fr/hal-03428073v2)]

[2] D. T. Wojtowicz, S. Yin, J. Martinez-Gil, F. Morvan and A. Hameurlain, "Multi-Cloud Query Optimisation with Accurate and Efficient Quoting," _IEEE International Conference on Big Data (BigData)_, Osaka (Japan), 2022, 6 p., _To be published_ [[PDF](https://hal-univ-tlse3.archives-ouvertes.fr/hal-03841516)]

[3] V. Leis, A. Gubichev, A. Mirchev, P. Boncz, A. Kemper, and T. Neumann, “How good are query optimizers, really?”, _
Proceedings of the VLDB Endowment_, vol. 9, no. 3, pp. 204–215, Nov.
2015. [[PDF](https://www.vldb.org/pvldb/vol9/p204-leis.pdf)]

[4] Experiments were carried out using the Grid'5000 testbed, supported by a scientific interest group hosted by Inria and including CNRS, RENATER and several Universities as well as other organizations. See [https://www.grid5000.fr/](https://www.grid5000.fr/)
