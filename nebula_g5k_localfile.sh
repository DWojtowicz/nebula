#!/bin/bash

# Runs Nebula on g5k.
#
# $1 : File with machine list

source venv/bin/activate || { echo "No venv found"; exit 1; }
export PYTHONPATH=$PYTHONPATH:$(pwd)
python3 nebula/middleware/middleware.py --g5k_prov_list $1