#!/bin/bash

# Runs Nebula.
#
# Arguments
#   $1 : TSV provider list
#   $@ : All other possible arguments for middleware.py except --prov_list

source venv/bin/activate || { echo "No venv found"; exit 1; }
export PYTHONPATH=$PYTHONPATH:$(pwd)
python3 nebula/middleware/middleware.py --prov_list $@