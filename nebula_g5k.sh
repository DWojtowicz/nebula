#!/bin/bash

# Runs Nebula on g5k.

source venv/bin/activate || { echo "No venv found"; exit 1; }
export PYTHONPATH=$PYTHONPATH:$(pwd)
python3 nebula/middleware/middleware.py --g5k_prov_list http://public.grenoble.grid5000.fr/~dwojtowicz/machines