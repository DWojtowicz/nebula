#!/bin/bash

if [ ! -d templates ]
then
  python build_templates.py
fi

python generate_workload.py -c
python workload_stats.py
python scheduler.py