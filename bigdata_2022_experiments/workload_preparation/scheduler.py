"""Schedules the queries of a G5K workload so they can be runned a bit in parallel without interfering between themselves"""
import argparse
import csv
import json
import os

import networkx as nx


def mark_graph(G, providers):
    # Marking the graph with every combination of providers
    for p1 in providers:
        for p2 in providers:
            if p1 == p2:
                G.nodes[p1]["weight"] = 1
            else:
                shortest_path = nx.shortest_path(G, p1, p2)
                # Marking the nodes of the path
                for n in shortest_path:
                    G.nodes[n]["weight"] = 1
                # Marking the edges
                for i in range(len(shortest_path) - 1):
                    G.edges[(shortest_path[i], shortest_path[i + 1])]["weight"] = 1

    return G


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        "Schedules the queries of a G5K workload so they can be runned a bit in parallel without interfering between themselves")
    parser.add_argument("-w", "--workload", type=str, default="./workload",
                        help="Directory with all the queries to run.")
    parser.add_argument("-p", "--providers", type=str, default="./providers.csv",
                        help="File containing the list of the providers' names. One provider per line, with a multiplying factor to make it pop out more often.")
    parser.add_argument("-s", "--steps", type=str, default="./steps.json",
                        help="File containing the scheduling.")
    args = parser.parse_args()

    G = nx.Graph()

    # Add nodes for the providers
    G.add_nodes_from(
        ["chifflet", "chetemi", "chiclet", "chifflot", "dahu", "taurus", "nova", "uvb", "paravance", "parasilo",
         "paranoia", "econome", "petitprince", "grimoire", "grisou", "gros"])

    # Add nodes for the gateways
    G.add_nodes_from(["gw_lille_main", "gw_lille_1", "gw_lille_2", "gw_paris", "gw_nancy", "gw_lyon_main", "gw_lyon_1",
                      "gw_bretagne", "gw_nantes"])

    # Add edges between GWs and nodes
    G.add_edges_from([
        ("chifflet", "gw_lille_1"), ("chetemi", "gw_lille_1"),
        ("chiclet", "gw_lille_2"), ("chifflot", "gw_lille_2"),
        ("petitprince", "gw_nancy"), ("grimoire", "gw_nancy"), ("grisou", "gw_nancy"), ("gros", "gw_nancy"),
        ("dahu", "gw_lyon_main"), ("uvb", "gw_lyon_main"),
        ("taurus", "gw_lyon_1"), ("nova", "gw_lyon_1"),
        ("econome", "gw_bretagne"),
        ("paravance", "gw_nantes"), ("parasilo", "gw_nantes"), ("paranoia", "gw_nantes")
    ])

    # Add edges between the nodes
    G.add_edges_from([
        ("gw_lille_1", "gw_lille_main"), ("gw_lille_2", "gw_lille_main"),
        ("gw_paris", "gw_lille_main"), ("gw_paris", "gw_nancy"), ("gw_paris", "gw_lyon_main"),
        ("gw_lyon_main", "gw_lyon_1"), ("gw_lyon_main", "gw_bretagne"),
        ("gw_nantes", "gw_bretagne"),
    ])

    # Add weights
    for n in G.nodes:
        G.nodes[n]["weight"] = 0
    for s, t in G.edges:
        G.edges[s, t]["weight"] = 0

    # Load providers list
    with open(args.providers, "r") as f:
        csvreader = csv.reader(f, delimiter=";")
        providers = [l[0] for l in csvreader]

    # Load queries
    queries = dict()
    for fname in os.listdir(args.workload):
        if fname.endswith(".sql"):
            with open(os.path.join("workload_bak", fname), "r") as f:
                queries[fname[:-4]] = {"sql": " ".join(f.readlines())}

    # Get provider list per query
    for q, v in queries.items():
        queries[q] = [p for p in providers if p in v["sql"]]

    # Sorting the queries collection by ascending order of number of provider
    queries = dict(sorted(queries.items(), key=lambda x: len(x[1])))

    # Building the steps of the graph
    steps = list()
    processed_queries = list()
    for q1, v1 in queries.items():
        # Do not consider queries already processed
        if q1 in processed_queries:
            continue
        else:
            processed_queries.append(q1)
            step = [q1]

        # Finding combinations among quite small queries only
        if len(v1) <= 8:
            working_g = G.copy()
            # Mark the graph with current query
            working_g = mark_graph(working_g, v1)

            # Explore other queries
            for q2, v2 in queries.items():
                if len(v2) <= 6 \
                        and sum(len(queries[k]) for k in step) + len(v2) <= 8 \
                        and len(set(v1) & set(v2)) == 0 \
                        and q2 not in processed_queries:

                    # Get the graph
                    g_tmp = G.copy()
                    g_tmp = mark_graph(g_tmp, v2)

                    # Checking collisions
                    for e in g_tmp.edges:
                        g_tmp.edges[e]["weight"] += working_g.edges[e]["weight"]
                    # Is there is a collision, we step to the next query
                    if any(g_tmp.edges[e]["weight"] > 1 for e in g_tmp.edges):
                        continue
                    # If not, the query is integrated to the steps
                    else:
                        for n in g_tmp.nodes:
                            g_tmp.nodes[n]["weight"] += working_g.nodes[n]["weight"]
                        step.append(q2)
                        processed_queries.append(q2)
                        working_g = g_tmp

        steps.append(step)

    # Export away the steps
    with open(args.steps, "w") as f:
        json.dump(steps, f)
