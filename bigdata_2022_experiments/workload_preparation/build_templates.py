import argparse
import logging
import os
import re

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)-5s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


def process_query(q, input_dir, out_dir):
    # Reading the query

    with open(os.path.join(input_dir, q), "r") as f:
        query = f.read()
    
    query_raw = query
    query = query.replace("SELECT", "")
    projection = query.split("FROM")[0]
    relations = query.split("FROM")[1].split("WHERE")[0]
    selection = query.split("WHERE")[1]
    
    # Relations and attribute mapping to a code
    relations_mapping = {
        r.split(" AS ")[1].replace(",", "").strip() : (i, r.split(" AS ")[0].strip(), r.replace(",", "").strip()) 
        for i, r in enumerate(relations.strip().split("\n"))
    }

    attributes = dict()
    for i, e in enumerate(set(re.findall("(?!\d)[^\W]+\.(?!\d)[^\W]+", query))) :
        attributes[i] = {
            "name" : re.sub("\d", "", e.replace(".", "_")),
            "from" : relations_mapping[e.split(".")[0]][0],
            "replace" : e
        }
    
    # Replacing elements in the query, by descending order of attribute length
    for i in sorted(attributes, key=lambda i : len(attributes[i]["replace"]), reverse=True) :
        e = attributes[i]
        query_raw = query_raw.replace(e["replace"], f"aaa{i}aaa")

    for i in sorted(relations_mapping, key=lambda i : len(relations_mapping[i][2]), reverse=True) :
        e = relations_mapping[i]
        query_raw = query_raw.replace(e[2], f"rrr{e[0]}rrr")
    query_raw = query_raw.replace("MIN(", "").replace("MAX(", "").replace("AVG(", "")
    query_raw = re.sub("\) AS \w+(?=[,]?)", "", query_raw)
    
    # Final query
    for k, v in attributes.items() :
        v.pop("replace")
    
    info = {
            "relations" : {v[0] : v[1] for k, v in relations_mapping.items()},
            "attributes" : attributes
    }
    final_str = f"-- {info}\n{query_raw}"
    
    with open(os.path.join(out_dir, f"{q}.template"), "w+") as f :
        f.write(final_str)
    
if __name__ == "__main__" :

    # Reading 
    parser = argparse.ArgumentParser("Builds query templates from the JOB")
    parser.add_argument("-i", "--queries_folder", type=str, default="./job", help="The directory containing JOB's queries. Each query must be alone in a .sql file.")
    parser.add_argument("-o", "--templates_folder", type=str, default="./templates", help="The directory that will contain the templates built from the JOB.")
    args = parser.parse_args()

    # Checking if the directories exists
    if not os.path.isdir(args.queries_folder) :
        raise FileNotFoundError(f"Directory {args.queries_folder} unknown.")

    # Creating the output directory
    os.makedirs(args.templates_folder, exist_ok=True)


    queries =[e for e in os.listdir(args.queries_folder) if e.endswith("sql")]
    
    for q in queries :
        logging.info(f"Processing {q}")
        process_query(q, args.queries_folder, args.templates_folder)