import argparse
import copy
import csv
import logging
import re
from typing import List, Set, Dict

import networkx as nx
from networkx.algorithms.community import louvain_communities

logging.basicConfig(level=logging.WARN)

import os
import random


def make_graph(mapping, sql):
    # A DiGraph is used in order to make the community finding algorithm find larger communities
    G = nx.DiGraph()
    for r, inf in mapping["relations"].items():
        G.add_node(
            r, rname="inf"
        )

    # Extracting links from selection clauses
    where = sql.split("WHERE ")[1][:-3].split("\n  AND ")  # Lists all selection clauses

    links = list()
    for l in where:
        link = list({mapping["attributes"][int(a[3:-3])]["from"] for a in re.findall(r"aaa\d+aaa", l)})
        if len(link) > 1:
            links.append(link)

    # Linking together the nodes
    for lnk in links:
        G.add_edge(lnk[0], lnk[1])
        G.add_edge(lnk[1], lnk[0])

    return G


def node_set_neighbours(G: nx.DiGraph, nodes):
    return set(s for n in nodes for s in G.successors(n)) - set(nodes)


def merge_communities(G, communities, all_communities):
    """Merging communities until only two are remaining : towards less complex queries"""
    # Copying just to be sure to avoid side effects
    coms_tmp = copy.deepcopy(communities)
    while len(coms_tmp) > 2:
        c = random.choice(coms_tmp)
        c_neighbours = node_set_neighbours(G, c)
        merger = random.choice([e for e in coms_tmp if e != c and any(n in c_neighbours for n in e)])
        coms_tmp.remove(c)
        coms_tmp.remove(merger)
        coms_tmp.append(c | merger)

        all_communities.append(coms_tmp)
        coms_tmp = copy.deepcopy(coms_tmp)


def split_communities(G, communities, all_communities, nb_relations):
    """Splitting communities as much as possible : towards more complex queries"""
    # Copying just to be sure to avoid side effects
    coms_tmp = copy.deepcopy(communities)
    while len(coms_tmp) < nb_relations:
        c = random.choice([e for e in coms_tmp if len(e) > 1])
        coms_tmp.remove(c)
        A = random.sample(c, random.randint(1, len(c) - 1))
        A = set(A)
        c -= A
        coms_tmp.append(c)
        coms_tmp.append(A)

        all_communities.append(coms_tmp)
        coms_tmp = copy.deepcopy(coms_tmp)


def generate_queries_template_backbone(mapping, sql) -> List[List[Set[int]]]:
    """Returns a list of possible groups of relations to be affected to the providers."""
    G = make_graph(mapping, sql)

    # Compute communities of nodes in the query's graph as a baseline
    coms = louvain_communities(G)
    all_coms = [coms]

    if len(coms) > 1:
        merge_communities(G, coms, all_coms)

    if len(coms) < len(mapping["relations"]):
        split_communities(G, coms, all_coms, len(mapping["relations"]))

    return all_coms


def generate_sql(mapping, sql):
    sql = copy.deepcopy(sql)

    # Substitution of relations names
    for k, r in mapping["relations"].items():
        if r not in sql:
            sql = sql.replace(f"rrr{k}rrr", r)
        else:
            raise ValueError(f"Relation {r} already in the query...")

    # Substitution of the attributes
    for k, a in mapping["attributes"].items():
        sql = sql.replace(f"aaa{k}aaa", a["name"])

    return sql


def make_one_query(mapping, structure, sql, providers, providers_weight) -> str:
    # Copying in order to avoid side effects
    mapping = copy.deepcopy(mapping)

    # Listing joins
    sql_bak, selections = sql.strip()[:-1].split("WHERE")
    sql_bak += "WHERE "

    selections = {e.strip() for e in selections.split("AND")}
    joins_bak = {e for e in selections if re.match("aaa\d+aaa = aaa\d+aaa", e)}
    joins = {
        (
            int(e.split(" = ")[0].replace("aaa", "")),
            int(e.split(" = ")[1].replace("aaa", ""))
        ) for e in joins_bak
    }

    joins_per_provider_couple = dict()
    for j in joins:
        j_0_rel = mapping["attributes"][j[0]]["from"]
        j_1_rel = mapping["attributes"][j[1]]["from"]
        j_0_rel_group = [j_0_rel in e for e in structure].index(True)
        j_1_rel_group = [j_1_rel in e for e in structure].index(True)

        if j_0_rel != j_1_rel and j_0_rel_group != j_1_rel_group:
            if j_0_rel_group < j_1_rel_group:
                key = (j_0_rel_group, j_1_rel_group)
            else:
                key = (j_1_rel_group, j_0_rel_group)
            try:
                joins_per_provider_couple[key].add(j)
            except KeyError:
                joins_per_provider_couple[key] = {j}

    # Kepping only a join per group
    joins_to_remove = set()
    for v in joins_per_provider_couple.values():
        safe = random.choice(list(v))
        for e in v - {safe}:
            joins_to_remove.add(f"aaa{e[0]}aaa = aaa{e[1]}aaa")
            joins_to_remove.add(f"aaa{e[1]}aaa = aaa{e[0]}aaa")

    selections -= joins_to_remove

    # Rebuilding the SQL
    sql_bak += "\n AND ".join(selections)

    sql_bak += ";"

    # Choosing a set of providers for the query
    chosed_providers = list()
    while len(set(chosed_providers)) != len(structure):
        chosed_providers = random.sample(providers, len(structure), counts=providers_weight)

    # Mapping them to the relations
    for k, r in mapping["relations"].items():
        pr_i = [k in s for s in structure].index(True)
        mapping["relations"][k] = f"{r}_{chosed_providers[pr_i]}"

    # Renaming attributes
    for k, v in mapping["attributes"].items():
        p_name = mapping["relations"][v["from"]].split("_")[-1]
        v["name"] = f"{v['name']}_{p_name}"
        mapping["attributes"][k] = v

    return generate_sql(mapping, sql)


def generate_variants(qname, mapping, structures, sql, providers, providers_weight, nb_variants) -> Dict[str, str]:
    variants = set()
    for s in structures:
        # Generating variants
        for _ in range(nb_variants // 2 + 1):
            try:
                variants.add(make_one_query(mapping, s, sql, providers, providers_weight))
            except ValueError:  # A relation was redundant...
                pass

    # Picking only the number required
    try:
        variants = random.sample(variants, k=nb_variants)
    except ValueError:  # Less queries generated than expected...
        logging.warning(f"Less variants than expected for {qname} - {len(variants)}/{nb_variants}...")

    return {f"q{qname}_{i:02d}": e for i, e in enumerate(variants)}


def generate_queries(templates_path, providers, providers_weight, nb_variants=None):
    queries = dict()
    for f_name in os.listdir(templates_path):
        if f_name.endswith(".sql.template"):
            # Loadind the template
            with open(os.path.join(templates_path, f_name), "r") as f:
                lines = f.readlines()
                mapping = eval(lines[0][3:])
                sql = "".join(lines[1:])
            qname = f_name[:-len(".sql.template")]

            try:
                # Generate groups of relations to be mapped to a provider
                structures = generate_queries_template_backbone(mapping, sql)
                # Pruning out relations set with too much involved providers
                structures = [e for e in structures if len(e) <= len(providers)]
                queries.update(
                    generate_variants(qname, mapping, structures, sql, providers, providers_weight, nb_variants)
                )
                logging.info(f"Query {qname} processed...")
            except:
                logging.error(f"Query {qname} failed...")

    return queries

def dump_queries(output_folder, queries) :
    os.makedirs(output_folder, exist_ok=True)
    for k, sql in queries.items():
        with open(os.path.join(output_folder, f"{k}.sql"), "w") as f:
            f.write(sql)

if __name__ == "__main__" :
    parser = argparse.ArgumentParser(description="Generates JOB queries from the templates")
    parser.add_argument("-p", "--providers", type=str, default="./providers.csv",
                        help="File containing the list of the providers' names. One provider per line, with a multiplying factor to make it pop out more often.")
    parser.add_argument("-i", "--templates_folder", default="./templates", type=str)
    parser.add_argument("-o", "--output_folder", default="./workload", type=str)
    parser.add_argument("-N", "--queries_per_template", type=int, default=5)
    parser.add_argument("-c", "--clean", action='store_true',
                        help="Purges the output folder form its SQL queries before generating the variants.")

    args = parser.parse_args()

    if args.clean:
        try:
            for filename in os.listdir(args.output_folder):
                if filename.endswith(".sql"):
                    os.remove(os.path.join(args.output_folder, filename))
                    logging.info(f"Removing {filename}")
        except FileNotFoundError:
            logging.info("Nothing to clean...")

    # Reading the providers list
    with open(args.providers, "r", newline="\n") as f:
        csvreader = csv.reader(f, delimiter=";")
        providers = list()
        prov_weight = list()
        for l in csvreader:
            providers.append(l[0])
            prov_weight.append(int(l[1]))

    queries = generate_queries(args.templates_folder, providers, prov_weight, args.queries_per_template)
    dump_queries(args.output_folder, queries)
