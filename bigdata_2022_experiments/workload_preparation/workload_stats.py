import argparse
import os

import pandas as pd


def load_queries(queries_directory) :
	queries = dict()
	for f_name in os.listdir(queries_directory):
		if f_name.endswith(".sql"):
			# Loadind the template
			with open(os.path.join(queries_directory, f_name), "r") as f:
				f = f.readlines()
				queries[f_name] = "".join(e for e in f)
	return queries

def compute_one_query_stats(sql, providers):
	stats = {
		p : int(p in sql) for p in providers
	}
	stats["nb_prov"] = sum(stats[p] for p in providers)
	stats["nb_rels"] = len(sql.split("FROM ")[1].split("WHERE ")[0].strip().split("\n"))
	return stats

def compute_stats(queries, providers):
	return {
		k : compute_one_query_stats(v, providers) for k, v in queries.items()
	}

if __name__ == "__main__" :
	parser = argparse.ArgumentParser(description="Get stats about the workload")
	parser.add_argument("-q", "--queries_directory", type=str, default="./workload")
	parser.add_argument("-p", "--providers", type=str, default="./providers.csv")
	parser.add_argument("-o", "--output_stats", type=str, default="./stats.csv")

	args = parser.parse_args()

	# Reading the providers list
	with open(args.providers, "r") as f:
		providers = [l.strip().split(";")[0] for l in f.readlines() if len(l.strip()) > 0]

	queries = load_queries(args.queries_directory)
	stats = compute_stats(queries, providers)

	df = pd.DataFrame.from_dict(stats, orient="index")

	df = df.reset_index().rename(columns={"index": "qname"})

	print(df)
	print(df["nb_prov"].describe())
	gr = df.value_counts(subset=["nb_prov"], sort=True)
	print(gr)
	df.to_csv(args.output_stats, index=False)
