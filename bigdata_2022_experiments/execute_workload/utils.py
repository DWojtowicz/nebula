import logging
import os
from collections import OrderedDict
from datetime import datetime
from typing import Dict

import pandas as pd
import requests


def __load_excluded_queries(excluded_queries_file):
    try:
        with open(excluded_queries_file, "r") as f:
            return set(e.strip() for e in f.readlines() if len(e) > 3)
    except:
        return set()


def __load_workload_queries(workload_stats, excluded_queries, complexity:int=None):
    # Loading the queries names
    df_stats = pd.read_csv(workload_stats)
    # Filtering out unwanted ones
    df_stats = df_stats[~df_stats["qname"].isin(excluded_queries)]

    if complexity is not None and complexity > 0 :
        df_stats = df_stats[~df_stats["nb_prov"].equals(complexity)]
    # Sorting the dataframe by ascending order of the number of involved providers
    df_stats = df_stats.sort_values(by=["nb_prov", "qname"], ascending=False)

    return df_stats["qname"].to_list()


def load_queries(workload_dir, excluded_queries_file, workload_stats, complexity:int=None, max_complexity:int=None) -> Dict[str, str]:
    """Maps queries names to theis SQL code."""
    excluded_queries = __load_excluded_queries(excluded_queries_file)
    logging.info(f"Excluded queries: {excluded_queries}")
    q_names = __load_workload_queries(workload_stats, excluded_queries)

    # Queries will be processed by ascending complexity so an OrderedDict is used to preserve the order
    queries = OrderedDict()
    for q in q_names:
        try:
            with open(os.path.join(workload_dir, q), "r") as f:
                queries[f"{q[:-4]}"] = " ".join(e.strip() for e in f.readlines())
        except:
            continue

    return queries


def load_providers(g5k_machines_list="http://public.grenoble.grid5000.fr/~dwojtowicz/machines"):
    machines = requests.get(g5k_machines_list).text
    return {
        e.split("-")[0]: f"{e}:4242"
        for e in set(machines.strip().split("\n"))
    }


def make_stats(all_quotes, quote, plan, bill=None):
    now = datetime.now()

    res = {
        "query_name": all_quotes["qname"],
        "timestamp": str(now),
        "quotation_generation_time": all_quotes["sla_generation_time"],
        "quotation": plan,
        "quotations_number": len(all_quotes["quotations"]),
        "quoted_response_time": quote["response_time"],
        "quoted_monetary_cost": quote["monetary_cost"]
    }
    if bill is not None:
        res["real_response_time"] = bill["bill"]["response_time"],
        res["real_monetary_cost"] = bill["bill"]["total_cost"]

    return res
