import json
import logging
import os
import shutil

import requests
import time


class Nebula():

    def __init__(self, url, cost_model, exploration_strategy, exec_engine, checkpoint, nb_iterations_randomised, max_size_hypergraph, max_depth, no_history, disable_cost_model):
        self.url = url
        self.cost_model = cost_model
        self.exploration_strategy = exploration_strategy
        self.exec_engine = exec_engine
        self.checkpoint_file_name = checkpoint
        self.nb_iterations_randomised = nb_iterations_randomised
        self.max_size_hypergraph = max_size_hypergraph
        self.max_depth = max_depth
        self.no_history = no_history
        self.disable_cost_model = disable_cost_model

    def wait(self):
        """Waits for Nebula to be up"""
        up = False
        while not up:
            try:
                requests.get(f"http://{self.url}/")
                up = True
            except requests.exceptions.ConnectionError:
                logging.warning("Nebula not ready yet... Retrying!")
                time.sleep(10)

        logging.info("Nebula is ready!")

    def quote_query(self, query, query_name):
        data = {
            "query": query,
            "query_name": query_name,
            "cost_model": self.cost_model,
            "exploration_strategy": self.exploration_strategy,
            "nb_iterations": self.nb_iterations_randomised,
            "max_size_hypergraph": self.max_size_hypergraph,
            "max_depth": self.max_depth,
            "no_history": self.no_history,
            "disable_cost_model": self.disable_cost_model
        }
        message = requests.post(
            f"http://{self.url}/estimate",
            data=data,
            timeout=900
        )
        if message.status_code == 200:
            res =  json.loads(message.content)
            if "quotations" not in res :
                raise Exception(f"No quotation generated... {res}")
            else:
                return res
        else:
            raise Exception(f"No quotation generated... {message}")

    def execute_query(self, query, quotation):
        data = {
            "query_name": query,
            "method": self.exec_engine,
            "quotation_name" : quotation,
            "disable_cost_model": self.disable_cost_model
        }
        message = requests.post(
            f"http://{self.url}/execute",
            data=data,
            timeout=1200
        )
        if message.status_code == 200:
            msg = json.loads(message.content)
            if "status_code" in msg.keys() and msg["status_code"] != 201 :
                raise Exception(f"Execution failed... {message}")
            else :
                return msg
        else:
            raise Exception(f"Execution failed... {message}")

    def save_cost_model_history(self, cost_model_history_file):
        """Overwrites the cost model if possible"""
        history = requests.get(f"http://{self.url}/cost_model_history")

        if history.status_code == 200:
            history = json.loads(history.text)
            logging.info("Cost model history retrieved")

            # Backing up before writing to the file, just in case the process is killed so I don't loose a night's worth of experiments.
            # TODO : Check if a file is lost when G5K kills a process while it writes in it
            try:
                shutil.copy(cost_model_history_file, f"{cost_model_history_file}.bak")
            except FileNotFoundError:
                logging.warning("Backup for cost model history failed")
            with open(cost_model_history_file, "w+") as f:
                json.dump(history, f)
        else:
            logging.warning("Cost model fata failed to be retrieved... See you next time !")

        logging.info(f"Cost model history dumped to {cost_model_history_file}")

    def backup_cost_model(self):
        # Backing up before writing to the file, just in case the process is killed so I don't loose a night's worth of experiments.
        # TODO : Check if a file is lost when G5K kills a process while it writes in it
        try:
            shutil.copy(self.checkpoint_file_name, f"{self.checkpoint_file_name}.bak")
        except FileNotFoundError:
            logging.warning("Backup for checkpoint failed")

        response = requests.get(f"http://{self.url}/backup_cost_model")
        with open(self.checkpoint_file_name, "wb") as f:
            f.write(response.content)
        logging.info("Cost model backup done.")

    def load_backup(self):
        with open(self.checkpoint_file_name, 'rb') as f:
            r = requests.post(f"http://{self.url}/load_cost_model",
                              files={
                                  'file': (os.path.split(self.checkpoint_file_name)[1], f, 'application/octet-stream')})
        logging.info("Cost model loaded !")
