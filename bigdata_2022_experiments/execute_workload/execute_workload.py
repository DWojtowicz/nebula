import argparse
import datetime
import json
import logging
import shutil
import threading
from concurrent.futures import ThreadPoolExecutor
from typing import Dict, List

from requests.exceptions import Timeout

import pandas as pd

from expe2022.execute_workload.nebula import Nebula
from expe2022.execute_workload.utils import load_queries, make_stats

LOCK_ERR_LOG = threading.Lock()

COUNTER = 0

def log_error(query, failures_log, quotation="None", additional_info=""):
    global LOCK_ERR_LOG

    now = datetime.datetime.now()
    message = f"{str(now)} - {query} - {quotation} - {additional_info}"
    logging.error(message)

    LOCK_ERR_LOG.acquire()
    with open(failures_log, "a") as f:
        f.write(message + "\n")
    LOCK_ERR_LOG.release()

def process_one_query(q_name: str, sql: str, nebula: Nebula, results_history, failures_log, not_execute):
    global COUNTER
    try:
        logging.info(f"Asking quotatons for {q_name}")
        quotes = nebula.quote_query(sql, q_name)
    except Timeout :
        log_error(q_name, failures_log, additional_info="Timeout")
    except Exception as e:
        # Switching to the next query...
        log_error(q_name, failures_log, additional_info=str(e))
    else:
        for q in quotes["quotations"]:
            if not_execute :
                stats = make_stats(quotes, q, q["plan"])
                results_history.append(stats)

            else :
                try:
                    logging.info(f"Executing {q_name} with {q['plan']}")
                    bill = nebula.execute_query(q_name, q["plan"])
                except Timeout :
                    log_error(q_name, failures_log, q["plan"], "Timeout")
                    continue
                except Exception as e:
                    # Switching to the next quotation...
                    log_error(q_name, failures_log, q["plan"], additional_info=str(e))
                    continue
                else:
                    stats = make_stats(quotes, q, q["plan"], bill)
                    results_history.append(stats)
    COUNTER += 1
    logging.info(f"Query {q_name} done! - {COUNTER}")


def save_results(res, out_file, excluded_queries):
    # Backing up before writing to the file, just in case the process is killed so I don't loose a night's worth of experiments.
    # TODO : Check if a file is lost when G5K kills a process while it writes in it
    try:
        shutil.copy(out_file, f"{out_file}.bak")
    except FileNotFoundError:
        logging.warning("Backup for cost results failed")
    df = pd.DataFrame.from_dict(res)
    df.to_csv(out_file)

    # Backing up before writing to the file, just in case the process is killed so I don't loose a night's worth of experiments.
    # TODO : Check if a file is lost when G5K kills a process while it writes in it
    try:
        shutil.copy(excluded_queries, f"{excluded_queries}.bak")
    except FileNotFoundError:
        logging.warning("Backup for excluded queries failes")

    with open(excluded_queries, 'a') as f:
        # timestamp is removed from the query name before saving its name
        f.writelines(f"{'-'.join(e['query_name'].split('_')[:-1])}.sql\n" if "_" in e else f"{e}.sql" for e in res)


def run_experiments(steps, queries, nebula, cost_model_history, workload_results_file, failures_log, excluded_queries,
                    timestamp, not_execute):
    results_history = list()

    def thred_process_one_query(q):
        try:
            sql = queries[q]
        except KeyError:  # The query was previously filtered out.
            return
        process_one_query(f"{q}_{timestamp}", sql, nebula, results_history, failures_log, not_execute)

    for s in steps:
        if len(s) > 1:  # No need to thread stuff if there is no parallel query
            with ThreadPoolExecutor(max_workers=len(s)) as executor:
                for q in s:
                    future = executor.submit(thred_process_one_query, q)
                    err = future.exception()
                    if err:
                        raise err
        else:
            thred_process_one_query(s[0])

        save_results(results_history, workload_results_file, excluded_queries)
        nebula.save_cost_model_history(cost_model_history)  # After each batch
        nebula.backup_cost_model()

    logging.info("Finished !")


def load_steps(stepsfile, queries) -> List[List[str]]:
    try:
        with open(stepsfile, "r") as f:
            return json.load(f)
    except:
        return [[k] for k in queries]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Runs a batch of the JOB.")

    parser.add_argument("url", type=str, help="Nebula's URL.")

    g1 = parser.add_argument_group("Nebula", "Nebula configuration")
    g1.add_argument("-c", "--cost_model", type=str, default="stacked_postprocessed",
                    choices=["stacked", "literature", "stacked_postprocessed", "vanilla"],
                    help="The cost model to use.")
    g1.add_argument("-g", "--exploration_strategy", type=str, default="exhaustive", choices=["exhaustive", "randomised"],
                    help="The exploration strategy to use.")
    g1.add_argument("-m", "--execution_mode", type=str, default="dynamic", choices=["static", "dynamic", "static_randomised"],
                    help="The execution engine to use.")
    g1.add_argument("-x", "--not_execute", action='store_true',
                    help="Only gets the quotations.")

    g_random_strat = parser.add_argument_group("Randomised strategy parameters")
    g_random_strat.add_argument("-i", "--nb_iterations_randomised", type=int, default=1,
                    help="Number of iterations for the search space exploration")
    g_random_strat.add_argument("-z", "--max_size_hypergraph", type=int, default=10e6,
                    help="Maximal number of neighbours to consider")
    g_random_strat.add_argument("--max_depth", type=int, default=15,
                    help="Max number of neighbours layers from the initial plan")
    g_random_strat.add_argument("-t", "--complexity", type=int, default=None,
                            help="Runs queries of a given complexity. Defaults to no filter.")
    g_random_strat.add_argument("--max_complexity", type=int, default=None,
                            help="Runs queries of a maximal given complexity. Defaults to no filter.")
    g_random_strat.add_argument("--disable_cost_model", action='store_true',
                            help="Disables the cost model.")

    g2 = parser.add_argument_group("Resuming experiments")
    g2.add_argument("-r", "--resume", action='store_true',
                    help="If this argument is set, the checkpoint is loaded and the experiments resume where they stopped. Arguments -m and -c are overrided with checkpoint values.")

    g_in_files = parser.add_argument_group("Input files")
    g_in_files.add_argument("-w", "--workload", type=str, default="./workload",
                            help="Directory with all the queries to run.")
    g_in_files.add_argument("-e", "--exclude_queries", type=str, default="./excluded_queries.txt",
                            help="A list of queries not to process")
    g_in_files.add_argument("--ignore", type=str, default="./ignore.txt",
                            help="A list of queries not to process")
    g_in_files.add_argument("-s", "--workload_stats", type=str, default="./workload_stats.csv",
                            help="Stats about the queries produced by the `workload_stats.py` script.")
    g_in_files.add_argument("-l", "--parallel_steps", type=str, default=None,
                            help="List of list of queries, each sub-list containing queries that can be processed in parallel.")


    g_out_files = parser.add_argument_group("Output files")
    g_out_files.add_argument("-p", "--checkpoint", type=str, default="checkpoint.pickle", help="The backup file.")
    g_out_files.add_argument("--cost_model_history", type=str, default="history.json",
                             help="History of the cost model, JSON format.")
    g_out_files.add_argument("--workload_results", type=str, default="results.csv",
                             help="Execution results, csv format.")
    g_out_files.add_argument("--failures_log", type=str, default="failures.log",
                             help="Log of failed queries.")

    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO, format=f'{args.cost_model} %(asctime)s %(levelname)-5s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    # Waiting for Nebula to be up...
    nebula = Nebula(
        args.url, args.cost_model, args.exploration_strategy, args.execution_mode, args.checkpoint,
        args.nb_iterations_randomised, args.max_size_hypergraph, args.max_depth, args.not_execute, args.disable_cost_model)
    nebula.wait()
    try:
        nebula.load_backup()
    except FileNotFoundError:
        logging.info("No cost model to load...")
    except:
        logging.error("Cost model was not loaded...")
        logging.exception("Error :")
    # Loading the queries
    queries: Dict[str, str] = load_queries(args.workload, args.ignore, args.workload_stats, args.complexity, args.max_complexity)
    steps = load_steps(args.parallel_steps, queries)
    ts = datetime.datetime.now().strftime("%s")

    logging.info(f"{len(queries)} queries to process...")

    run_experiments(steps, queries, nebula, args.cost_model_history, args.workload_results, args.failures_log,
                    args.exclude_queries, ts, args.not_execute)
