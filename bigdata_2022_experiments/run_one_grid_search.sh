#!/bin/bash

if [ "$#" -ne 4 ]; then
	echo "Usage: ./run_one_grid_search.sh NEBULAINSTANCE MAX_DEPTH nb_iterations_randomised MAH_SIZE_HYPERGRAPH"
	exit 1
fi

cd /home/dwojtowicz/nebula
echo "In Nebula"

source venv/bin/activate || { echo "No venv found"; exit 1; }
export PYTHONPATH=$PYTHONPATH:$(pwd)
export BASE_WDIR=/home/dwojtowicz/expe_$(date +"%Y_%m_%d__%H_%M")

WDIR=${BASE_WDIR}__D$2__I$3

mkdir $WDIR
python3 \
        -u expe2022/execute_workload/execute_workload.py \
    $1":4242" \
    --parallel_steps steps.json \
    --cost_model "stacked_postprocessed" \
    --exclude_queries /home/dwojtowicz/max_12_prov.txt \
    --checkpoint $WDIR/checkpoint.pickle \
    --cost_model_history $WDIR/history.json \
    --workload_results $WDIR/results.csv \
    --execution_mode "static_randomised" \
    --exploration_strategy "randomised" \
    --max_depth $2 \
    --nb_iterations_randomised $3 \
    --max_size_hypergraph $4 \
    --disable_cost_model \
    --failures_log $WDIR/failures.log &> $WDIR/execution.log

wait
echo "Done!"
exit 1