import argparse
import os
from multiprocessing.pool import ThreadPool

from time import sleep
from typing import List
from urllib.request import urlopen

parser = argparse.ArgumentParser("Runs the experiments w/o running the queries w/ different parameters.")
parser.add_argument("script_path", type=str, help="Path to shell script to run one try.")
parser.add_argument(
    "--nebula_instances",
    type=str,
    default="http://public.grenoble.grid5000.fr/~dwojtowicz/nebula_instances",
    help="URL to fetch the list of instances of Nebula"
)
parser.add_argument("--max_hypergraph_size", type=int, default=1000000, help="Maximal amount of plans to consider")

parser.add_argument("--min_improvements", type=int, default=30, help="Minimal amount of improvements to consider")
parser.add_argument("--max_improvements", type=int, default=500, help="Maximal amount of improvements to consider")
parser.add_argument("--step_improvements", type=int, default=50, help="Increment for improvements")

parser.add_argument("--min_inflate", type=int, default=3, help="Minimal amount of search space inflation to perform")
parser.add_argument("--max_inflate", type=int, default=20, help="Maximal amount of search space inflation to perform")
parser.add_argument("--step_inflate", type=int, default=1, help="Increment for search space inflation")

args = parser.parse_args()

###############################################################################
# Load list of Nebula instances
###############################################################################

def read_nebula_instances(nebula_instances_url:str) -> List[str]:
    f = urlopen(nebula_instances_url)
    return f.read().decode('utf-8').strip().split()

instances_list = read_nebula_instances(args.nebula_instances)
workload = {k: list() for k in instances_list}


###############################################################################
# Building the experiments scenario
###############################################################################

cpt = 0
for inflate in range(args.min_inflate, args.max_inflate+args.step_inflate, args.step_inflate) :
    for improvements in range(args.min_improvements, args.max_improvements+args.step_improvements, args.step_improvements) :
        workload[instances_list[cpt%len(instances_list)]].append((improvements, inflate))
        cpt += 1


###############################################################################
# Running the experiments
###############################################################################

def run_config(instance:str, hypergraph_sz:int, improvements:int, inflation:int) -> None:
    print(f"Running on {instance} : D{improvements}, I{inflation} -- {hypergraph_sz}")
    os.system(f"bash {args.script_path} {instance} {improvements} {inflation} {hypergraph_sz}")
    print(f"Done on {instance} : D{improvements}, I{inflation} -- {hypergraph_sz}")

def run_instance(instance: str) :
    for e in workload[instance] :
        run_config(instance, args.max_hypergraph_size, e[0], e[1])

pool = ThreadPool(len(instances_list))
pool.map(run_instance, instances_list)
pool.close()
pool.join()