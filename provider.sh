#!/bin/bash

# Runs a simulated provider.
#
# Arguments
#   $1 : Provider's JSON conf file
#   $@ : All other arguments available for provider.py except ---conf

source venv/bin/activate || { echo "No venv found"; exit 1; }
export PYTHONPATH=$PYTHONPATH:$(pwd)
python3 nebula/provider/provider.py --conf $@