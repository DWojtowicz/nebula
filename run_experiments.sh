#!/bin/bash

# Arguments
#   $@ : Other arguments for execute_workload.py, e.g. --exploration_strategy and --execution_mode
#   $1 : expermiment mode, in ['randomised', 'exhaustive']
#   $2 : experiments duration in seconds

# Preparing venv and experiments script
if [ "$#" -ne 2 ]; then
        echo "Usage: ./run_experiments.sh MODE EXPERIMENTS_DURATION_IN_SECONDS"
        exit 1
fi


cd nebula
echo "In Nebula"

source venv/bin/activate || { echo "No venv found"; exit 1; }
export PYTHONPATH=$PYTHONPATH:$(pwd)
export BASE_WDIR=/home/dwojtowicz/expe_$(date +"%Y_%m_%d__%H_%M")

if $1 == "exhaustive" ; then

	costModelMode=("vanilla" "stacked" "stacked_postprocessed" "literature")

	# Getting list of Nebulas
	if [ -f nebula_instances ] ; then
		rm nebula_instances
	fi
	wget http://public.grenoble.grid5000.fr/~dwojtowicz/nebula_instances

	CPT=0
	cat nebula_instances | while read NEBULAINSTANCE
	do
		COST_MODEL=${costModelMode[$CPT]}
		WDIR=${BASE_WDIR}__${COST_MODEL}

		mkdir $WDIR
		python3 -u expe2022/execute_workload/execute_workload.py \
			$NEBULAINSTANCE":4242" \
			--parallel_steps steps.json \
			--cost_model $COST_MODEL \
			--exclude_queries $WDIR/excluded_queries.txt \
			--checkpoint $WDIR/checkpoint.pickle \
			--cost_model_history $WDIR/history.json \
			--workload_results $WDIR/results.csv \
			--failures_log $WDIR/failures.log &> $WDIR/execution.log &

		let CPT=CPT+1
	done

elif [ $1 == "randomised" ]; then

	costModelMode=("vanilla")

	# Getting list of Nebulas
	if [ -f nebula_instances ] ; then
		rm nebula_instances
	fi
	wget http://public.grenoble.grid5000.fr/~dwojtowicz/nebula_instances

	CPT=0
	cat nebula_instances | while read NEBULAINSTANCE
	do
		COST_MODEL=${costModelMode[$CPT]}
		WDIR=${BASE_WDIR}__${COST_MODEL}

		mkdir $WDIR
		python3 -u expe2022/execute_workload/execute_workload.py \
			$NEBULAINSTANCE":4242" \
			--parallel_steps steps.json \
			--cost_model $COST_MODEL \
			--exclude_queries $WDIR/excluded_queries.txt \
			--checkpoint $WDIR/checkpoint.pickle \
			--cost_model_history $WDIR/history.json \
			--workload_results $WDIR/results.csv \
			--execution_mode "static_randomised" \
			--failures_log $WDIR/failures.log &> $WDIR/execution.log &

		let CPT=CPT+1
	done

else
	echo "Mode unknown!!"
	exit 1
fi


wait
echo "All launched!"
sleep $1
echo "Done"