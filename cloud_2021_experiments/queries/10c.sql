select chn_name,
       t_title
from char_name,
     cast_info,
     company_name,
     company_type,
     movie_companies,
     role_type,
     title
where ci_note LIKE '%(producer)%'
  AND cn_country_code = '[us]'
  AND t_production_year > 1990
  AND t_id = mc_movie_id
  AND t_id = ci_movie_id
  AND ci_movie_id = mc_movie_id
  AND chn_id = ci_person_role_id
  AND rt_id = ci_role_id
  AND cn_id = mc_company_id
  AND ct_id = mc_company_type_id;
