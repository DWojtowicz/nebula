select n_name,
       t_title
from aka_name,
     cast_info,
     info_type,
     link_type,
     movie_link,
     name,
     person_info,
     title
where an_name LIKE '%a%'
  AND it_info = 'mini biography'
  AND lt_link = 'features'
  AND n_name_pcode_cf LIKE 'd%'
  AND n_gender = 'm'
  AND pi_note = 'volker boehm'
  AND (t_production_year BETWEEN 1980 AND 1984)
  AND n_id = an_person_id
  AND n_id = pi_person_id
  AND ci_person_id = n_id
  AND t_id = ci_movie_id
  AND ml_linked_movie_id = t_id
  AND lt_id = ml_link_type_id
  AND it_id = pi_info_type_id
  AND pi_person_id = an_person_id
  AND pi_person_id = ci_person_id
  AND an_person_id = ci_person_id
  AND ci_movie_id = ml_linked_movie_id;

