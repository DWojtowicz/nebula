select k_keyword,
       n_name,
       t_title
from cast_info,
     keyword,
     movie_keyword,
     name,
     title
where k_keyword = 'marvel-cinematic-universe'
  AND n_name LIKE '%downey%robert%'
  AND t_production_year > 2010
  AND k_id = mk_keyword_id
  AND t_id = mk_movie_id
  AND t_id = ci_movie_id
  AND ci_movie_id = mk_movie_id
  AND n_id = ci_person_id;

