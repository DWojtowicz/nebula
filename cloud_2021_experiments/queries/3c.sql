select t_title
from keyword,
     movie_info,
     movie_keyword,
     title
where k_keyword LIKE '%sequel%'
  AND mi_info IN ('sweden',
                  'norway',
                  'germany',
                  'denmark',
                  'swedish',
                  'denish',
                  'norwegian',
                  'german',
                  'usa',
                  'american')
  AND t_production_year > 1990
  AND t_id = mi_movie_id
  AND t_id = mk_movie_id
  AND mk_movie_id = mi_movie_id
  AND k_id = mk_keyword_id;

