select mi_idx_info,
       t_title
from info_type,
     keyword,
     movie_info_idx,
     movie_keyword,
     title
where it_info = 'rating'
  AND k_keyword LIKE '%sequel%'
  AND mi_idx_info > '5.0'
  AND t_production_year > 2005
  AND t_id = mi_idx_movie_id
  AND t_id = mk_movie_id
  AND mk_movie_id = mi_idx_movie_id
  AND k_id = mk_keyword_id
  AND it_id = mi_idx_info_type_id;

