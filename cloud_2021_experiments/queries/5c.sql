select t_title
from company_type,
     info_type,
     movie_companies,
     movie_info,
     title
where ct_kind = 'production companies'
  AND mc_note NOT LIKE '%(tv)%'
  AND mc_note LIKE '%(usa)%'
  AND mi_info IN ('sweden',
                  'norway',
                  'germany',
                  'denmark',
                  'swedish',
                  'denish',
                  'norwegian',
                  'german',
                  'usa',
                  'american')
  AND t_production_year > 1990
  AND t_id = mi_movie_id
  AND t_id = mc_movie_id
  AND mc_movie_id = mi_movie_id
  AND ct_id = mc_company_type_id
  AND it_id = mi_info_type_id;

