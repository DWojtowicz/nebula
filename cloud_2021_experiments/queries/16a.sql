select an_name,
       t_title
from aka_name,
     cast_info,
     company_name,
     keyword,
     movie_companies,
     movie_keyword,
     name,
     title
where cn_country_code = '[us]'
  AND k_keyword = 'character-name-in-title'
  AND t_episode_nr >= 50
  AND t_episode_nr < 100
  AND an_person_id = n_id
  AND n_id = ci_person_id
  AND ci_movie_id = t_id
  AND t_id = mk_movie_id
  AND mk_keyword_id = k_id
  AND t_id = mc_movie_id
  AND mc_company_id = cn_id
  AND an_person_id = ci_person_id
  AND ci_movie_id = mc_movie_id
  AND ci_movie_id = mk_movie_id
  AND mc_movie_id = mk_movie_id;

