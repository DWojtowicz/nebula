select n_name,
       t_title
from aka_name,
     char_name,
     cast_info,
     company_name,
     info_type,
     movie_companies,
     movie_info,
     name,
     role_type,
     title
where ci_note IN ('(voice)',
                  '(voice: japanese version)',
                  '(voice) (uncredited)',
                  '(voice: english version)')
  AND cn_country_code = '[us]'
  AND it_info = 'release dates'
  AND mi_info IS NOT NULL
  AND (mi_info LIKE 'japan:%200%'
    OR mi_info LIKE 'usa:%200%')
  AND n_gender = 'f'
  AND n_name LIKE '%an%'
  AND rt_role = 'actress'
  AND t_production_year > 2000
  AND t_id = mi_movie_id
  AND t_id = mc_movie_id
  AND t_id = ci_movie_id
  AND mc_movie_id = ci_movie_id
  AND mc_movie_id = mi_movie_id
  AND mi_movie_id = ci_movie_id
  AND cn_id = mc_company_id
  AND it_id = mi_info_type_id
  AND n_id = ci_person_id
  AND rt_id = ci_role_id
  AND n_id = an_person_id
  AND ci_person_id = an_person_id
  AND chn_id = ci_person_role_id;

