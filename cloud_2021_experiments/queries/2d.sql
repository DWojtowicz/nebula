select t_title
from company_name,
     keyword,
     movie_companies,
     movie_keyword,
     title
where cn_country_code = '[us]'
  AND k_keyword = 'character-name-in-title'
  AND cn_id = mc_company_id
  AND mc_movie_id = t_id
  AND t_id = mk_movie_id
  AND mk_keyword_id = k_id
  AND mc_movie_id = mk_movie_id;

