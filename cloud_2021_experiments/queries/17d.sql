select n_name
from cast_info,
     company_name,
     keyword,
     movie_companies,
     movie_keyword,
     name,
     title
where k_keyword = 'character-name-in-title'
  AND n_name LIKE '%bert%'
  AND n_id = ci_person_id
  AND ci_movie_id = t_id
  AND t_id = mk_movie_id
  AND mk_keyword_id = k_id
  AND t_id = mc_movie_id
  AND mc_company_id = cn_id
  AND ci_movie_id = mc_movie_id
  AND ci_movie_id = mk_movie_id
  AND mc_movie_id = mk_movie_id;

