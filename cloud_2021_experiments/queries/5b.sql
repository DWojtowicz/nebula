select t_title
from company_type,
     info_type,
     movie_companies,
     movie_info,
     title
where ct_kind = 'production companies'
  AND mc_note LIKE '%(vhs)%'
  AND mc_note LIKE '%(usa)%'
  AND mc_note LIKE '%(1994)%'
  AND mi_info IN ('usa',
                  'america')
  AND t_production_year > 2010
  AND t_id = mi_movie_id
  AND t_id = mc_movie_id
  AND mc_movie_id = mi_movie_id
  AND ct_id = mc_company_type_id
  AND it_id = mi_info_type_id;

