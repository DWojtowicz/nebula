select cn_name,
       lt_link,
       t_title
from company_name,
     company_type,
     keyword,
     link_type,
     movie_companies,
     movie_info,
     movie_keyword,
     movie_link,
     title
where cn_country_code != '[pl]'
  AND (cn_name LIKE '%film%'
    OR cn_name LIKE '%warner%')
  AND ct_kind = 'production companies'
  AND k_keyword = 'sequel'
  AND lt_link LIKE '%follow%'
  AND mc_note IS NULL
  AND mi_info IN ('germany',
                  'german')
  AND (t_production_year BETWEEN 2000 AND 2010)
  AND lt_id = ml_link_type_id
  AND ml_movie_id = t_id
  AND t_id = mk_movie_id
  AND mk_keyword_id = k_id
  AND t_id = mc_movie_id
  AND mc_company_type_id = ct_id
  AND mc_company_id = cn_id
  AND mi_movie_id = t_id
  AND ml_movie_id = mk_movie_id
  AND ml_movie_id = mc_movie_id
  AND mk_movie_id = mc_movie_id
  AND ml_movie_id = mi_movie_id
  AND mk_movie_id = mi_movie_id
  AND mc_movie_id = mi_movie_id;

