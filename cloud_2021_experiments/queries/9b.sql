select an_name,
       chn_name,
       n_name,
       t_title
from aka_name,
     char_name,
     cast_info,
     company_name,
     movie_companies,
     name,
     role_type,
     title
where ci_note = '(voice)'
  AND cn_country_code = '[us]'
  AND mc_note LIKE '%(200%)%'
  AND (mc_note LIKE '%(usa)%'
    OR mc_note LIKE '%(worldwide)%')
  AND n_gender = 'f'
  AND n_name LIKE '%angel%'
  AND rt_role = 'actress'
  AND (t_production_year BETWEEN 2007 AND 2010)
  AND ci_movie_id = t_id
  AND t_id = mc_movie_id
  AND ci_movie_id = mc_movie_id
  AND mc_company_id = cn_id
  AND ci_role_id = rt_id
  AND n_id = ci_person_id
  AND chn_id = ci_person_role_id
  AND an_person_id = n_id
  AND an_person_id = ci_person_id;

