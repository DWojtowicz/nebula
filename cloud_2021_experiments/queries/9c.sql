select an_name,
       chn_name,
       n_name,
       t_title
from aka_name,
     char_name,
     cast_info,
     company_name,
     movie_companies,
     name,
     role_type,
     title
where ci_note IN ('(voice)',
                  '(voice: japanese version)',
                  '(voice) (uncredited)',
                  '(voice: english version)')
  AND cn_country_code = '[us]'
  AND n_gender = 'f'
  AND n_name LIKE '%an%'
  AND rt_role = 'actress'
  AND ci_movie_id = t_id
  AND t_id = mc_movie_id
  AND ci_movie_id = mc_movie_id
  AND mc_company_id = cn_id
  AND ci_role_id = rt_id
  AND n_id = ci_person_id
  AND chn_id = ci_person_role_id
  AND an_person_id = n_id
  AND an_person_id = ci_person_id;

