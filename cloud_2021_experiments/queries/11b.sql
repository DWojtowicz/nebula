select cn_name,
       lt_link,
       t_title
from company_name,
     company_type,
     keyword,
     link_type,
     movie_companies,
     movie_keyword,
     movie_link,
     title
where cn_country_code != '[pl]'
  AND (cn_name LIKE '%film%'
    OR cn_name LIKE '%warner%')
  AND ct_kind = 'production companies'
  AND k_keyword = 'sequel'
  AND lt_link LIKE '%follows%'
  AND mc_note IS NULL
  AND t_production_year = 1998
  AND t_title LIKE '%money%'
  AND lt_id = ml_link_type_id
  AND ml_movie_id = t_id
  AND t_id = mk_movie_id
  AND mk_keyword_id = k_id
  AND t_id = mc_movie_id
  AND mc_company_type_id = ct_id
  AND mc_company_id = cn_id
  AND ml_movie_id = mk_movie_id
  AND ml_movie_id = mc_movie_id
  AND mk_movie_id = mc_movie_id;

