select mc_note,
       t_title,
       t_production_year
from company_type,
     info_type,
     movie_companies,
     movie_info_idx,
     title
where ct_kind = 'production companies'
  AND it_info = 'top 250 rank'
  AND mc_note NOT LIKE '%(as metro-goldwyn-mayer pictures)%'
  AND (mc_note LIKE '%(co-production)%'
    OR mc_note LIKE '%(presents)%')
  AND ct_id = mc_company_type_id
  AND t_id = mc_movie_id
  AND t_id = mi_idx_movie_id
  AND mc_movie_id = mi_idx_movie_id
  AND it_id = mi_idx_info_type_id;

