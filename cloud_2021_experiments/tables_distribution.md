# Tables distribution over the simulated providers

## Provider `P1` "Actors"

| Table               | Size (MB) |
|---------------------|-----------|
| `aka_name`          | 129,24    |
| `cast_info`         | 3477,34   |
| `char_name`         | 392,60    |
| `info_type`         | 0,01      |
| `person_info`       | 610,38    |
| `role_type`         | 0,01      |
| `name`              | 666,14    |

## Provider `P2` "Movies"

| Table               | Size (MB) |
|---------------------|-----------|
| `title`             | 518,71    |
| `movie_keyword`     | 315,88    |
| `movie_link`        | 109,17    |
| `movie_info_idx`    | 2461,69   |
| `movie_info`        | 2461,70   |
| `keyword`           | 13,40625  |
| `comp_cast_type`    | 0,01      |
| `aka_title`         | 73        |
| `kind_type`         | 0,01      |
| `link_type`         | 0,01      |

## Provider `P3` "Companies"

| Table               | Size (MB) |
|---------------------|-----------|
| `company_name`      | 36,12     |
| `company_type`      | 0,01      |
| `complete_cast`     | 5,71      |
| `movie_companies`   | 272,34    |