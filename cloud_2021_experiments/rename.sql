ALTER TABLE aka_name
    RENAME COLUMN id TO an_id;
ALTER TABLE aka_name
    RENAME COLUMN imdb_index TO an_imdb_index;
ALTER TABLE aka_name
    RENAME COLUMN surname_pcode TO an_surname_pcode;
ALTER TABLE aka_name
    RENAME COLUMN name TO an_name;
ALTER TABLE aka_name
    RENAME COLUMN md5sum TO an_md5sum;
ALTER TABLE aka_name
    RENAME COLUMN name_pcode_nf TO an_name_pcode_nf;
ALTER TABLE aka_name
    RENAME COLUMN name_pcode_cf TO an_name_pcode_cf;
ALTER TABLE aka_name
    RENAME COLUMN person_id TO an_person_id;

ALTER TABLE aka_title
    RENAME COLUMN id TO at_id;
ALTER TABLE aka_title
    RENAME COLUMN episode_nr TO at_episode_nr;
ALTER TABLE aka_title
    RENAME COLUMN episode_of_id TO at_episode_of_id;
ALTER TABLE aka_title
    RENAME COLUMN imdb_index TO at_imdb_index;
ALTER TABLE aka_title
    RENAME COLUMN kind_id TO at_kind_id;
ALTER TABLE aka_title
    RENAME COLUMN md5sum TO at_md5sum;
ALTER TABLE aka_title
    RENAME COLUMN movie_id TO at_movie_id;
ALTER TABLE aka_title
    RENAME COLUMN note TO at_note;
ALTER TABLE aka_title
    RENAME COLUMN phonetic_code TO at_phonetic_code;
ALTER TABLE aka_title
    RENAME COLUMN production_year TO at_production_year;
ALTER TABLE aka_title
    RENAME COLUMN season_nr TO at_season_nr;
ALTER TABLE aka_title
    RENAME COLUMN title TO at_title;

ALTER TABLE cast_info
    RENAME COLUMN person_id TO ci_person_id;
ALTER TABLE cast_info
    RENAME COLUMN note TO ci_note;
ALTER TABLE cast_info
    RENAME COLUMN nr_order TO ci_nr_order;
ALTER TABLE cast_info
    RENAME COLUMN movie_id TO ci_movie_id;
ALTER TABLE cast_info
    RENAME COLUMN role_id TO ci_role_id;
ALTER TABLE cast_info
    RENAME COLUMN person_role_id TO ci_person_role_id;
ALTER TABLE cast_info
    RENAME COLUMN id TO ci_id;

ALTER TABLE char_name
    RENAME COLUMN name_pcode_nf TO chn_name_pcode_nf;
ALTER TABLE char_name
    RENAME COLUMN name TO chn_name;
ALTER TABLE char_name
    RENAME COLUMN md5sum TO chn_md5sum;
ALTER TABLE char_name
    RENAME COLUMN imdb_index TO chn_imdb_index;
ALTER TABLE char_name
    RENAME COLUMN id TO chn_id;
ALTER TABLE char_name
    RENAME COLUMN imdb_id TO chn_imdb_id;
ALTER TABLE char_name
    RENAME COLUMN surname_pcode TO chn_surname_pcode;

ALTER TABLE company_name
    RENAME COLUMN name TO cn_name;
ALTER TABLE company_name
    RENAME COLUMN name_pcode_nf TO cn_name_pcode_nf;
ALTER TABLE company_name
    RENAME COLUMN name_pcode_sf TO cn_name_pcode_sf;
ALTER TABLE company_name
    RENAME COLUMN id TO cn_id;
ALTER TABLE company_name
    RENAME COLUMN imdb_id TO cn_imdb_id;
ALTER TABLE company_name
    RENAME COLUMN md5sum TO cn_md5sum;
ALTER TABLE company_name
    RENAME COLUMN country_code TO cn_country_code;

ALTER TABLE company_type
    RENAME COLUMN kind TO ct_kind;
ALTER TABLE company_type
    RENAME COLUMN id TO ct_id;

ALTER TABLE comp_cast_type
    RENAME COLUMN id TO cct_id;
ALTER TABLE comp_cast_type
    RENAME COLUMN kind TO cct_kind;
ALTER TABLE complete_cast
    RENAME COLUMN id TO cc_id;
ALTER TABLE complete_cast
    RENAME COLUMN movie_id TO cc_movie_id;
ALTER TABLE complete_cast
    RENAME COLUMN status_id TO cc_status_id;
ALTER TABLE complete_cast
    RENAME COLUMN subject_id TO cc_subject_id;

ALTER TABLE info_type
    RENAME COLUMN id TO it_id;
ALTER TABLE info_type
    RENAME COLUMN info TO it_info;

ALTER TABLE keyword
    RENAME COLUMN id TO k_id;
ALTER TABLE keyword
    RENAME COLUMN phonetic_code TO k_phonetic_code;
ALTER TABLE keyword
    RENAME COLUMN keyword TO k_keyword;

ALTER TABLE link_type
    RENAME COLUMN link TO lt_link;
ALTER TABLE link_type
    RENAME COLUMN id TO lt_id;

ALTER TABLE movie_companies
    RENAME COLUMN company_id TO mc_company_id;
ALTER TABLE movie_companies
    RENAME COLUMN company_type_id TO mc_company_type_id;
ALTER TABLE movie_companies
    RENAME COLUMN id TO mc_id;
ALTER TABLE movie_companies
    RENAME COLUMN movie_id TO mc_movie_id;
ALTER TABLE movie_companies
    RENAME COLUMN note TO mc_note;

ALTER TABLE movie_info
    RENAME COLUMN movie_id TO mi_movie_id;
ALTER TABLE movie_info
    RENAME COLUMN note TO mi_note;
ALTER TABLE movie_info
    RENAME COLUMN info TO mi_info;
ALTER TABLE movie_info
    RENAME COLUMN id TO mi_id;
ALTER TABLE movie_info
    RENAME COLUMN info_type_id TO mi_info_type_id;

ALTER TABLE movie_info_idx
    RENAME COLUMN movie_id TO mi_idx_movie_id;
ALTER TABLE movie_info_idx
    RENAME COLUMN note TO mi_idx_note;
ALTER TABLE movie_info_idx
    RENAME COLUMN info_type_id TO mi_idx_info_type_id;
ALTER TABLE movie_info_idx
    RENAME COLUMN id TO mi_idx_id;
ALTER TABLE movie_info_idx
    RENAME COLUMN info TO mi_idx_info;

ALTER TABLE movie_keyword
    RENAME COLUMN movie_id TO mk_movie_id;
ALTER TABLE movie_keyword
    RENAME COLUMN keyword_id TO mk_keyword_id;
ALTER TABLE movie_keyword
    RENAME COLUMN id TO mk_id;

ALTER TABLE movie_link
    RENAME COLUMN movie_id TO ml_movie_id;
ALTER TABLE movie_link
    RENAME COLUMN id TO ml_id;
ALTER TABLE movie_link
    RENAME COLUMN link_type_id TO ml_link_type_id;
ALTER TABLE movie_link
    RENAME COLUMN linked_movie_id TO ml_linked_movie_id;

ALTER TABLE name
    RENAME COLUMN gender TO n_gender;
ALTER TABLE name
    RENAME COLUMN name TO n_name;
ALTER TABLE name
    RENAME COLUMN imdb_id TO n_imdb_id;
ALTER TABLE name
    RENAME COLUMN id TO n_id;
ALTER TABLE name
    RENAME COLUMN imdb_index TO n_imdb_index;
ALTER TABLE name
    RENAME COLUMN md5sum TO n_md5sum;
ALTER TABLE name
    RENAME COLUMN name_pcode_nf TO n_name_pcode_nf;
ALTER TABLE name
    RENAME COLUMN surname_pcode TO n_surname_pcode;
ALTER TABLE name
    RENAME COLUMN name_pcode_cf TO n_name_pcode_cf;

ALTER TABLE person_info
    RENAME COLUMN info_type_id TO pi_info_type_id;
ALTER TABLE person_info
    RENAME COLUMN id TO pi_id;
ALTER TABLE person_info
    RENAME COLUMN info TO pi_info;
ALTER TABLE person_info
    RENAME COLUMN person_id TO pi_person_id;
ALTER TABLE person_info
    RENAME COLUMN note TO pi_note;

ALTER TABLE role_type
    RENAME COLUMN role TO rt_role;
ALTER TABLE role_type
    RENAME COLUMN id TO rt_id;

ALTER TABLE title
    RENAME COLUMN imdb_id TO t_imdb_id;
ALTER TABLE title
    RENAME COLUMN episode_of_id TO t_episode_of_id;
ALTER TABLE title
    RENAME COLUMN season_nr TO t_season_nr;
ALTER TABLE title
    RENAME COLUMN phonetic_code TO t_phonetic_code;
ALTER TABLE title
    RENAME COLUMN imdb_index TO t_imdb_index;
ALTER TABLE title
    RENAME COLUMN id TO t_id;
ALTER TABLE title
    RENAME COLUMN kind_id TO t_kind_id;
ALTER TABLE title
    RENAME COLUMN episode_nr TO t_episode_nr;
ALTER TABLE title
    RENAME COLUMN md5sum TO t_md5sum;
ALTER TABLE title
    RENAME COLUMN title TO t_title;
ALTER TABLE title
    RENAME COLUMN series_years TO t_series_years;
ALTER TABLE title
    RENAME COLUMN production_year TO t_production_year;