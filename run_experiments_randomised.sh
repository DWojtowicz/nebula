#!/bin/bash

# Arguments
#   $1 : nebula instance
#   $2 : wait delay

# Preparing venv and experiments script
if [ "$#" -ne 2 ]; then
        echo "Usage: ./run_experiments.sh NEBULA_URL WAITING_TIME"
        exit 1
fi

cd nebula
echo "In Nebula"

source venv/bin/activate || { echo "No venv found"; exit 1; }
export PYTHONPATH=$PYTHONPATH:$(pwd)
export BASE_WDIR=/home/dwojtowicz/expe_randomised_$(date +"%Y_%m_%d__%H_%M")

WDIR=${BASE_WDIR}__${COST_MODEL}

mkdir $WDIR
python3 -u expe2022/execute_workload/execute_workload.py \
	$1":4242" \
	--cost_model "vanilla" \
	--exploration_strategy "randomised" \
	--execution_mode "static_randomised" \
	--parallel_steps steps.json \
	--exclude_queries $WDIR/excluded_queries.txt \
	--checkpoint $WDIR/checkpoint.pickle \
	--cost_model_history $WDIR/history.json \
	--workload_results $WDIR/results.csv \
	--failures_log $WDIR/failures.log &> $WDIR/execution.log

wait
echo "All launched!"
# sleep $2
echo "Done"